<?php
namespace App\Repositories;

use App\User;

class UserRepository implements UserRepositoryInterface
{

public function all()
{
return User::all();
}

public function create($data)
{
return User::create($data);
}

public function find($id)
{
return User::find($id);
}

public function delete($id)
{
return User::destroy($id);
}

public function update($id, array $data)
{
return User::find($id)->update($data);
}
}
