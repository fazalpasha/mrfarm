<?php

namespace App\Repositories;
use App\Entities\attendence;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class AttendenceRepository implements AttendenceRepositoryInterface
{
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return attendence::all();
    }

    public function create($data)
    {
        return attendence::create($data);
    }

    public function find($id)
    {
        return attendence::find($id);
    }

    public function delete($id)
    {
        return attendence::destroy($id);
    }

    public function update($id, array $data)
    {
        return attendence::find($id)->update($data);
    }

}
