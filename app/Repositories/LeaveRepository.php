<?php

namespace App\Repositories;
use App\Entities\leave;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class LeaveRepository implements LeaveRepositoryInterface
{
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return leave::all();
    }

    public function create($data)
    {
        return leave::create($data);
    }

    public function find($id)
    {
        return leave::find($id);
    }

    public function delete($id)
    {
        return leave::destroy($id);
    }

    public function update($id, array $data)
    {
        return leave::find($id)->update($data);
    }

}
