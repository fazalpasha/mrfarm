<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedchart extends Model
{
    protected $table = 'feedcharts';
    public $translatedAttributes = [];
    protected $fillable = ['id','cell_number','user_id','category_id','subcategory_id','quantity','units','date_given','created_at','updated_at','branch_id'];
}
