<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $table = 'otpvalidate';
    public $translatedAttributes = [];
    protected $fillable = ['id','countryCode','phone','otp','exp_time'];
}
