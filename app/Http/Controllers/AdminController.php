<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\attendence;
use App\Entities\entrylogs;
use App\Entities\sales;
use App\Entities\animalstock;
use App\Entities\expense;
use App\Entities\branch;
use App\Entities\cell;
use App\Entities\vendor;
use Illuminate\Support\Facades\Auth;
use DB;
use App\user;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index(Request $request)
  {
    // print_r($request->all());die();
      $sales = sales::all();
      $branch=branch::all();
       $userid = Auth::user();
       $newbranch_id=$userid->branch_id;
      if(isset($request->branch_id))
      {
        // print_r($request->all());die();

        $newbranch_id=$request->branch_id;
            $aa=User::where('id','=',$userid->id)->update(['branch_id'=>$request->branch_id]);
           // $userid->save();
      }
     
      $mybranch=branch::find($newbranch_id); 
      // print_r($userid->branch_id);die();
      $saleschart = sales::whereYear('created_at','=',date('Y'))->where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->select(DB::raw('sum(total_price) as Count ,MONTHNAME(created_at) as monthname'))->groupBy(DB::raw('MONTH(created_at)'))->get();
      $entrychart=entrylogs::whereYear('created_at','=',date('Y'))->where('entrylogs.branch_id','!=',NULL)->where('entrylogs.branch_id','=',$userid->branch_id)->select(DB::raw('count(created_at) as Count,MONTHNAME(created_at) as monthname'))->groupBy(DB::raw('MONTH(created_at)'))->get();
      $staffattend = attendence::all();
      $latestsale=sales::take(5)->where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->get();
      $latestanimal= animalstock::leftJoin('category','animalstock.category_id','=','category.id')->leftJoin('cell_box','animalstock.cell_number','=','cell_box.id')->where('animalstock.branch_id','!=',NULL)->where('animalstock.branch_id','=',$userid->branch_id)->select('category.category_name','category.image','animalstock.*')->orderBy('id','DESC')->take(5)->get();
      $expenses=expense::whereYear('date','=',date('Y'))->where('expense.branch_id','!=',NULL)->where('expense.branch_id','=',$userid->branch_id)->select(DB::raw('sum(total) as Count ,MONTHNAME(date) as monthname'))->groupBy(DB::raw('MONTH(date)'))->get();
      
      $count=animalstock::where('animalstock.branch_id','!=',NULL)->where('animalstock.branch_id','=',$userid->branch_id)->count('id');
      $totstaff=user::where('users.branch_id','!=',NULL)->where('users.branch_id','=',$userid->branch_id)->where('users.roles','=',2)->count('id');
      $totcell=cell::where('cell_box.branch_id','!=',NULL)->where('cell_box.branch_id','=',$userid->branch_id)->count('id');
      $totsale=sales::where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->count('id');
      $totvendor=vendor::where('vendor.branch_id','!=',NULL)->where('vendor.branch_id','=',$userid->branch_id)->count('id');
      $totexpense=expense::where('expense.branch_id','!=',NULL)->where('expense.branch_id','=',$userid->branch_id)->sum('total');
      $totincome=sales::where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->sum('total_price');
      $ventrylogs=entrylogs::where('entrylogs.branch_id','!=',NULL)->where('entrylogs.branch_id','=',$userid->branch_id)->count('id');

        return view('index',compact('sales','staffattend','saleschart','entrychart','latestsale','latestanimal','expenses','branch','count','totsale','totexpense','totincome','ventrylogs','mybranch','totstaff','totcell','totvendor')); 
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

   /**
     * Store a newly created resource in storage.
    *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
    * Display the specified resource.
     *
    * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
