<?php

namespace App\Http\Controllers;
use App\Entities\subcategory;
use App\Entities\category;
use Illuminate\Http\Request;
use App\Http\Requests\subcategoryrequest;
use Illuminate\Support\Facades\Auth;


class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     $data = subcategory::leftJoin('category','subcategory.category_id','=','category.id')
            ->select('category.*','subcategory.*')->get();
        return view('subcategory.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $category = category::all();
        return view('subcategory.create',compact('data','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(subcategoryrequest $request)
    {
        $userid = Auth::user();
        $vendor = subcategory::create(['subcategory_name' => $request->input('subcategory_name'),'status'=> $request->status,'category_id'=> $request->id,'branch_id'=>$userid->branch_id]);
      return redirect()->route('subcategory.index')
       ->with('success','Subcategory created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(subcategory $subcategory)
    {
         $category = category::all();
        return view('subcategory.edit',compact('subcategory','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(subcategory $subcategory,request $request)
    {
         $this->validate($request, [
            'subcategory_name' => 'required',
            'status'=>'required'
           
            
        ]);

     $subcategory->subcategory_name = $request->input('subcategory_name');
     $subcategory->status = $request->input('status');
     $subcategory->category_id = $request->input('id');

        $subcategory->save();
        return redirect()->route('subcategory.index')
        ->with('success','Subcategory updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(subcategory $subcategory)
    {
        subcategory::find($subcategory->id)->delete();
        return redirect()->route('subcategory.index')
        ->with('success','Vendor deleted successfully');
    }
}
