<?php

namespace App\Http\Controllers;
use App\Entities\stock;

use App\Entities\category;
use App\Entities\stockmanagement;
use App\Entities\vendor;
use App\Entities\subcategory;
use Illuminate\Http\Request;
use App\Http\Requests\foodstockrequest;
use App\Http\Requests\stockupdaterequest;


class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


       $data = stock::leftJoin('category','stockmanagement.id','=','category.id')
       ->leftjoin('subcategory','stockmanagement.id','=','subcategory.id')
            ->select('category.*','subcategory.*','stockmanagement.*')->get();
        return view('stock.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $vendor = vendor::all();
         $subcategory = subcategory::all();
         $category = category::all();
        return view('stock.create',compact('data','vendor','subcategory','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(foodstockrequest $request)
    {


     $stocks = stock::create(['qrcode' => $request->input('qrcode'),'item_name'=>$request->item_name,'vendor_name'=>$request->vendor_name,'vendor_id'=>$request->vendor_id,'billing_amount'=>$request->billing_amount,'receipt_no'=>$request->receipt_no,'invoice_no'=>$request->invoice_no,'invoice_prefix'=>$request->invoice_prefix,'quality'=>$request->quality,'quantity'=>$request->quantity,'date'=>$request->date,'remarks'=>$request->remarks,'received_by_name'=>$request->received_by_name,'document_upload'=>$request->document_upload,'category_id'=>$request->category_id,'subcategory_id'=>$request->subcategory_id]);

   
       return redirect()->route('stock.index')
       ->with('success','Role created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(stock $stock)
    {
          $vendor = vendor::all();
          $subcategory = subcategory::all();
          $category = category::all();
        
    return view('stock.edit',compact('stock','vendor','subcategory','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(stock $stock,Request $request)
    {
        $this->validate($request, [
           
            'item_name'=>'required',
            'vendor_name'=>'required',
            'billing_amount'=>'required',
            'receipt_no'=>'required',
            'date'=>'required',
            'invoice_no'=>'required',
            'invoice_prefix'=>'required',
            'quality'=>'required',
            'quantity'=>'required',
            'remarks'=>'required',
            'received_by_name'=>'required',
            'document_upload'=>'required'
          
 ]);


     $foodstock = stock::find($stock->id);
     $foodstock->qrcode = $request->input('qrcode');
     $foodstock->item_name = $request->input('item_name');
     $foodstock->vendor_name = $request->input('vendor_name');
     $foodstock->vendor_id = $request->input('vendor_id');
     $foodstock->billing_amount = $request->input('billing_amount');
     $foodstock->receipt_no = $request->input('receipt_no');
     $foodstock->date = $request->input('date');
     $foodstock->invoice_no = $request->input('invoice_no');
     $foodstock->invoice_prefix = $request->input('invoice_prefix');
     $foodstock->quality = $request->input('quality');
     $foodstock->quantity = $request->input('quantity');
     $foodstock->remarks = $request->input('remarks');
     $foodstock->received_by_name = $request->input('received_by_name');
     $foodstock->document_upload = $request->input('document_upload');
     $foodstock->category_id = $request->input('category_id');
     $foodstock->subcategory_id = $request->input('subcategory_id');


        $foodstock->save();
        return redirect()->route('stock.index')
        ->with('success','Stock updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(stock $stock)
    {
        stock::find($stock->id)->delete();
        return redirect()->route('stock.index')
        ->with('success','Stock deleted successfully');
    }
}
