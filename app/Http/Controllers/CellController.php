<?php

namespace App\Http\Controllers;
use App\Entities\cell;
use Illuminate\Http\Request;
use App\Http\Requests\cellrequest;
use Illuminate\Support\Facades\Auth;


class CellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = cell::where('cell_box.branch_id','!=',NULL)->where('cell_box.branch_id','=',$userid->branch_id)->orderBy('id','DESC')->get();
        return view('cell.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cell.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(cellrequest $request)
    {
      $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
      $cell = cell::create($request->all());
       return redirect()->route('cell.index')
       ->with('success','Cell created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(cell $cell)
    {
        return view('cell.edit',compact('cell'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(cell $cell,request $request)
    {

         $this->validate($request, [
            'cell_name'=>'required',
            'max_allowed'=>'required',
            'details'=>'required'
            
        ]);

     $cell->cell_name = $request->input('cell_name');
     $cell->max_allowed = $request->input('max_allowed');
     $cell->details = $request->input('details');

        $cell->save();
        return redirect()->route('cell.index')
        ->with('success','Cell updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(cell $cell)
    {
        cell::find($cell->id)->delete();
        return redirect()->route('cell.index')
        ->with('success','Cell deleted successfully');
    }
}
