<?php
namespace App\Http\Controllers\Api;


use App\Entities\attendence;
use App\Entities\branch;
use App\Entities\leave;
use App\Repositories\AttendenceRepository;
use App\Repositories\LeaveRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Input;
use Log;
use DateTime;

class StaffApiController extends Controller
{
    protected $guard, $response;

    public function __construct(Response $response, Guard $guard)
    {
        $this->response = $response;
        $this->guard = $guard;
    }


    public function staffattedance(Request $request){
       $validator = Validator::make($request->all(), [
          'month' => 'required',
          'year' => 'required',
      ]);
      if ($validator->fails()) {
          $errors = $validator->errors();
          foreach ($errors->all() as $message) {
              $meserror =$message;
          }
        return $this->response->setStatusCode(401,$meserror);
      }else{
         $authicated_user = Auth::user();
         return $this->attedancelist($authicated_user->id,$request->month,$request->year);
      }
    }



   public function staffattedancemonth(Request $request){
       $validator = Validator::make($request->all(), [
          'month' => 'required',
          'year' => 'required',
      ]);
      if ($validator->fails()) {
          $errors = $validator->errors();
          foreach ($errors->all() as $message) {
              $meserror =$message;
          }
        return $this->response->setStatusCode(401,$meserror);
      }else{
        $authicated_user = Auth::user();
         return $this->attedancelist($authicated_user->id,$request->month,$request->year);
      }
    }




    public function logattedance(Request $request){
      $validator = Validator::make($request->all(), [
          'type' => 'required',
          'user_id' => 'required'
      ]);
      if ($validator->fails()) {
          $errors = $validator->errors();
          foreach ($errors->all() as $message) {
              $meserror =$message;
          }

         $this->response->setContent(["error"=>$errors]);
        return $this->response->setStatusCode(401,$meserror);
      }else{
        $userdata = User::find($request->user_id);

        if($request->type == 'punchin'){
            $today=  attendence::whereDate('in_date','=',date('Y-m-d'))->where('user_id','=',$userdata->id)->where('completed','=',0)->orderBy('id','DESC')->first();
          if(isset($today->id)){
            return array('state' => 1 , 'message' => 'please punch out' );
          }else{

            $punch_in = date('Y-m-d H:i:s');
            $in_date = date('Y-m-d');
            $punch_in_time = date('H:i:s');
            $log_punchin =array('punch_in' => $punch_in,
                                'user_id' => $userdata->id,
                                'in_date' => $in_date,
                                'punch_in_time' => $punch_in_time,
                                'branch_id' => $request->branch_id);

             $att = attendence::create($log_punchin);
             return array('state' => 1 ,'logstate' => 'punchin','message' => 'sucesfully punch in' );
          }

        }elseif($request->type == 'punchout'){
            $today=  attendence::whereDate('in_date','=',date('Y-m-d'))->where('user_id','=',$userdata->id)->where('completed','=',0)->orderBy('id','DESC')->first();

          if(isset($today->id)){
            $log_punchout =array('punch_out' => date('Y-m-d H:i:s'),'completed' => 1,'punch_out_time' => date('H:i:s'));
         attendence::find($today->id)->update($log_punchout);
              return array('state' => 0 ,'logstate' => 'punchout','message' => 'sucesfully punch out' );

          }else{
            return array('state' => 0 ,'message' => 'please punch in' );
          }

        }elseif($request->type == 'status'){
            $today=  attendence::where('in_date','=',date('Y-m-d'))->where('user_id','=',$userdata->id)->where('completed','=',0)->get();
          if($today){
            return array('state' => 1,'message'=>'already punched in');
          }else{
            return array('state' => 0,'message'=>'please punched in');
          }

        }
         $meserr = 'invalid type passed';
         return $this->response->setStatusCode(400,$meserr);

      }


    }


   public function addtrace(Request $request){
      $validator = Validator::make($request->all(), [
          'finger_hand' => 'required',
          'finger_keys' => 'required',
          'key' => 'required',
          'user_id' => 'required',
      ]);
      if ($validator->fails()) {
          $errors = $validator->errors();
          foreach ($errors->all() as $message) {
              $meserror =$message;
          }
          $this->response->setContent(["error"=>$errors]);
        return $this->response->setStatusCode(400,$meserror);
      }else{

          if($request->key){

            $authicated_user = User::find($request->user_id);
            //$usertrace =   $this->user->findByAttributes(['user_id'=> $request->user_id]);

            if($authicated_user->fingertrace_status != 1){
                User::find($authicated_user->id)->update(['finger_keys' => $request->finger_keys ,'finger_hand' => $request->finger_hand]);
            }
            return $authicated_user;
          }else{

             $meserr = 'invalid key passed';
             return $this->response->setStatusCode(400,$meserr);

          }
       }
    }



    public function attedancelist($id,$month,$year){

        $dates =array();

        if ($month == 2) {
           $end = 29;
        }else if ($month % 2 == 0) {
            $end = 31;
        }else{
            $end = 32;
        }

        for ($i=1 ; $i < $end ; $i++ ) {
            if($i == 1){
                array_push($dates, 'User_Id');
                array_push($dates, 'Name');
            }
            array_push($dates, $i);

            if($i == $end-1){
                array_push($dates, 'Days');
                array_push($dates, 'Present');
                array_push($dates, 'Absent');
                array_push($dates, 'Leaves');
            }

        }
        $header = $dates;

        $fullattedence =array();
        $fullattedence = ['heading' => $dates];

        if($month < 9 && strlen($month) < 2){
            $month = '0'.$month;
        }
        $productRecset =array();
        $index =0 ;

        $value = User::find($id);


          $productRec =array();
        $date= $year.'-'.$month.'-00';
        $lastdate = $year.'-'.$month.'-31';


        $totalattedence = attendence::select('in_date','punch_in','punch_out','is_absent','user_id')->where('is_absent', '!=', 1)
            ->where('user_id', '=', $value->id)
            ->where('in_date', '>=', $date)
            ->where('in_date', '<=', $lastdate)
            ->orderBy('in_date','asc')
            ->get();

        $totalleaves =leave::select('start_date','end_date','user_id')
            ->where('user_id', '=', $value->id)
            ->where('start_date', '>=', $date)
            ->where('start_date', '<=', $lastdate)
            ->where('status', '=', 2)
            ->orderBy('start_date','asc')
            ->get();

            $staffdetails['name'] = $value->first_name;
            $staffdetails['id'] = $value->id;
            //array_push($productRec,$value->id);
            //array_push($productRec,$value->first_name);
            $variable =array();
            $timecheck = array();
            foreach ($totalattedence as $key) {
                array_push($variable, $key['in_date']);
                if(isset($timecheck[$key['in_date'].'-in']) && $timecheck[$key['in_date'].'-in']){
                    $timecheck[$key['in_date'].'-out'] = $key['punch_out'];
                }else{
                    $timecheck[$key['in_date'].'-in'] = $key['punch_in'];
                    $timecheck[$key['in_date'].'-out'] = $key['punch_out'];
                }
            }
            $leavelist =array();
            $lcount = 0;
            $dot = 0;
            foreach ($totalleaves as $key) {
                array_push($leavelist, $key['start_date']);
                array_push($leavelist, $key['end_date']);
            }
            //print_r($leavelist);die;
            for ($i=1 ; $i < $end ; $i++ ) {

                if($i <= 9){
                    $i = '0'.$i;
                }
                $present = '';
                foreach ($totalleaves as $key) {
                if($year.'-'.$month.'-'.$i >= $key['start_date'] && $year.'-'.$month.'-'.$i <= $key['end_date']){
                    $lcount = $lcount + 1;
                    $present = 'L';
                    break;
                    continue;
                }

                }
                $arraystate = [];
                $ltotime = "00:00:00";
                $lfromtime = "00:00:00";
                $totaltimework = 0;
                if($present != 'L'){
                    if(in_array($year.'-'.$month.'-'.$i, $variable)){
                        $to_time = strtotime($timecheck[$year.'-'.$month.'-'.$i.'-out']);
                        $from_time = strtotime($timecheck[$year.'-'.$month.'-'.$i.'-in']);
                      $ltotime = $timecheck[$year.'-'.$month.'-'.$i.'-out'];
                      $lfromtime = $timecheck[$year.'-'.$month.'-'.$i.'-in'];
                        if($to_time){
                            $present = 'P';
                            $totaltimework = round(($to_time - $from_time)/3600, 1);


                        }else{
                            $present = 'P';

                        }
                        //$present = floor($present / 60);
                    }elseif($year.'-'.$month.'-'.$i > date('Y-m-d')){
                        $present = '---';
                        $dot = $dot + 1;
                    }else{
                        $present = 'A';
                    }
                }

                if($present == 'P'){
                    $arraystate = ['status' => $present,'date' => $year.'-'.$month.'-'.$i,'in_time' => $lfromtime,'out_time' => $ltotime,'totaltimeworkhours' => $totaltimework];
                }else{
                    $arraystate = ['status' => $present,'date' => $year.'-'.$month.'-'.$i];
                }


                array_push($productRec, $arraystate);

            }

            $statistics["totaldays"] = $end-1;
            $statistics["presentdays"] = count($variable);
            $statistics["absentdays"] = $end-$dot-1-count($variable)-$lcount;
            $statistics['leavedays'] = $lcount;
            //array_push($productRec, 'break');

            $productRecset[$index] = $productRec;




        $dateline = $year.'-'.$month;
        $str = $year.'-'.$month;
        $dateline = DateTime::createFromFormat('Y-m', $str);
        $dateline =  $dateline->format('M-Y');

        return ['statistics' => $statistics,'staffdetails' =>$staffdetails ,'attedence' => $productRecset[0],'monthselected' => $dateline];

    }


     public function leavelist(Request $request){
      $authicated_user = Auth::user();
        return leave::where('user_id','=',$authicated_user->id)->paginate();
     }


     public function leavestatistics(Request $request){
        $authicated_user = Auth::user();
        $leavetaken =  leave::where('user_id','=',$authicated_user->id)->where('status','=',2)->count();

        $leavepending =  leave::where('user_id','=',$authicated_user->id)->where('status','=',0)->count();

        $totalleaves = 15;

        $earnedleaves = 4;
        $lossofpay = 2;


        return array('totalleaves' => $totalleaves,'leavetaken' => $leavetaken, 'earnedleaves' => $earnedleaves,'leavepending' => $leavepending,'lossofpay' => $lossofpay);

     }

     public function applyleave(Request $request){

           $validator = Validator::make($request->all(), [
              'start_date' => 'required|date|date_format:Y-m-d',
              'end_date' => 'required|date|date_format:Y-m-d'
          ]);
          if ($validator->fails()) {
              $errors = $validator->errors();
              foreach ($errors->all() as $message) {
                  $meserror =$message;
              }
            return $this->response->setStatusCode(400,$meserror);
          }else{
              $authicated_user = Auth::user();
              $request->merge(['user_id' => $authicated_user->id,'status' => 1]);

              return leave::create($request->all());

            }

      }

    public function branchaccess(Request $request){
        $validator = Validator::make($request->all(), [
            'ime' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror =$message;
            }
            $this->response->setContent(array('message'=> $meserror));
            return $this->response->setStatusCode(400,$meserror);
        }else{
            $branch = branch::where('ime','=',$request->ime)->first();
            if(!isset($branch->id)){
                $this->response->setContent(array('message'=> 'Invalid ime'));
                return $this->response->setStatusCode(400,'Invalid ime');
            }
            $token =  Auth::guard('api')->generateTokenById($branch->id);

            $userlist = User::where('access_key','=',$request->password)->first();

            if(!isset($userlist->id))
            {
                $this->response->setContent(array('message'=> 'Invalid Key'));
                return $this->response->setStatusCode(400,'Invalid Key');
            }

            $token =  Auth::guard('api')->generateTokenById($branch->id);
            return response(json_encode(['token' => $token]))->header('Content-Type', 'application/json');
        }
    }

    public function branchdetails(Request $request){
        $validator = Validator::make($request->all(), [
            'ime' => 'required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror =$message;
            }
            $this->response->setContent(array('message'=> $meserror));
            return $this->response->setStatusCode(400,$meserror);
        }else{
            $branch = branch::where('ime','=',$request->ime)->first();
            if(empty($branch)){
                return ['status' => 0 ,'message' => 'invalid ime'];
            }

            $token =  Auth::guard('api')->generateTokenById($branch->id);

            return response(json_encode(['branch' => $branch,'status' => 1,'token' => $token]))->header('Content-Type', 'application/json');
        }
    }

    public function staffassigns(Request $request){

        $validator = Validator::make($request->all(), [
            'key' => 'required:exists:users,id'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror =$message;
            }
            $this->response->setContent(array('message'=> $meserror));
            return $this->response->setStatusCode(400,$meserror);
        }else{


            $userlist = User::where('users.access_key','=',$request->key)->leftjoin('staff_attedances',function($jointhree) use($request)
            {
                $jointhree->on('staff_attedances.user_id', '=', 'users.id');
                $jointhree->where('in_date','=',date('Y-m-d'));

            })->select('first_name','last_name','users.id','emp_id','staff_attedances.punch_in_time','staff_attedances.punch_out_time','staff_attedances.in_date','staff_attedances.punch_out','finger_keys','finger_hand','completed')->first();

            if(isset($userlist->id)){
                return $userlist;

            }else{
                $this->response->setContent(array('message'=> 'Invalid Key'));
                return $this->response->setStatusCode(400,'Invalid Key');
            }

        }
    }


    public function todayattedancelist(Request $request){

        $dates =array();

        
        $date= $request->selectday;
        $lastdate = $request->selectday;

        $staffs = User::select('profileImg','first_name','last_name','id','phone')->get();
        $totalattedence = attendence::select('in_date','punch_in','punch_out','is_absent','user_id')->where('is_absent', '!=', 1)
            ->where('in_date', '>=', $date)
            ->where('in_date', '<=', $lastdate)
            ->orderBy('in_date','asc')
            ->get();

        $totalleaves =leave::select('start_date','end_date','user_id')
            ->where('start_date', '>=', $date)
            ->where('start_date', '<=', $lastdate)
            ->where('status', '=', 2)
            ->orderBy('start_date','asc')
            ->get();

        foreach ($staffs as $staff) {

            $staff->attedance = 'Absent';
            $staff->punch_in = '';
            $staff->punch_out = '';
            foreach ($totalattedence as $attedence) {
                  
                  if($staff->id == $attedence->user_id && $attedence->is_absent == 0){

                     $staff->attedance = 'Present';
                     $staff->punch_in = $attedence->punch_in ;
                     $staff->punch_out = $attedence->punch_out;

                  }else{

                    $staff->attedance = 'Absent';

                  }

                  foreach ($totalleaves as $totalleave) {
                      
                      if($staff->id == $totalleave->user_id){

                         $staff->attedance = 'Leave';

                      }
                  }
            }
        }
            

        return ['staffs' => $staffs];

    }

}
