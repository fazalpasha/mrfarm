<?php

namespace App\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Input;
use Log;
use Validator;
use Image;
use App\Entities\category;
use App\Entities\subcategory;
use App\Entities\cell;
use App\Entities\topcategory;
use App\Entities\animalstock;
use App\Entities\foodstock;
use App\Entities\vendor;
use App\Entities\qrcode;
use App\Entities\sales;
use App\Feedchart;
use App\Entities\production;
use App\Entities\entrylogs;
use App\Entities\qrcodeconnection;
use App\Entities\Notice;
use App\Entities\salesdetail;
use App\User;
use App\Otp;


class BasicController extends Controller
{
    protected $guard, $response;

    public function __construct(Response $response, Guard $guard)
    {
        $this->response = $response;
        $this->guard = $guard;
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('error' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'phone';
            $request->merge([
                $login_type => $request->input('login')
            ]);
            if (Auth::attempt([$login_type => $request->input('login'),'password' => $request->password])) {
                $authicated_user = Auth::user();
                if ($authicated_user) {
                    $last_login = $authicated_user->last_login;
                    Auth::user()->last_login = new \DateTime();
                    $token = Auth::guard('api')->generateTokenById($authicated_user->id);
                    Auth::User()->authtoken = $token;
                    $authicated_user->token = $token;
                    $authicated_user = json_decode($authicated_user, true);
                    $response = array();
                    $response['id'] = $authicated_user['id'];
                    $response['email'] = $authicated_user['email'];
                    $response['address'] = $authicated_user['address'];
                     $response['photo'] = $authicated_user['profileImg'];
                     $response['first_name'] = $authicated_user['first_name'];
                     $response['last_name'] = $authicated_user['last_name'];
                     $response['phone'] = $authicated_user['phone'];

                    $response['token'] = $authicated_user['token'];

                    return response(json_encode($response))->header('Content-Type', 'application/json');
                } else {
                    $this->response->setContent(array('message' => 'Please Activate your account'));
                    return $this->response->setStatusCode(400, 'Please Activate your account');
                }
            }else{
                $this->response->setContent(array('message' => 'Email or Password is invalid'));
                return $this->response->setStatusCode(400, 'Email or Password is invalid');
            }
        }
        $this->response->setContent(array('message' => 'Email or Password is invalid'));
        return $this->response->setStatusCode(401, 'Email or Password is invalid');
    }

    public function userlists(Request $request){

        $lists = User::where('status','=',1)->select('first_name','last_name','id','profileImg')->get();

         $this->response->setContent(array('lists' => $lists,'status' => 1));
         return $this->response->setStatusCode(200, 'OK');

    }
    

    public function profile(Request $request){
        return Auth::user();
    }


    public function dutystatus(Request $request){

       $validator = Validator::make($request->all(), [
            'lat'=>'required',
            'lng'=>'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        } else {
          $users = Auth::user();

            $this->response->setContent(array('status' => 1,'duty_status' => $users->duty_status));
            return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function TopCategoryCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'topcategory_name'=>'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $topcategory = topcategory::create(['topcategory_name'=>$request->topcategory_name,'status'=>1]);
            $this->response->setContent(array('message' => 'TopCategory Created Successfully'));
            return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function TopCategoryEdit(Request $request){
        $validator = Validator::make($request->all(), [
        'topcategory_id'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $topcategoryedit = topcategory::where(['id'=>$request->topcategory_id])->first();
            return $topcategoryedit;
        }

    }

    public function TopCategoryUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            'id'=>'required',
            'topcategory_name'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $topcategoryupdate = topcategory::where(['id'=>$request->id])->update(['topcategory_name'=>$request->topcategory_name,'status'=>1]);

            return topcategory::where('id','=',$request->id)->first();
        }
        
    }


    public function updatestatus(Request $request){
      
           $user = Auth::user();
           $user->duty_status = !$user->duty_status;
           $user->save();

           $this->response->setContent(array('status' => 1,'message' => 'Updated Successfully','duty_status' => $user->duty_status));
            return $this->response->setStatusCode(200, 'OK');


    }

    public function Entrylogadd(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
            'last_name'=>'required',
            'phone'=>'required',
            'reason'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $log = entrylogs::create($request->all());


             $this->response->setContent(array('status' => 1,'message' => 'Log Created Successfully'));
            return $this->response->setStatusCode(200, 'OK');
            
        }
        
    }


    public function Entryloglist(Request $request){
       
            $logs = entrylogs::paginate();


             $this->response->setContent($logs);
            return $this->response->setStatusCode(200, 'OK');
            
        
        
    }

    

     public function TopCategoryList(Request $request)
    {
        $topcategorylist = topcategory::where(['status'=>1])->get();
        if(count($topcategorylist) > 0){
         return response()->json(compact('topcategorylist'),200);
        }else{
        return response()->json(['message' => 'No TopCategory List','status' => 0],402);
        }
    }


     public function CategoryCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name'=>'required',
            'topcategory_id'=>'required',    
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $category = category::create(['category_name'=>$request->category_name,'type'=>$request->topcategory_id,'status'=>1]);
            $this->response->setContent(array('message' => 'Category Created Successfully'));
            return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function CategoryEdit(Request $request){
        $validator = Validator::make($request->all(), [
        'category_id'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $categoryedit = category::where(['id'=>$request->category_id])->first();
            return $categoryedit;
        }

    }

    public function CategoryUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            'id'=>'required',
            'category_name'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $categoryupdate = category::where(['id'=>$request->id])->update(['category_name'=>$request->category_name,'status'=>1]);

            return category::where('id','=',$request->id)->first();
        }
        
    }

    public function CategoryFilter(Request $request)
    {   

        if(isset($request->type)){

            $topcategory = TopCategory::where('topcategory_name','=',$request->type)->first();

            if(isset($topcategory->id)){
                 $categorylist = category::where(['status'=>1,'type' => $topcategory->id])->get();
             }else{
                $categorylist = category::where(['status'=>1])->get();
             }
           
        }else{
            $categorylist = category::where(['status'=>1])->get();
        }
        
     

        if(count($categorylist) > 0){
         return response()->json(compact('categorylist'),200);
        }else{
        return response()->json(['message' => 'No Category List','status' => 0],402);
        }
    }

     public function CategoryList(Request $request)
    {   

        if(isset($request->type)){

            $topcategory = TopCategory::where('topcategory_name','=',$request->type)->first();

            if(isset($topcategory->id)){
                 $categorylist = category::where(['status'=>1,'type' => $topcategory->id])->get();
             }else{
                $categorylist = category::where(['status'=>1])->get();
             }
           
        }else{
            $categorylist = category::where(['status'=>1])->get();
        }
        
        foreach ($categorylist as $category) {
            $category->subcategory;
        }

        $vendorlist = vendor::all();
        $celllist = cell::all();
        $units =['Kg','Liters','Ton','Box','Counts','Items'];
        $unitslist = [];
        foreach ($units as $unit) {
            $newarray = ['name' => $unit];
            array_push($unitslist, $newarray);
        }

        if(count($categorylist) > 0){
         return response()->json(compact('categorylist','vendorlist','celllist','unitslist'),200);
        }else{
        return response()->json(['message' => 'No Category List','status' => 0],402);
        }
    }

      public function SubCategoryCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subcategory_name'=>'required',
            'category_id'=>'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $subcategory = subcategory::create(['subcategory_name'=>$request->subcategory_name,'category_id'=>$request->category_id,'status'=>1]);
            $this->response->setContent(array('message' => 'SubCategory Created Successfully'));
            return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function SubCategoryEdit(Request $request){
        $validator = Validator::make($request->all(), [
        'subcategory_id'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $subcategoryedit = subcategory::where(['id'=>$request->subcategory_id])->first();
            return $subcategoryedit;
        }

    }

     public function SubCategoryUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            'id'=>'required',
            'subcategory_name'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $categoryupdate = subcategory::where(['id'=>$request->id])->update(['subcategory_name'=>$request->subcategory_name,'status'=>1]);

            return subcategory::where('id','=',$request->id)->first();
        }
        
    }


      public function SubCategoryList(Request $request)
    {
        $subcategorylist = subcategory::where(['status'=>1])->get();
        if(count($subcategorylist) > 0){
         return response()->json(compact('subcategorylist'),200);
        }else{
        return response()->json(['message' => 'No SubCategory List','status' => 0],402);
        }
    }


      public function CellBoxCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cell_name'=>'required',
            'max_allowed'=>'required',
            'details'=>'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $cellbox = cell::create(['cell_name'=>$request->cell_name,'max_allowed'=>$request->max_allowed,'details'=>$request->details]);
            $this->response->setContent(array('message' => 'Cellbox Created Successfully'));
            return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function CellBoxEdit(Request $request){
        $validator = Validator::make($request->all(), [
        'cellbox_id'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $cellboxedit = cell::where(['id'=>$request->cellbox_id])->first();
            return $cellboxedit;
        }

    }

     public function CellBoxUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            'id'=>'required',
            'cell_name'=>'required',
            'max_allowed'=>'required',
            'details'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $cellboxupdate = cell::where(['id'=>$request->id])->update(['cell_name'=>$request->cell_name,'max_allowed'=>$request->max_allowed,'details'=>$request->details]);

            return cell::where('id','=',$request->id)->first();
        }
        
    }

     public function CellBoxList(Request $request)
    {
        $cellboxlist = cell::get();
        if(count($cellboxlist) > 0){
         return response()->json(compact('cellboxlist'),200);
        }else{
        return response()->json(['message' => 'No CellBox List','status' => 0],402);
        }
    }

    public function CellList(Request $request)
    {
        $cellboxlist = cell::paginate();
        if(count($cellboxlist) > 0){
         return response()->json(compact('cellboxlist'),200);
        }else{
        return response()->json(['message' => 'No CellBox List','status' => 0],402);
        }
    }


     public function CellBoxreport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cell_id'=>'required'
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $new =[];
            $newp =[];
            $newfeed=[];
            $category = category::get();
            foreach ($category as $cat) {

               $cellboxlistcount = animalstock::where('cell_number','=',$request->cell_id)->leftjoin('subcategory','subcategory.id','=','animalstock.subcategory_id')->where('subcategory.category_id','=',$cat->id)->count();

               $cellboxlistname = $cat->category_name;

               array_push($new, ['count' => $cellboxlistcount,'name' => $cellboxlistname]);

            }


            foreach ($category as $cat) {

               $cellboxlistcount = production::where('cell_id','=',$request->cell_id)->leftjoin('subcategory','subcategory.id','=','production.subcategory_id')->where('subcategory.category_id','=',$cat->id)->count();

               $cellboxlistname = $cat->category_name;

               array_push($newp, ['count' => $cellboxlistcount,'name' => $cellboxlistname]);

            }


            foreach ($category as $cat) {

               $cellboxlistcount = Feedchart::where('cell_number','=',$request->cell_id)->leftjoin('subcategory','subcategory.id','=','feedcharts.subcategory_id')->where('subcategory.category_id','=',$cat->id)->count();

               $cellboxlistname = $cat->category_name;

               array_push($newfeed, ['count' => $cellboxlistcount,'name' => $cellboxlistname]);

            }



            $data = ['animal' => $new,'production' => $newp,'newfeed'=>$newfeed];
            


      
             return response()->json(compact('data'),200);
            

        }
    }


      public function QRCodeList(Request $request)
    {
         $qrcodelist = qrcode::get();
        if(count($qrcodelist) > 0){
         return response()->json(compact('qrcodelist'),200);
        }else{
        return response()->json(['message' => 'No CellBox List','status' => 0],402);
        }
        
    }


    public function AnimalStockCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id'=>'required',
            'subcategory_id'=>'required',
            'cell_id'=>'required',
            'qrcode'=>'required',
            'total_weight'=>'required',
            'vendor_id'=>'required',
            'billing_amount'=>'required',
            'date'=>'required',
            'remarks'=>'required',
            'health_check'=>'required',
            'physically_cahllenge'=>'required',
            'any_allergy'=>'required',
            'pregnent'=>'required',  

        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $cellbox = animalstock::create(['category_id'=>$request->category_id,'subcategory_id'=>$request->subcategory_id,'cell_number'=>$request->cell_id,'qrcode'=>$request->qrcode,'total_weight'=>$request->total_weight,'vendor_id'=>$request->vendor_id,'date'=>$request->date,'remarks'=>$request->remarks,'health_check'=>$request->health_check,'physically_cahllenge'=>$request->physically_cahllenge,'any_allergy'=>$request->any_allergy,'pregnent'=>$request->pregnent,'verified'=>1]);

            $qrdetails = qrcode::where('qr_code','=',$request->qrcode)->find();

            qrcodeconnection::create(['animal_id' => $cellbox->id,'qr_id' => $qrdetails->id,'status' => 1]);

        $this->response->setContent(array('message' => 'Animal Stock Created Successfully'));
        return $this->response->setStatusCode(200, 'OK');
        }
    }

     public function AnimalStockEdit(Request $request){
        $validator = Validator::make($request->all(), [
        'animalstock_id'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $animalstaockedit = animalstock::where(['id'=>$request->animalstock_id])->first();
            return $animalstaockedit;
        }

    }

     public function AnimalStockUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            'id'=>'required',
            'qrcode'=>'required',
            'total_weight'=>'required',
            'vendor_name'=>'required',
            'vendor_id'=>'required',
            'billing_amount'=>'required',
            'receipt_no'=>'required',
            'date'=>'required',
            'remarks'=>'required',
            'health_check'=>'required',
            'physically_cahllenge'=>'required',
            'any_allergy'=>'required',
            'pregnent'=>'required',
            'verified'=>'required',
        ]);
         if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        }else {
            $animalstaockupdate = animalstock::where(['id'=>$request->id])->update(['qrcode'=>$request->qrcode,'total_weight'=>$request->total_weight,'vendor_name'=>$request->vendor_name,'vendor_id'=>$request->vendor_id,'billing_amount'=>$request->billing_amount,'receipt_no'=>$request->receipt_no,'date'=>$request->date,'remarks'=>$request->remarks,'health_check'=>$request->health_check,'physically_cahllenge'=>$request->physically_cahllenge,'any_allergy'=>$request->any_allergy,'pregnent'=>$request->pregnent,'verified'=>$request->verified]);

            return animalstock::where('id','=',$request->id)->first();
        }
        
    }

     public function AnimalStockList(Request $request)
    {

        $animalstocklist = animalstock::leftjoin('subcategory','subcategory.id','=','animalstock.subcategory_id')->leftjoin('category','category.id','=','subcategory.category_id')->select('subcategory.subcategory_name','category.category_name','category.image','animalstock.*')->paginate(25);


        if(count($animalstocklist) > 0){
         return response()->json(compact('animalstocklist'),200);
        }else{
        return response()->json(['message' => 'No Animalstock List','status' => 0],402);
        }
    }

    public function qrscaned(Request $request)
    {

        $details = animalstock::leftjoin('subcategory','subcategory.id','=','animalstock.subcategory_id')->leftjoin('category','category.id','=','subcategory.category_id')->where('animalstock.qrcode','=',$request->qrcode)->select('subcategory.subcategory_name','category.category_name','category.image','animalstock.*')->first();

        //->leftjoin('qr_code','qr_code.id','=','qr_connection.qr_id')

        if(isset($animalstocklist->id) > 0){
          return response()->json(compact('details'),200);
        }else{
        return response()->json(['message' => 'No Animal found','status' => 0],402);
        }
    }


    public function animaldetails(Request $request)
    {

        $details = animalstock::leftjoin('subcategory','subcategory.id','=','animalstock.subcategory_id')->leftjoin('category','category.id','=','subcategory.category_id')->where('animalstock.id','=',$request->animal_id)->select('subcategory.subcategory_name','category.category_name','category.image','animalstock.*')->first();


        if(isset($details->id) > 0){
          return response()->json(compact('details'),200);
        }else{
        return response()->json(['message' => 'No Animal found','status' => 0],400);
        }
    }


    public function reports(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year'=>'required',
            'month'=>'required'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $reports1 = ['name' => 'Total Animal','value' => '111'];

            $reports2 = ['name' => 'Foodfeeded','value' => '141'];


            $reports3 = ['name' => 'Foodfeeded','value' => '131'];

            $reports4 = ['name' => 'totalsales','value' => '151'];

            $reports5 = ['name' => 'Totalresale','value' => '181'];


        $this->response->setContent(array('reports' => [$reports1,$reports2,$reports3,$reports4,$reports5],'status' => 1));
        return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function FoodStockCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'item_name'=>'required',
            'subcategory_id'=>'required',
            'vendor_name'=>'required',
            'vendor_id'=>'required',
            'billing_amount'=>'required',
            'quantity'=>'required',
            'units'=>'required',
            'date'=>'required',
            'remarks'=>'required',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $newrow = foodstock::create($request->all());

        $this->response->setContent(array('message' => 'Animal Stock Created Successfully'));
        return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function FoodStockList(Request $request)
    {

         $newrows = foodstock::leftjoin('subcategory','subcategory.id','=','foodmedicalstock.subcategory_id')->leftjoin('category','category.id','=','subcategory.category_id')->select('subcategory.subcategory_name','category.category_name','category.image','foodmedicalstock.*')->paginate();

        $this->response->setContent($newrows);
        return $this->response->setStatusCode(200, 'OK');
    }
     

    public function FoodStockDetails(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'stock_id'=>'required',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;

            }

             $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        }else {

            $newrow = foodstock::find($request->stock_id);
       
            $this->response->setContent(['status' => 1,'details' => $newrow]);
             return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function FoodStockUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'item_name'=>'required',
            'subcategory_id'=>'required',
            'vendor_name'=>'required',
            'vendor_id'=>'required',
            'billing_amount'=>'required',
            'quantity'=>'required',
            'units'=>'required',
            'date'=>'required',
            'remarks'=>'required',
            'stock_id'=>'required',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $id = $request->stock_id;
            $updatearray = ['item_name' => $request->item_name,'vendor_name'=> $request->vendor_name,'remarks'=> $request->remarks,'quantity'=>$request->quantity,'billing_amount'=>$request->billing_amount,'units'=>$request->units,'subcategory_id'=> $request->subcategory_id];
            $newrow = foodstock::where(['id' => $request->stock_id])->update($updatearray);

        $this->response->setContent(array('message' => 'Animal Stock Updated Successfully','status' =>1));
        return $this->response->setStatusCode(200, 'OK');
        }
    }


    public function FeedchartCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subcategory_id'=>'required',
            'quantity'=>'required',
            'units'=>'required',
            'date_given'=>'required',
            'cell_number' => 'required'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $user = Auth::user();
            $createarray = ['quantity'=>$request->quantity,'units'=>$request->units,'subcategory_id'=> $request->subcategory_id,'date_given' => $request->date_given,'user_id' => $user->id,'cell_number' => $request->cell_number];
            $newrow = Feedchart::create($createarray);

        $this->response->setContent(array('message' => 'Animal Feed Added Successfully','status' =>1));
        return $this->response->setStatusCode(200, 'OK');
        }
    }

    public function DailyFeedList(Request $request)
    {

         $newrows = Feedchart::leftjoin('subcategory','subcategory.id','=','feedcharts.subcategory_id')->leftjoin('cell_box','cell_box.id','=','feedcharts.cell_number')->leftjoin('category','category.id','=','subcategory.category_id')->select('subcategory.subcategory_name','category.category_name','category.image','cell_box.cell_name','feedcharts.*')->paginate();

        $this->response->setContent($newrows);
        return $this->response->setStatusCode(200, 'OK');
    }


    public function qranimal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'qrcode'=>'required',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

               $code = qrcodeconnection::leftjoin('qr_code','qr_code.id','=','qr_connection.qr_id')->leftjoin('animalstock','qr_connection.animal_id','=','animalstock.id')->leftjoin('subcategory','subcategory.id','=','animalstock.subcategory_id')->leftjoin('category','category.id','=','subcategory.category_id')->leftjoin('config','category.id','=','config.category_id')->select('subcategory.subcategory_name','category.category_name','category.image','config.price','animalstock.*')->where('animalstock.qrcode','=',$request->qrcode)->first();


               if(isset($code->id)){
                    $this->response->setContent(array('details' => $code,'status' =>1));
               }else{

                  $this->response->setContent(array('details' => $code,'status' =>0,'message' => 'Invalide Code Try Again'));
               }
               
               return $this->response->setStatusCode(200, 'OK');

        }

    }




    public function salestore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gst'=>'required',
            'vendor_name'=>'required',
            'vendor_address'=>'required',
            'vendor_phone'=>'required',
            'total_price' => 'required',
            'sub_price' => 'required',
            'gst_price' => 'required',
            'sales' => 'required',
            'total_animal' => 'required',
            'discount_amount' => 'required',
            'amount_paid' => 'required'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $userid = Auth::user();
            $request->merge(['user_id'=>$userid->id,'status'=>1]);
            $gettopcat = topcategory::where('topcategory_name','=','Animal')->first();
            $request->merge(['topcat_id'=>$gettopcat->id]);
            if($request->top_id == "Animal"){
                $request->merge(['total_weight'=>0]);
            }
            $sale = sales::create($request->all());
            if($request->top_id == "Animal") {
                $totalweight = array();
                $salesdetail = "";
                $salesdetail = $request->input('sales');
                foreach ($salesdetail as $sales) {
                    salesdetail::create(['animal_id' => $sales['acode'], 'price' => $sales['price'], 'weight' => $sales['weight'], 'sales_id' => $sale->id]);
                    array_push($totalweight,$sales['weight']);
                }

                sales::where('id', '=', $sale->id)->update(['total_weight'=>array_sum($totalweight)]);
            }

            $this->response->setContent(array('details' => 'Added Successfully','status' =>1));
            return $this->response->setStatusCode(200, 'OK');
        }
    }


    public function resalestore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gst'=>'required',
            'vendor_name'=>'required',
            'vendor_address'=>'required',
            'vendor_phone'=>'required',
            'total_price' => 'required',
            'sub_price' => 'required',
            'gst_price' => 'required',
            'total_animal' => 'required',
            'discount_amount' => 'required',
            'amount_paid' => 'required',
            'items' => 'required'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {



            $userid = Auth::user();
            $request->merge(['user_id'=>$userid->id,'status'=>1]);
            $gettopcat = topcategory::where('topcategory_name','=','Animal')->first();
            $request->merge(['topcat_id'=>$gettopcat->id]);
           
            $sale = sales::create($request->all());

            $totalweight = array();
            $salesdetail = "";
            $salesdetail = $request->input('items');
            foreach ($salesdetail as $sales) {
                salesdetail::create(['name' => $sales['name'], 'price' => $sales['price'], 'qty' => $sales['qty'], 'sales_id' => $sale->id]);
                
            }
            
            $this->response->setContent(array('details' => 'Added Successfully','status' =>1));
            return $this->response->setStatusCode(200, 'OK');
        }
    }


    public function salesdetails(Request $request){

        $validator = Validator::make($request->all(), [
            'sale_id'=>'required',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $data = sales::leftJoin('top_categories','sales.topcat_id','=','top_categories.id')->leftJoin('users','sales.user_id','=','users.id')->select('top_categories.topcategory_name as itemtype','users.username as addbyname','sales.*')->first();

            $data->saleitems;

            $this->response->setContent(array('details' => $data,'status' =>1));
                return $this->response->setStatusCode(200, 'OK');
        }

    }


    public function saleslist(Request $request){

        $validator = Validator::make($request->all(), [
            'month'=>'required',
            'year'=>'required'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $data = sales::leftJoin('top_categories','sales.topcat_id','=','top_categories.id')->leftJoin('users','sales.user_id','=','users.id')->where('sales.status','=',1)->whereMonth('sales.created_at','=',$request->month)->whereYear('sales.created_at','=',$request->year)->select('top_categories.topcategory_name as itemtype','users.username as addbyname','sales.*')->paginate(25);


            $this->response->setContent(array('details' => $data,'status' =>1));
                return $this->response->setStatusCode(200, 'OK');
        }

    }


    public function ProductionCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subcategory_id'=>'required',
            'quantity'=>'required',
            'units'=>'required',
            'date'=>'required',
            'cell_id' => 'required'

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $user = Auth::user();
            $request->merge(['added_by' => $user->id]);
            $newrow = production::create($request->all());

          $this->response->setContent(array('message' => 'Added Successfully','status' =>1));
          return $this->response->setStatusCode(200, 'OK');
        }
    }


    public function createsales(Request $request)
    {
        $vendors = vendor::all();
        $nogst = ['name' => 'No GST','value' => 0];
        $gst = ['name' => 'GST','value' => 18];

        $payment_type1 = ["method" => "Cash"];
        $payment_type2 = ["method" => "Card"];
        $payment_type3 = ["method" => "Google Pay"];
        $payment_type4 = ["method" => "PhonePe"];
        $payment_type5 = ["method" => "NetBanking"];
        $payment_type6 = ["method" => "Cheque"];

        $this->response->setContent(array('data' =>['vendors' => $vendors,'gst' => [$nogst,$gst],'payment_method' => [$payment_type1,$payment_type2,$payment_type3,$payment_type4,$payment_type5,$payment_type6]] ,'status' =>1));

        return $this->response->setStatusCode(200, 'OK');

    }

    public function ProductionList(Request $request)
    {

         $newrows = production::leftjoin('subcategory','subcategory.id','=','production.subcategory_id')->leftjoin('cell_box','cell_box.id','=','production.cell_id')->leftjoin('category','category.id','=','subcategory.category_id')
             ->leftjoin('users','production.added_by','=','users.id')->select('subcategory.subcategory_name','category.category_name','category.image','cell_box.cell_name','users.username as addby_name','production.*')->paginate();

        $this->response->setContent($newrows);
        return $this->response->setStatusCode(200, 'OK');
    }


    public function noticelist(Request $request){
       $lists =  Notice::paginate();

       foreach ($lists as $list) {
            $list->touser;
            $list->fromuser;
           # code...
       }
       $this->response->setContent(array('details' => $lists,'status' =>1));
        return $this->response->setStatusCode(200, 'OK');
    }


     public function uploadimg(Request $request){

        
        

            $imagepath ='/assets/media/upload.png';
            if(isset($request->photo) && $request->photo){
                $images = $request->file('photo');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = Image::make($images->getRealPath());
                $destinationPath = public_path('assets/media');
                $images->move($destinationPath, $input['imagename']);
                $imagepath = "assets/media/" . $input['imagename'];

            }


            $this->response->setContent(array('status' =>1,'imagepath' => $imagepath));
            return $this->response->setStatusCode(200, 'OK');

     }


    public function noticecreate(Request $request){

        $validator = Validator::make($request->all(), [
            'sub_title'=>'required',
            'description'=>'required',
            'date'=>'required',
            'is_event'=>'required',
            'to_all'=>'required',

        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $errors));
            return $this->response->setStatusCode(400, $meserror);
        } else {

            $user = Auth::user();
            


            $imagepath ='/assets/media/upload.png';
            if(isset($request->photo) && $request->photo){
               
                $imagepath = $request->photo;

            }

            $request->merge(['from_user_id' => $user->id,'image' =>  $imagepath]);

            Notice::create($request->all());

            $this->response->setContent(array('message' => 'Created Successfully','status' =>1));
            return $this->response->setStatusCode(200, 'OK');

        }
    }


    public function forgotsendotp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:users,phone'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $otp = $this->OtpVerify($request->phone);
            if (isset($otp->id)) {
                $msg = 'Your OTP :' . $otp->otp;
                $this->smsSend($request->phone, $msg, $otp->countryCode,$otp->otp);

                $request->merge(['message' => 'OTP Successfully Sent', 'status' => 1,'otp' => $otp->otp]);
                return response(json_encode($request->all()))->header('Content-Type', 'application/json');
            }

            $this->response->setContent(array('message' => 'Something went wrong'));
                    return $this->response->setStatusCode(400, 'Something went wrong');
        }

    }

    public function forgotverifyotp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:users,phone',
            'otp' => 'required',
            'password' => 'required|confirmed|min:4'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $meserror = $message;
            }
            $this->response->setContent(array('message' => $message));
            return $this->response->setStatusCode(400, $meserror);
        } else {
            $check = $this->checkotp($request->phone, $request->otp);
            if ($check['status'] == 1) {
                $password = $request->password;
                $guestpsw = Hash::make($request->password);


                User::where('phone','=',$request->phone)->update(['password' => $guestpsw]);

                $request->merge(['message' => 'Successfully Updated', 'status' => 1]);
                return response(['phone' => $request->phone,'status' => 1,'message' => 'Successfully Updated'])->header('Content-Type', 'application/json');
            }

            $this->response->setContent(array('message' => 'Something went wrong'));
                    return $this->response->setStatusCode(400, 'Something went wrong');
        }

    }

    public function checkotp($phone, $otp)
    {
        $otpexit = Otp::orderBy('id', 'DESC')->where('phone', '=', $phone)->where('otp', '=', $otp)->where('exp_time', '>=', date('Y-m-d h:i:s'))->first();
        if (isset($otpexit->id)) {
            return ["status" => 1];
        }
        return ["status" => 0];
    }

    public function OtpVerify($phone)
    {
        $randnum = rand(1111, 9999);
        $selectedTime = date('h:i:s');
        $endTime = strtotime($selectedTime) + 900;
        $newTime = date('Y-m-d h:i:s', $endTime);
        //return $newTime;
        $otpsend = Otp::create(['phone' => $phone, 'otp' => $randnum, 'countryCode' => '91', 'exp_time' => $newTime]);
        return $otpsend;
    }

    public function curlRequest($url, $headers, $curlOptions, $method, $data)
    {

        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_URL, $url);

        if (!empty($curlOptions)) {
            curl_setopt_array($this->ch, $curlOptions);
        }

        if ($method == 'post') {
            curl_setopt($this->ch, CURLOPT_POST, 1);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        } elseif ($method == 'put') {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($this->ch, CURLOPT_POSTFIELDS,http_build_query($data));
        } elseif ($method !== 'get') {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        }
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($this->ch);
        curl_close($this->ch);
        return $response;

    }

    public function smsSend($number,$text,$country_code,$otp){

        $method ="get";
        // $url = "http://bhashsms.com/api/sendmsg.php?user=Poornesh&pass=posh@1234&sender=CARAPP&priority=ndnd&stype=normal&phone=$country_code.$number&text=$text'";

       $url =  "https://api.msg91.com/api/v5/otp?authkey=135926AxYau7H4YG4K586b60cb&template_id=5e4d7fe8d6fc054c5d0fd9b8&mobile=".$number."&invisible=1&otp=".$otp;

        //        $url =  "https://control.msg91.com/api/sendhttp.php?authkey=135926AxYau7H4YG4K586b60cb&mobiles=".$number."&message=".$text."&sender=BSPYDR&route=4&country=".$country_code."";
        return $this->curlRequest($url,[],false,$method,null);
    }

}