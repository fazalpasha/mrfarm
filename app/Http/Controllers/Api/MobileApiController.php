<?php
namespace App\Http\Controllers\Api;


use App\Entities\attendence;
use App\Entities\branch;
use App\Entities\leave;
use App\Repositories\AttendenceRepository;
use App\Repositories\LeaveRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Input;
use Log;
use DateTime;

class MobileApiController extends Controller
{
    protected $guard, $response;

    public function __construct(Response $response, Guard $guard)
    {
        $this->response = $response;
        $this->guard = $guard;
    }

    public function stafflist(Request $request){

        $users = User::select('id','first_name','last_name','phone','emp_id','age','service_dept','profileImg')->paginate();

       return response(['status' => 1,'data' => $users])->header('Content-Type', 'application/json');
  
    }

    public function staffdetails(Request $request){
       $validator = Validator::make($request->all(), [
          'staff_id' => 'required'
      ]);
      if ($validator->fails()) {
          $errors = $validator->errors();
          foreach ($errors->all() as $message) {
              $meserror =$message;
          }
        $this->response->setContent(array('message'=> $errors));
        return $this->response->setStatusCode(400,$meserror);
      }else{

      	 $users = User::find($request->staff_id);

       	return response(['status' => 1,'data' => $users])->header('Content-Type', 'application/json');
  
   			
      }
    }

 }