<?php

namespace App\Http\Controllers;

use App\Entities\expense;
use App\Entities\category;
use App\Entities\subcategory;
use App\Entities\vendor;
use App\Entities\cell;
use App\Http\Requests\expenserequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\productionrequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Image;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
        $data = expense::leftJoin('users','expense.added_by','=','users.id')->where('expense.branch_id','!=',NULL)->where('expense.branch_id','=',$userid->branch_id)->select('users.username','expense.*')->orderBy('id','DESC')->get();
        return view('expense.index',compact('data'));

    }

    public function Filter(Request $request)
    {
        $userid = Auth::user();
        $production  = expense::leftJoin('users','expense.added_by','=','users.id')->leftJoin('vendor','expense.vendor_id','=','vendor.id')->where('expense.branch_id','!=',NULL)->where('expense.branch_id','=',$userid->branch_id)->where(function ($production) use ($request) {
            if($request->filter_category != ""){
                $production->where('expense.payment_method','=',$request->filter_category);
            }
            if($request->date_from != "" || $request->date_to != ""){
                $production->whereBetween('expense.date',[$request->date_from,$request->date_to]);
            }
        })->select('vendor.vendor_name','users.last_name','users.first_name','expense.*')->get();

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval(count($production)),
            "recordsFiltered" => intval(count($production)),
            "data"            => $production,
            "grandtotal" =>$production->sum('total')
        );
        return json_encode($json_data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendor = vendor::all();
        $userv=user::all();
        return view('expense.create',compact('vendor','userv'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(expenserequest $request)
    {
        $userid = Auth::user();
      $request->merge(['status'=>1,'branch_id'=>$userid->branch_id]);
        $newfile = '/assets/media/upload.png';
        if(isset($request->image) && $request->image) {
            $images = $request->file('image');
            $input['imagename'] ="";
            $randnum = rand(11111111, 99999999);
            $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
            $img = Image::make($images->getRealPath());
            $destinationPath = public_path('images/users');
            $images->move($destinationPath, $input['imagename']);
            $imagepath1 = "images/" . $input['imagename'];
            $request->merge(['image'=>$imagepath1]);
        }
      $production = expense::create($request->all());
      return redirect()->route('expense.index')
       ->with('success','Expense created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(expense $expense)
    {
        $vendor = vendor::all();
        $users = user::all();
        return view('expense.edit',compact('expense','vendor','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(expense $expense,request $request)
    {
        $newfile = '/assets/media/upload.png';
        if(isset($request->image) && $request->image) {
            $images = $request->file('image');
            $input['imagename'] ="";
            $randnum = rand(11111111, 99999999);
            $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
            $img = Image::make($images->getRealPath());
            $destinationPath = public_path('images/users');
            $images->move($destinationPath, $input['imagename']);
            $imagepath1 = "images/" . $input['imagename'];
            $request->merge(['image'=>$imagepath1]);
        }
        expense::where('id','=',$expense->id)->update($request->except(['_token','_method','image']));
        return redirect()->route('expense.index')
        ->with('success','Expense updated successfully');
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(expense $expense)
    {
        expense::find($expense->id)->delete();
        return redirect()->route('expense.index')
        ->with('success','Expense deleted successfully');
    }


}
