<?php

namespace App\Http\Controllers;

use App\Entities\animalstock;
use App\Entities\priceconfig;
use App\Entities\production;
use App\Entities\qrcodeconnection;
use App\Entities\sales;
use App\Entities\salesdetail;
use App\Entities\topcategory;
use App\Entities\category;
use App\Http\Requests\staffrequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\User;

class ResaleController extends Controller
{
    public function index()
    {

     $userid = Auth::user();
        $data = sales::leftJoin('top_categories','sales.topcat_id','=','top_categories.id')->leftJoin('users','sales.user_id','=','users.id')->where('sales.status','=',1)->where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->where('sales.resale','!=',NULL)->where('sales.resale','=',1)->select('top_categories.topcategory_name as itemtype','users.username as addbyname','sales.*')->get();


        $topcat = topcategory::all();
        return view('resale.index',compact('topcat'));
    }

    public function ResaleFilter(Request $request)
    {
    	$userid = Auth::user();
        $data  = sales::leftJoin('top_categories','sales.topcat_id','=','top_categories.id')->leftJoin('users','sales.user_id','=','users.id')->where('sales.resale','=',1)->where('sales.status','=',1)->where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->where(function ($production) use ($request) {
            if($request->filter_category != ""){
                $data->where('sales.topcat_id','=',$request->filter_category);
            }
            if($request->date_from != "" || $request->date_to != ""){
                $data->whereBetween('sales.created_at',[$request->date_from,$request->date_to]);
            }
        })->select('top_categories.topcategory_name as itemtype','users.username as addbyname','sales.*')->get();

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval(count($data)),
            "recordsFiltered" => intval(count($data)),
            "data"            => $data,
        );
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sales = sales::all();
        $qrcode = qrcodeconnection::leftJoin('qr_code','qr_connection.qr_id','=','qr_code.id')
            ->leftJoin('animalstock','qr_connection.animal_id','=','animalstock.id')->where('qr_connection.status','=',1)
           ->leftJoin('category','animalstock.category_id','=','category.id')->leftJoin('subcategory','animalstock.subcategory_id','=','subcategory.id')
            ->leftJoin('cell_box','animalstock.cell_number','=','cell_box.id')
            ->select('category.category_name','cell_box.cell_name','subcategory.subcategory_name','qr_connection.id as qr_con','qr_code.*','animalstock.*')->get();
if($request->type == "Milk"){
production::leftJoin('category','production.category_id','=','category.id')->leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=',$request->type)->select('production.*')->get();
}
         return view('resale.create',compact('qrcode','request','sales'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
        $request->merge(['user_id'=>$userid->id,'status'=>1,'resale'=>1]);
        $gettopcat = topcategory::where('topcategory_name','=',$request->top_id)->first();
        $request->merge(['topcat_id'=>$gettopcat->id,'branch_id'=>$userid->branch_id]);
        if($request->top_id == "Animal"){
            $request->merge(['total_weight'=>0]);
        }
        $sale = sales::create($request->all());
        if($request->top_id == "Animal") {
            $totalweight = array();
            $salesdetail = "";
            $salesdetail = $request->input('sales');
            foreach ($salesdetail as $sales) {
                salesdetail::create(['animal_id' => $sales['name'], 'price' => $sales['price'], 'weight' => $sales['weight'], 'sales_id' => $sale->id]);
                array_push($totalweight,$sales['weight']);
            }
            sales::where('id', '=', $sale->id)->update(['total_weight'=>array_sum($totalweight)]);
        }

        return redirect()->route('resale.index')
            ->with('success','Resale created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function GetPrice(Request $request)
    {
       $animal = animalstock::where('id','=',$request->animal_id)->first();
       return  priceconfig::where('category_id','=',$animal->category_id)->orderBy('id','DESC')->first();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(sales $sales)
    {
        $role = Role::all();
        $category=category::all();
        $topcategory=topcategory::all();
        return view('sales.edit',compact('topcategory','category','sales','role','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(user $user, request $request)
    {
        user::find($user->id)->update($request->all());
        return redirect()->route('resale.index')
            ->with('success','Resale updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(sales $resale,$id,Request $request)
    {
        sales::where('id', '=', $request->id)->update(['resale'=>0,'status'=>0]);
        return redirect()->route('resale.index')
            ->with('success','Resale deleted successfully');
    }
}
