<?php

namespace App\Http\Controllers;
use App\Entities\category;
use App\Entities\topcategory;
use Illuminate\Http\Request;
use App\Http\Requests\categoryrequest;
use Illuminate\Support\Facades\Auth;
use File;
use Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

         $data = category::leftJoin('top_categories','category.type','=','top_categories.id')
            ->select('top_categories.*','category.*')->get();
          return view('category.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $topcategory = topcategory::all();
        return view('category.create',compact('data','topcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(categoryrequest $request)
    {
      
       
       //    $vendor = new category();
       // $image = $request->file('image');
       //  $name= $image->getClientOriginalExtension();
       //  $destinationPath = public_path('/assets/media');
       //  $imagePath = $destinationPath. "/".  $name;
       //  $image->move($destinationPath, $name);
       //  $vendor->image = $name;
$userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
         $newfile2 ="";
            if(isset($request->images) && $request->images){
                $images = $request->file('images');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];
                $request->merge(['image'=>$imagepath3]);
       }
               $vendor = category::create($request->all());
       
     
       return redirect()->route('category.index')
       ->with('success','Category created successfully'); 
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(category $category)
    {
         $topcategory = topcategory::all();
        return view('category.edit',compact('category','topcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(category $category,request $request)
    {
         $this->validate($request, [
            'category_name' => 'required',
            'gender'=>'required',
            'status' => 'required'
            
        ]);

          


    $category->category_name = $request->input('category_name');
    $category->type = $request->input('type');
      $category->gender = $request->input('gender');

       $newfile2 =$category->image;
            if(isset($request->images) && $request->images){
                $images = $request->file('images');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];

            }

    category::where('id','=',$category->id)->update(['image'=>$imagepath3]);

    $category->status = $request->input('status');

        $category->save();
        return redirect()->route('category.index')
        ->with('success','Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(category $category)
    {
      category::find($category->id)->delete();
        return redirect()->route('category.index')
        ->with('success','Category deleted successfully');
    }
}
