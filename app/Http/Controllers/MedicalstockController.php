<?php

namespace App\Http\Controllers;

use App\Entities\medicalstock;
use App\Entities\vendor;
use App\Entities\subcategory;
use App\Entities\category;
use Illuminate\Http\Request;
use App\Entities\foodstock;
use App\Http\Requests\medicalstockrequest;



class MedicalstockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

          $data = foodstock::leftJoin('category','foodmedicalstock.id','=','category.id')
       ->leftjoin('subcategory','foodmedicalstock.id','=','subcategory.id')
           // ->where('item_name', 'medicine')
             ->select('category.*','subcategory.*','foodmedicalstock.*')->get();
        return view('medicalstock.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $vendor = vendor::all();
        $category = category::all();
        $subcategory = subcategory::all();
        return view('medicalstock.create',compact('data','vendor','category','subcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(medicalstockrequest $request)
    {

    $stocks = foodstock::create(['qrcode' => $request->input('qrcode'),'item_name'=>$request->item_name,'vendor_name'=>$request->vendor_name,'vendor_id'=>$request->vendor_id,'billing_amount'=>$request->billing_amount,'receipt_no'=>$request->receipt_no,'invoice_no'=>$request->invoice_no,'invoice_prefix'=>$request->invoice_prefix,'quality'=>$request->quality,'quantity'=>$request->quantity,'date'=>$request->date,'remarks'=>$request->remarks,'received_by_name'=>$request->received_by_name,'document_upload'=>$request->document_upload,'category_id'=>$request->category_id,'subcategory_id'=>$request->subcategory_id]);


       return redirect()->route('medicalstock.index')
       ->with('success','medicalstock created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(foodstock $foodstock)
    {
        $vendor = vendor::all();
        $subcategory = subcategory::all();
        $category = category::all();
        return view('medicalstock.edit',compact('foodstock','category','vendor','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(foodstock $foodstock,Request $request)
    {
        
        $this->validate($request, [
            'qrcode'=>'required',
            'item_name'=>'required',
            'vendor_name'=>'required',
         
            'billing_amount'=>'required',
            'receipt_no'=>'required',
            'date'=>'required',
            'invoice_no'=>'required',
            'invoice_prefix'=>'required',
            'quality'=>'required',
            'quantity'=>'required',
            'remarks'=>'required',
            'received_by_name'=>'required',
           'document_upload'=>'required'
          
 ]);

                $foodstock = foodstock::find($foodstock->id);
     $foodstock->qrcode = $request->input('qrcode');
     $foodstock->item_name = $request->input('item_name');
     $foodstock->vendor_name = $request->input('vendor_name');
     $foodstock->vendor_id = $request->input('vendor_id');
     $foodstock->billing_amount = $request->input('billing_amount');
     $foodstock->receipt_no = $request->input('receipt_no');
     $foodstock->date = $request->input('date');
     $foodstock->invoice_no = $request->input('invoice_no');
     $foodstock->invoice_prefix = $request->input('invoice_prefix');
     $foodstock->quality = $request->input('quality');
     $foodstock->quantity = $request->input('quantity');
     $foodstock->remarks = $request->input('remarks');
     $foodstock->received_by_name = $request->input('received_by_name');
     $foodstock->document_upload = $request->input('document_upload');
     $foodstock->category_id = $request->input('category_id');
   

        $foodstock->save();
        return redirect()->route('medicalstock.index')
        ->with('success','Medicalstock updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(foodstock $foodstock)
    {
        foodstock::find($foodstock->id)->delete();
        return redirect()->route('medicalstock.index')
        ->with('success','Medicalstock deleted successfully');
    }
}
