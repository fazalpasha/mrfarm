<?php

namespace App\Http\Controllers;

use App\Staffsalary;
use Illuminate\Http\Request;

class StaffsalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Staffsalary  $staffsalary
     * @return \Illuminate\Http\Response
     */
    public function show(Staffsalary $staffsalary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Staffsalary  $staffsalary
     * @return \Illuminate\Http\Response
     */
    public function edit(Staffsalary $staffsalary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Staffsalary  $staffsalary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staffsalary $staffsalary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Staffsalary  $staffsalary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staffsalary $staffsalary)
    {
        //
    }
}
