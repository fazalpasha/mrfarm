<?php

namespace App\Http\Controllers;
use App\Entities\topcategory;
use App\Entities\category;
use Illuminate\Http\Request;
use App\Http\Requests\topcategoryrequest;
use Illuminate\Support\Facades\Auth;


class TopcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = topcategory::orderBy('id','DESC')->get();
        return view('topcategory.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('topcategory.create',compact('data','topcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(topcategoryrequest $request)
    {
        $userid = Auth::user();
        $vendor = topcategory::create(['topcategory_name' => $request->input('topcategory_name'),'status'=> $request->status,'branch_id'=>$userid->branch_id]);
      return redirect()->route('topcategory.index')
       ->with('success','Topcategory created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(topcategory $topcategory)
    {
         
        return view('topcategory.edit',compact('topcategory','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(topcategory $topcategory,request $request)
    {
         $this->validate($request, [
            'topcategory_name' => 'required',
            'status'=>'required',
           
           
            
        ]);

     $topcategory->topcategory_name = $request->input('topcategory_name');
     $topcategory->status = $request->input('status');
    

        $topcategory->save();
        return redirect()->route('topcategory.index')
        ->with('success','Topcategory updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(topcategory $topcategory)
    {
        topcategory::find($topcategory->id)->delete();
        return redirect()->route('subcategory.index')
        ->with('success','Vendor deleted successfully');
    }
}
