<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\complaint;
use App\Entities\category;
use App\Entities\vendor;
use Illuminate\Support\Facades\Auth;
use Image;
use File;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
        $data = complaint::leftJoin('category','complaint.categoryname','=','category.id')->leftJoin('vendor','complaint.vendor_id','=','vendor.id')->where('complaint.branch_id','!=',NULL)->where('complaint.branch_id','=',$userid->branch_id)->select('category.category_name as cname','vendor.vendor_name as vname','complaint.*')->orderBy('id','DESC')->get();
        return view('complaint.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendor=vendor::all();
        $complaint=complaint::all();
        $category=category::all();
        return view('complaint.create',compact('complaint','category','vendor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
        $newfile2 ="";
            if(isset($request->images) && $request->images){
                $images = $request->file('images');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];
                $request->merge(['image'=>$imagepath3]);
       }
       $complaint =complaint::create($request->all());
        return redirect()->route('complaint.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(complaint $complaint)
    {
        $category=category::all();
        $vendor=vendor::all();
        return view('complaint.edit',compact('complaint','vendor','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(complaint $complaint,request $request)
    {
        
$newfile2 =$complaint->image;
            if(isset($request->image) && $request->image){
                $images = $request->file('image');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];

            }

    complaint::where('id','=',$complaint->id)->update(['image'=>$imagepath3]);
                $complaint->update($request->all());
                return redirect()->route('complaint.index')
        ->with('success','Topcategory updated successfully');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(complaint $complaint)
    {
        complaint::find($complaint->id)->delete();
        return redirect()->route('complaint.index')
        ->with('success','Complaint deleted successfully');
    }
}
