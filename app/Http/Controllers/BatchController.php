<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\batch;
use App\Entities\branch;
use App\Entities\category;
use App\Entities\subcategory;
use Illuminate\Support\Facades\Auth;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
        $data = batch::leftJoin('category','batch.category_id','=','category.id')->leftJoin('subcategory','batch.subcategory_id','=','subcategory.id')->where('batch.branch_id','!=',NULL)->where('batch.branch_id','=',$userid->branch_id)->select('category.category_name as cname','subcategory.subcategory_name as sname','batch.*')->orderBy('id','DESC')->get();
        return view('batch.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=category::all();
        $subcategory=subcategory::all();
        return view('batch.create',compact('category','subcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
      $request->merge(['branch_id'=>$userid->branch_id]);
        
      $batch = batch::create($request->all());
      return redirect()->route('batch.index')
       ->with('success','Batch created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(batch $batch)
    {
        $category=category::all();
        $subcategory=subcategory::all();
        return view('batch.edit',compact('batch','category','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(batch $batch,request $request)
    {
        batch::find($batch->id)->update($request->all());
        return redirect()->route('batch.index')
        ->with('success','Batch updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(batch $batch)
    {
        batch::find($batch->id)->delete();
        return redirect()->route('batch.index')
        ->with('success','Batch deleted successfully');
    }
}
