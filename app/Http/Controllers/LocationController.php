<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\location;
use App\Entities\place;
use Illuminate\Support\Facades\Auth;
use App\User;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $userid = Auth::user();
        $data=location::where('location.branch_id','!=',NULL)->where('location.branch_id','=',$userid->branch_id)->get();
        return view('location.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $place = place::all();
        return view('location.create',compact('place'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
        $location = location::create($request->all());
      return redirect()->route('location.index')
       ->with('success','Location created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(location $location)
    {
        $place=place::all();
        return view('location.edit',compact('location','place'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(location $location,Request $request)
    {
        $location->update($request->all());

                return redirect()->route('location.index')
        ->with('success','Location updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(location $location)
    {
        location::find($location->id)->delete();
        return redirect()->route('location.index')
        ->with('success','Location deleted successfully');
    }
}
