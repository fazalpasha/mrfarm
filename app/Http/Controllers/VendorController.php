<?php

namespace App\Http\Controllers;
use App\Entities\vendor;
use App\Entities\subcategory;
use Illuminate\Http\Request;
use App\Http\Requests\vendorrequest;
use App\Http\Requests\vendorupdaterequest;
use Illuminate\Support\Facades\Auth;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = vendor::leftJoin('subcategory','vendor.subcategory_id','=','subcategory.id')->where('vendor.branch_id','!=',NULL)->where('vendor.branch_id','=',$userid->branch_id)
            ->select('subcategory.*','vendor.*')->get();
        return view('vendor.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $subcategory = subcategory::all();
        return view('vendor.create',compact('data','subcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(vendorrequest $request)
    {
          // $request->merge(['request' => 'bengaluru']);
        $userid = Auth::user();
        $vendor = vendor::create(['vendor_name' => $request->input('vendor_name'),'email' => $request->email,'phone'=> $request->phone,'city'=> $request->city,'address'=>$request->address,'subcategory_id'=> $request->id,'branch_id'=>$userid->branch_id]);

     

       return redirect()->route('vendor.index')
       ->with('success','Vendor created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(vendor $vendor)
    {
        $subcategory = subcategory::all();
        return view('vendor.edit',compact('vendor','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(vendor $vendor,request $request)
    {

          $this->validate($request, [
            'vendor_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'address' => 'required',
            // 'subcategory_id'=> 'required'
            
        ]);

     $vendor->vendor_name = $request->input('vendor_name');
     $vendor->email = $request->input('email');
     $vendor->phone = $request->input('phone');
     $vendor->city = $request->input('city');
     $vendor->address = $request->input('address');
     $vendor->subcategory_id = $request->input('id');

        $vendor->save();
        return redirect()->route('vendor.index')
        ->with('success','Vendor updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(vendor $vendor)
    {
        vendor::find($vendor->id)->delete();
        return redirect()->route('vendor.index')
        ->with('success','Vendor deleted successfully');
    }
}
