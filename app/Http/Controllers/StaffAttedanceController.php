<?php

namespace App\Http\Controllers;

use App\Entities\attendence;
use App\Entities\leave;
use App\StaffAttedance;
use App\StaffLeaves;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;

class StaffAttedanceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        //$staffattedences = $this->staffattedence->all();

        $userdetails = Auth::user();
        // print_r($userdetails->branch_id);die();

        if(isset($userdetails->roles[0]->slug) && ($userdetails->roles[0]->slug == 'Manager' || $userdetails->roles[0]->slug == 'Admin')){

            $enableattedance =true;

        }else{

            $enableattedance =false;
        }

        $month = date('m');
        $year = date('Y');


        if (isset($request->month) && isset($request->year)) {
            $month = $request->month;
            $year = $request->year;
        }else{
            $request->merge(['month' => $month,'year' => $year]);
        }

        $dates =array();
        $authicated_user = Auth::user();


        $end = cal_days_in_month(CAL_GREGORIAN,$month,$year);
        $end=$end+1;
        // print_r($end);die();
        // if ($month == 2) {
        //    $end = 29;
        // }else if ($month % 2 == 0 && $month != 8) {
        //     $end = 31;
        // }else{
        //     $end = 32;
        // }

        for ($i=1 ; $i < $end ; $i++ ) {
            if($i == 1){
                array_push($dates, 'Staff_ID');
                array_push($dates, 'Name');
                array_push($dates, 'Nick');
            }
            if(strlen($i) < 2){
                $i = '0'.$i;
            }
            array_push($dates, $i);

            if($i == $end-1){
                array_push($dates, 'Days');
                array_push($dates, 'P');
                array_push($dates, 'H');
                array_push($dates, 'A');
                array_push($dates, 'L');
                array_push($dates, 'W');
                array_push($dates, 'TWD');
                //array_push($dates, 'payment');
            }

        }

        //$staffsheet = $staffapi->attedancelist($authicated_user->id,$month,$year);
        $header = $dates;

        $fullattedence =array();
        $fullattedence = ['heading' => $dates];

        if($month < 10 && strlen($month) < 2){
            $month = '0'.$month;
        }
        $productRecset =array();
        $index =0 ;
        //print_r($year.'-'.$month.'-00'. $year.'-'.$month.'-32');die;
        if($request->security_id == 'all'){
            $staffs = user::whereIn('roles',[2,3])->where('not_ofattedance','=',0)->get();
        }else{
            $staffs = user::whereIn('roles',[2,3])->where('not_ofattedance','=',0)->get();
        }



        foreach ($staffs as $value) {
            $productRec =array();
            $date= $year.'-'.$month.'-00';
           $lastdate = $year.'-'.$month.'-31';


            $totalattedence = attendence::select('in_date','punch_in','punch_out','is_absent')->where('is_absent', '!=', 1)
                ->where('user_id', '=', $value->id)
                ->where('in_date', '>=', $date)
                ->where('in_date', '<=', $lastdate)
                ->orderBy('in_date','asc')
                ->get();

            $totalleaves =leave::select('start_date','end_date')
                ->where('user_id', '=', $value->id)
                ->where('start_date', '>=', $date)
                ->where('start_date', '<=', $lastdate)
                ->where('status', '=', 2)
                ->orderBy('start_date','asc')
                ->get();
            $bonuscount = 0;
            //$bonusrepo->getlists($value->id,$year.'-'.$month.'-00',$year.'-'.$month.'-31');

            array_push($productRec,'MRFS'.$value->id);
            array_push($productRec,$value->first_name);
            array_push($productRec,$value->username);
            $variable =array();
            $timecheck = array();
            foreach ($totalattedence as $key) {
                array_push($variable, $key['in_date']);
                if(isset($timecheck[$key['in_date'].'-in']) && $timecheck[$key['in_date'].'-in']){
                    $timecheck[$key['in_date'].'-out'] = $key['punch_out'];
                }else{
                    $timecheck[$key['in_date'].'-in'] = $key['punch_in'];
                    $timecheck[$key['in_date'].'-out'] = $key['punch_out'];
                    $timecheck[$key['in_date'].'-weekoff'] = $key['is_absent'];
                    // if($key['is_absent'] == 2){
                    //   print_r( $timecheck[$key['in_date'].'-weekoff']);die();
                    // }

                }
            }
            $leavelist =array();
            $lcount = 0;
            $dot = 0;
            foreach ($totalleaves as $key) {
                array_push($leavelist, $key['start_date']);
                array_push($leavelist, $key['end_date']);
            }
            $pday = 0;
            $hday =0;
            $w =0;
            //print_r($leavelist);die;
            for ($i=1 ; $i < $end ; $i++ ) {

                if($i <= 9){
                    $i = '0'.$i;
                }
                $present = '';
                foreach ($totalleaves as $key) {
                    if($year.'-'.$month.'-'.$i >= $key['start_date'] && $year.'-'.$month.'-'.$i <= $key['end_date']){
                        $lcount = $lcount + 1;
                        $present = 'L';
                        break;
                        continue;
                    }

                }
                if($present != 'L'){
                    if(in_array($year.'-'.$month.'-'.$i, $variable)){
                        $to_time = strtotime($timecheck[$year.'-'.$month.'-'.$i.'-out']);
                        $from_time = strtotime($timecheck[$year.'-'.$month.'-'.$i.'-in']);

                        $weekoff = $timecheck[$year.'-'.$month.'-'.$i.'-weekoff'];
                        //$present = round(abs($to_time - $from_time) /3600);

                        //$present = round(abs($to_time - $from_time) /3600,2);

                        if($weekoff == 2){
                            $present = 'W';
                            $w++;
                        }else{


                            if($to_time){
                                $halfpresent = round(($to_time - $from_time)/3600, 1);

                                if($halfpresent > 4){

                                    if(isset($request->submit) && $request->submit == 'csvexport'){

                                        $present = 'P';
                                    }else{
                                        $present = date('H:i',$from_time).'<br> (P) <br>'.date('H:i',$to_time);
                                    }

                                    $pday++;
                                }else{

                                    if(isset($request->submit) && $request->submit == 'csvexport'){

                                        $present = 'H';
                                    }else{
                                        $present = date('H:i',$from_time).'<br> (H) <br>'.date('H:i',$to_time);
                                    }
                                    $hday++;
                                }


                            }else{

                                if(isset($request->submit) && $request->submit == 'csvexport'){
                                    $present = 'P';
                                }else{
                                    $present = date('H:i',$from_time).'<br> (P) <br> Missing';
                                }
                                $pday++;
                            }
                        }

                        //$present = floor($present / 60);
                    }elseif($year.'-'.$month.'-'.$i > date('Y-m-d')){
                        $present = '---';
                        $dot = $dot + 1;
                    }else{
                        $present = 'A';
                    }
                }

                array_push($productRec, $present);

            }
            $presentdays =  $pday;
            $productRec[] = $end-1;
            $productRec[] = $presentdays;
            $productRec[] = $hday;
            $productRec[] = $end-$dot-1-count($variable)-$lcount;
            $productRec[] = $lcount;
            $productRec[] = $w;
            $productRec[] = $w+$presentdays+($hday/2);
            //$totalsalary = '--';
            // if(isset($value->salary_id) && $value->salary_id != 0){

            //     if($presentdays > $value->salary['min_day_month']){
            //       $bounusdays = ($presentdays-$value->salary['min_day_month'])*$value->salary['bonusday_pay'];
            //       $addonbonus = $bonuscount*$value->salary['bonusday_pay'];
            //       $totalsalary = $value->salary['salary'] + $bounusdays + $addonbonus;
            //     }else{
            //       $perday = $value->salary/$value->salary['min_day_month']>0?$value->salary['min_day_month']:26;
            //       $addonbonus = $bonuscount * $value->salary['bonusday_pay'];
            //       $totalsalary = ($presentdays * $perday) + $addonbonus;
            //     }

            // }else{
            //   $totalsalary = '--';
            // }
            //$productRec[] = $totalsalary;
            //array_push($productRec, 'break');

            $productRecset[$index] = $productRec;
            $index++;
        }
        $dateline = $year.'-'.$month;
        $dateline = DateTime::createFromFormat('y-M-d', '2017-01-01');
        $str = $year.'-'.$month;
        $date = DateTime::createFromFormat('Y-m', $str);
        $dateline = $date->format('M-Y');


        if(isset($request->submit) && $request->submit == 'csvexport'){

            $response = new StreamedResponse(function() use($request,$productRecset,$header,$staffs){


                $handle = fopen('php://output', 'w');

                fputcsv($handle, $header);


                foreach ($productRecset as $key => $value) {


                    //foreach ($value as $k => $attendence){

                    fputcsv($handle, $value
                    );
                    // }

                }


                fclose($handle);
            }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="Security.csv"',
            ]);

            return $response;




        }


        return view('attendence.index', compact('header','productRecset','dateline','request','staffsheet','staffs','enableattedance'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StaffAttedance  $staffAttedance
     * @return \Illuminate\Http\Response
     */
    public function show(StaffAttedance $staffAttedance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StaffAttedance  $staffAttedance
     * @return \Illuminate\Http\Response
     */
    public function edit(StaffAttedance $staffAttedance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StaffAttedance  $staffAttedance
     * @return \Illuminate\Http\Response
     */
    public function attdupdate(Request $request){

        //print_r($request->all());die();

        if(isset($request->date) && isset($request->staffid)){
            $exists = attendence::where('in_date','=',$request->date)->where('user_id','=',$request->staffid)->get();
        }



        if(isset($request->attedance) && $request->attedance == 'P'){


            $this->leavetrace($request->date,$request->staffid);

            if(isset($exists->id)){

                $ads_punchins =array(

                    'user_id' => $request->staffid,
                    'in_date' => $request->date,
                    'punch_in_time' => $request->intime,
                    'punch_out_time' => $request->outtime,
                    'punch_in' => $request->date.' '.$request->intime,
                    'punch_out' => $request->date.' '.$request->outtime,
                );
                attendence::where('id','=',$exists->id)->update($ads_punchins);
            }else{


                $ads_punchin =array(

                    'user_id' => $request->staffid,
                    'in_date' => $request->date,
                    'punch_in_time' => $request->intime,
                    'punch_out_time' => $request->outtime,
                    'punch_in' => $request->date.' '.$request->intime,
                    'punch_out' => $request->date.' '.$request->outtime,
                );
                attendence::create($ads_punchin);

            }


        }elseif(isset($request->attedance) && $request->attedance == 'A'){


            $this->leavetrace($request->date,$request->staffid);

            if(isset($exists->id)){
                attendence::where('id','=',$exists->id)->update(['is_absent' => 1]);
            }else{


                $ads_punchin =array(
                    'user_id' => $request->staffid,
                    'in_date' => $request->date,
                    'is_absent' => 1);

                attendence::create($ads_punchin);
            }

        }elseif(isset($request->attedance) && $request->attedance == 'H'){


            $this->leavetrace($request->date,$request->staffid);

            if(isset($exists->id)){

                $ads_punchins =array(

                    'user_id' => $request->staffid,
                    'in_date' => $request->date,
                    'punch_in_time' => $request->intime,
                    'punch_out_time' => $request->outtime,
                    'punch_in' => $request->date.' '.$request->intime,
                    'punch_out' => $request->date.' '.$request->outtime,
                );
                attendence::where('id','=',$exists->id)->update($ads_punchins);
            }else{


                $ads_punchin =array(

                    'user_id' => $request->staffid,
                    'in_date' => $request->date,
                    'punch_in_time' => $request->intime,
                    'punch_out_time' => $request->outtime,
                    'punch_in' => $request->date.' '.$request->intime,
                    'punch_out' => $request->date.' '.$request->outtime,
                );

                attendence::create($ads_punchin);
            }


        }elseif(isset($request->attedance) && $request->attedance == 'L'){

            $this->leavetrace($request->date,$request->staffid);

            leave::create(['user_id' => $request->staffid,'start_date' =>$request->date,'end_date' => $request->date,'status' => 2]);

        }elseif(isset($request->attedance) && $request->attedance == 'W'){

            $this->leavetrace($request->date,$request->staffid);

            if(isset($exists->id)){
                attendence::where('id','=',$exists->id)->update(['is_absent' => 2]);
            }else{


                $ads_punchin =array(
                    'user_id' => $request->staffid,
                    'in_date' => $request->date,
                    'is_absent' => 2);
                attendence::create($ads_punchin);
            }

        }


        return redirect()->route('staffattendance.index')
            ->with('success','Staff Attendance created successfully');


    }

    public function leavetrace($date,$user_id){

        $leave = leave::wheredate('start_date','<=',$date)->wheredate('end_date','>=',$date)->where('user_id','=',$user_id)->first();

        if(!isset($leave->id)){
            return 'failed';
        }
        if($leave->start_date == $date && $leave->end_date == $date){

            return leave::where('id','=',$leave->id)->delete();

        }elseif($leave->start_date == $date){

            $strdate = strtotime ( '+1 day' , strtotime ($date));
            $updatedate = date ( 'Y-m-d' , $strdate );
            return leave::where('id','=',$leave->id)->update(['start_date' => $updatedate]);

        }elseif($leave->end_date >= $date && $leave->start_date < $date){

            $strdate = strtotime ( '-1 day' , strtotime ($date));
            $updatedate = date ( 'Y-m-d' , $strdate );

            return leave::where('id','=',$leave->id)->update(['end_date' => $updatedate]);

        }else{

            return leave::where('id','=',$leave->id)->delete();
        }

    }




    public function update(Request $request, StaffAttedance $staffAttedance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StaffAttedance  $staffAttedance
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaffAttedance $staffAttedance)
    {
        //
    }
}
