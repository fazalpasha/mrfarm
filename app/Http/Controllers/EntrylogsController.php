<?php

namespace App\Http\Controllers;
use App\Entities\entrylogs;
use App\Entities\subcategory;
use App\Entities\vehicalreports;
use Illuminate\Http\Request;
use App\Http\Requests\entrylogsrequest;
use App\Http\Requests\vendorupdaterequest;
use Illuminate\Support\Facades\Auth;

class EntrylogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = entrylogs::where('entrylogs.branch_id','!=',NULL)->where('entrylogs.branch_id','=',$userid->branch_id)->orderBy('id','DESC')->get();
        return view('entrylogs.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $vehical=vehicalreports::all();
        return view('entrylogs.create',compact('data','entrylogs','vehical'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(entrylogsrequest $request)
    { 
        $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
        $entrylogs = entrylogs::create($request->all());
       return redirect()->route('entrylogs.index')
       ->with('success','Entrylogs created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(entrylogs $entrylogs)
    {
        $vehical=vehicalreports::all();
        return view('entrylogs.edit',compact('entrylogs','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(entrylogs $entrylogs,request $request)
    {

          $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'reason' => 'required',
            
           
            
        ]);

     $entrylogs->first_name = $request->input('first_name');
     $entrylogs->last_name = $request->input('last_name');
     $entrylogs->phone = $request->input('phone');
     $entrylogs->reason = $request->input('reason');
    

        $entrylogs->save();
        return redirect()->route('entrylogs.index')
        ->with('success','Vendor updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(entrylogs $entrylogs)
    {
        entrylogs::find($entrylogs->id)->delete();
        return redirect()->route('entrylogs.index')
        ->with('success','Entrylogs deleted successfully');
    }
}
