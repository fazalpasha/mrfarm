<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entities\vendor;
use App\Entities\vehicalreports;
use App\Entities\place;
use App\Entities\transport;
use App\User;


class TransportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
        $data=transport::where('transport_vehicle.branch_id','!=',NULL)->where('transport_vehicle.branch_id','=',$userid->branch_id)->get();

        return view('transport.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehical=vehicalreports::all();
        $place=place::all();
        return view('transport.create',compact('vehical','place'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
        $trans = transport::create($request->all());
      return redirect()->route('transport.index');
          }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(transport $transport)
    {
        $place=place::all();
        $vehical=vehicalreports::all();
        return view('transport.edit',compact('transport','place','vehical'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(transport $transport,request $request)
    {
        $transport->update($request->all());

                return redirect()->route('transport.index')
        ->with('success','Topcategory updated successfully');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(transport $transport)
    {
        transport::find($transport->id)->delete();
        return redirect()->route('transport.index')
        ->with('success','Transport deleted successfully');
    }
}
