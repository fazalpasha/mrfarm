<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Entities\credit;

class CreditController extends Controller
{
    public function index()
    {
        $userid = Auth::user();
        // $data = credit::leftJoin('users','expense.added_by','=','users.id')->where('expense.branch_id','!=',NULL)->where('expense.branch_id','=',$userid->branch_id)->select('users.username','expense.*')->orderBy('id','DESC')->get();
        $data=credit::all();
        return view('credit.index',compact('data'));

    }
	public function create()
    {
        return view('credit.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $userid = Auth::user();
      $request->merge(['branch_id'=>$userid->branch_id]);
        
      $credit = credit::create($request->all());
      return redirect()->route('credit.index')
       ->with('success','Expense created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(credit $credit)
    {
    	return view('credit.edit',compact('credit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(credit $credit,request $request)
    {
        credit::find($credit->id)->update($request->all());
        return redirect()->route('credit.index')
        ->with('success','Credit updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(credit $credit)
    {
        credit::find($credit->id)->delete();
        return redirect()->route('credit.index')
        ->with('success','Credit deleted successfully');
    }


}