<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\agent;
use App\Entities\branch;
use App\Entities\category;
use App\Entities\subcategory;
use Illuminate\Support\Facades\Auth;
use File;
use Image;

class AgentController extends Controller
{
    public function index()
    {
        $userid = Auth::user();
        $data = agent::where('agent.branch_id','!=',NULL)->where('agent.branch_id','=',$userid->branch_id)->select('agent.*')->orderBy('id','DESC')->get();
        return view('agent.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$userid = Auth::user();
      $request->merge(['branch_id'=>$userid->branch_id]);
      $newfile2 ="";
            if(isset($request->photo) && $request->photo){
                $images = $request->file('photo');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];
                $request->merge(['image'=>$imagepath3]);
       }
      $agent = agent::create($request->all());
      return redirect()->route('agent.index')
       ->with('success','Agent created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(batch $batch)
    {
        $category=category::all();
        $subcategory=subcategory::all();
        return view('batch.edit',compact('batch','category','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(batch $batch,request $request)
    {
        batch::find($batch->id)->update($request->all());
        return redirect()->route('batch.index')
        ->with('success','Batch updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(batch $batch)
    {
        batch::find($batch->id)->delete();
        return redirect()->route('batch.index')
        ->with('success','Batch deleted successfully');
    }
}