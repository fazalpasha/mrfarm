<?php

namespace App\Http\Controllers;

use App\StaffLeaves;
use Illuminate\Http\Request;

class StaffLeavesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StaffLeaves  $staffLeaves
     * @return \Illuminate\Http\Response
     */
    public function show(StaffLeaves $staffLeaves)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StaffLeaves  $staffLeaves
     * @return \Illuminate\Http\Response
     */
    public function edit(StaffLeaves $staffLeaves)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StaffLeaves  $staffLeaves
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaffLeaves $staffLeaves)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StaffLeaves  $staffLeaves
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaffLeaves $staffLeaves)
    {
        //
    }
}
