<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Entities\payslip;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\User;
use PDF;

class PayslipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = payslip::leftJoin('users','staffsalaries.user_id','=','users.id')->where('staffsalaries.branch_id','!=',NULL)->where('staffsalaries.branch_id','=',$userid->branch_id)
            ->select('users.*','staffsalaries.*')->get();
            // print_r($data);die();
        return view('payslip.index',compact('data'));
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::all();
        $staff = user::leftJoin('roles','users.roles','=','roles.id')->where('roles.slug_name','=','Staff')->select('roles.*','users.*')->get();
        return view('payslip.create',compact('role','staff'));
    }

  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // print_r($request->all());die();
       $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
        $bandgroup = $request->input('group');
        if(isset($bandgroup)) {
            foreach ($bandgroup as $bands) {
               payslip::create(['user_id' => $bands['userid'],'ref_date'=>$request->ref_date,'slip_month'=>$request->slip_month,'basic'=>$request->basic,'HRA'=>$request->HRA,'TA'=>$request->TA,'DA'=>$request->DA,'LTC'=>$request->LTC,'special_allowance'=>$request->special_allowance,'medical_allowance'=>$request->medical_allowance,'proffesional_development'=>$request->proffesional_development,'D_PF'=>$request->D_PF,'D_Proff_tax'=>$request->D_Proff_tax,'status'=>$request->status,'year'=>$request->year,'branch_id'=>$request->branch_id]);

            }
        }
         // $newfile1 ='/assets/media/upload.png';
         //    if(isset($request->image) && $request->image){
         //        $newfile1 = $this->fileupload->store($request->file('image'));
         //        $newfile1 = $newfile1->toArray();
         //        $newfile1 = $newfile1['pathfile'];
         //    $request->merge(['document_upload' => $newfile1]);
         //    }

        // $vendorname = vendor::where('id','=',$request->vendor_id)->first();
        // $request->merge(['vendor_name'=>$vendorname->vendor_name]);
        // $foodstock = foodstock::create($request->all());
       return redirect()->route('payslip.index')
       ->with('success','Payslip created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(foodstock $foodstock)
    {
        $vendor = vendor::all();
        $category = category::leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Food')->orWhere('top_categories.topcategory_name','=','Medical')->select('category.*')->get();
        return view('foodstock.edit',compact('foodstock','vendor','category','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(payslip $payslip)
    {
        payslip::find($payslip->id)->delete();
        return redirect()->route('payslip.index')
        ->with('success','payslip deleted successfully');
    }
    public function download(payslip $payslip,$id){
        $data = payslip::findOrFail($id);
        $pdf = PDF::loadView('payslip.download', compact('data','payslip'));
        return $pdf->download('result.pdf');
    }
    public function regeneratestore(Request $request)
    {

        // print_r($request->all());die();
         $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
       payslip::create($request->all());
       return redirect()->route('payslip.index')
       ->with('success','Payslip created successfully'); 
        }
}
