<?php

namespace App\Http\Controllers;
use App\Entities\branch;
use Illuminate\Http\Request;
use App\Http\Requests\topcategoryrequest;
use App\Http\Requests\branchrequest;
use Illuminate\Support\Facades\Auth;
use File;
use Image;


class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = branch::orderBy('id','DESC')->get();
        return view('branch.index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('branch.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(branchrequest $request)
    {
        $newfile2 ="";
            if(isset($request->images) && $request->images){
                $images = $request->file('images');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];
                $request->merge(['image'=>$imagepath3]);
       }
      $vendor = branch::create($request->all());
      return redirect()->route('branch.index')
       ->with('success','Branch created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(branch $branch)
    {

        return view('branch.edit',compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(branch $branch,request $request)
    {
branch::find($branch->id)->update($request->all());
    //     $branch->name = $request->input('name');
    // $branch->email = $request->input('email');
    //   $branch->mobilenumber = $request->input('mobilenumber');

       $newfile2 =$branch->image;
            if(isset($request->images) && $request->images){
                $images = $request->file('images');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];

            }
    branch::where('id','=',$branch->id)->update(['image'=>$imagepath3]);
        return redirect()->route('branch.index')
        ->with('success','Vranch updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(branch $branch)
    {
        branch::find($branch->id)->delete();
        return redirect()->route('branch.index')
        ->with('success','Vendor deleted successfully');
    }
}
