<?php

namespace App\Http\Controllers;
use App\Entities\animalstock;
use App\Entities\qrcode;
use App\Entities\qrcodeconnection;
use Illuminate\Http\Request;
use App\Http\Requests\animalstockrequest;
use App\Entities\vendor;
use App\Entities\branch;
use App\Entities\category;
use App\Entities\subcategory;
use App\Entities\cell;
use Illuminate\Support\Facades\Auth;


class AnimalstockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
         // print_r($userid->branch_id);die();
        $data = animalstock::leftJoin('category','animalstock.category_id','=','category.id')->leftJoin('subcategory','animalstock.subcategory_id','=','subcategory.id')->leftJoin('cell_box','animalstock.cell_number','=','cell_box.id')->where('animalstock.branch_id','!=',NULL)->where('animalstock.branch_id','=',$userid->branch_id)->select('category.category_name','cell_box.cell_name','subcategory.subcategory_name','animalstock.*')->orderBy('id','DESC')->get();
        //   print_r($data);die();
        $category = category::all();
        $cell=cell::all();
        return view('animalstock.index',compact('data','category','cell'));

    }

    public function Fliter(Request $request)
    {
      $userid = Auth::user();
        $data = animalstock::leftJoin('category','animalstock.category_id','=','category.id')->leftJoin('subcategory','animalstock.subcategory_id','=','subcategory.id')->leftJoin('cell_box','animalstock.cell_number','=','cell_box.id')->where('animalstock.branch_id','!=',NULL)->where('animalstock.branch_id','=',$userid->branch_id)->select('category.category_name','cell_box.cell_name','subcategory.subcategory_name','animalstock.*')->where(function ($data) use ($request) {
            if($request->date_from != "" || $request->date_to != ""){
                $data->whereBetween('animalstock.date',[$request->date_from,$request->date_to]);
            }
        })->orderBy('id','DESC')->get();

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval(count($data)),
            "recordsFiltered" => intval(count($data)),
            "data"            => $data
        );
        return json_encode($json_data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $userid = Auth::user();
      $vendor = vendor::all();
      $category = category::all();//leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','animalstock')->select('category.*')->get();
      $cell = cell::all();
      $qrcode = qrcode::select('qr_code.*')->where('qr_code.branch_id','!=',NULL)->where('qr_code.branch_id','=',$userid->branch_id)->get();
      return view('animalstock.create',compact('vendor','data','category','subcategory','cell','qrcode'));
  }

  public function  SubCategory(Request $request){
    return subcategory::where('category_id','=',$request->top_cat)->get();
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(animalstockrequest $request)
    {
      
      $userid = Auth::user();
        $vendorname = vendor::where('id','=',$request->vendor_id)->first();
        $qrid = $request->qrcode;
        $qr = qrcode::where('id','=',$qrid)->first();
        $request->merge(['vendor_name'=>$vendorname->vendor_name,'qrcode'=>$qr->prefix.$qr->qr_code,'branch_id'=>$userid->branch_id]);
        $animalstock = animalstock::create($request->all());
        qrcodeconnection::create(['qr_id'=>$qrid, 'animal_id'=>$animalstock->id, 'status'=>1]);
        qrcode::find($qr->id)->update(['status'=>1]);
        if($request->repeat == 1){
            $vendor = vendor::all();
            $category = category::leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Animal')->select('category.*')->get();
            $cell = cell::all();
            $qrcode = qrcode::where('status','=',0)->select('qr_code.*')->get();
            return view('animalstock.create',compact('vendor','data','category','subcategory','cell','qrcode','request'));
        }else{
            return redirect()->route('animalstock.index')
            ->with('success','animalstock created successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(animalstock $animalstock)
    {
       $vendor = vendor::all();
       $category = category::all();
       $subcategory = subcategory::all();
       $cell = cell::all();
       $qrcode=qrcode::all();
       return view('animalstock.edit',compact('animalstock','vendor','category','subcategory','cell','qrcode'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(animalstock $animalstock,Request $request)
    {
       
       animalstock::find($animalstock->id)->update($request->all());
       return redirect()->route('animalstock.index')
       ->with('success','animalstock updated successfully');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(animalstock $animalstock)
    {
        animalstock::find($animalstock->id)->delete();
        return redirect()->route('animalstock.index')
        ->with('success','animalstock deleted successfully');
    }
}
