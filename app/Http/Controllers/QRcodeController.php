<?php

namespace App\Http\Controllers;
use App\Entities\foodstock;
use App\Entities\qrcode;
use App\Entities\topcategory;
use Illuminate\Http\Request;
use App\Entities\subcategory;
use App\Entities\vendor;
use App\Entities\category;
use Illuminate\Support\Facades\Auth;

class QRcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = qrcode::leftJoin('top_categories','qr_code.type','=','top_categories.id')->where('qr_code.branch_id','!=',NULL)->where('qr_code.branch_id','=',$userid->branch_id)->orderBy('qr_code.id','DESC')->select('top_categories.*','qr_code.*')->get();
        $topcat= topcategory::where('status','=',1)->get();
        return view('qrcode.index',compact('data','topcat'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('qrcode.create',compact('vendor','data','category','subcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
        for($i=$request->range_from;$i<=$request->range_to;$i++) {
            qrcode::create(['qr_code'=>$i, 'prefix'=>$request->prefix, 'type'=>$request->type, 'status'=>0,'branch_id'=>$userid->branch_id]);
        }
       return redirect()->route('qrcode.index')
       ->with('success','qrcode created successfully');

    }

    public function GetCount(Request $request){
        return qrcode::where('type','=',$request->type)->count();
    }



    public function edit(qrcode $qrcode)
    {
        $userid = Auth::user();
        $data = qrcode::leftJoin('top_categories','qr_code.type','=','top_categories.id')->where('qr_code.branch_id','!=',NULL)->where('qr_code.branch_id','=',$userid->branch_id)->orderBy('qr_code.id','DESC')->select('top_categories.*','qr_code.*')->get();
        $topcat= topcategory::where('status','=',1)->get();
          return view('qrcode.edit',compact('qrcode','data','topcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(qrcode $qrcode,Request $request)
    {
        qrcode::where('id','=',$qrcode->id)->update($request->all());
        return redirect()->route('qrcode.index')
        ->with('success','qrcode updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $post = qrcode::findOrFail($id);
$post->delete();
        return redirect()->route('qrcode.index')
        ->with('success','Category deleted successfully');
    }
}
