<?php

namespace App\Http\Controllers;
use App\Entities\production;
use App\Entities\category;
use App\Entities\subcategory;
use App\Entities\vendor;
use App\Entities\cell;
use App\Entities\branch;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\productionrequest;
use Illuminate\Support\Facades\Auth;


class ProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = production::leftJoin('category','production.category_id','=','category.id')->leftJoin('subcategory','production.subcategory_id','=','subcategory.id')->leftJoin('cell_box','production.cell_id','=','cell_box.id')->leftJoin('users','production.added_by','=','users.id')->where('production.branch_id','!=',NULL)->where('production.branch_id','=',$userid->branch_id)->select('category.category_name','cell_box.cell_name','subcategory.subcategory_name','users.*','production.*')->paginate();
        $category = category::all();
        return view('production.index',compact('data','category'));
    }

    public function Filter(Request $request)
    {
        $userid = Auth::user();
        $production  = production::leftJoin('category','production.category_id','=','category.id')->leftJoin('subcategory','production.subcategory_id','=','subcategory.id')->leftJoin('cell_box','production.cell_id','=','cell_box.id')->leftJoin('users','production.added_by','=','users.id')->where('production.branch_id','!=',NULL)->where('production.branch_id','=',$userid->branch_id)->where(function ($production) use ($request) {
            if($request->date_from != "" || $request->date_to != ""){
                $production->whereBetween('production.created_at',[$request->date_from,$request->date_to]);
            }
        })->select('category.category_name','cell_box.cell_name','subcategory.subcategory_name','users.*','production.*')->get();

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval(count($production)),
            "recordsFiltered" => intval(count($production)),
            "data"            => $production
        );
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = category::all();//leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Production')->select('category.*')->get();
          $cell = cell::all();
          $add = user::all();
          return view('production.create',compact('category','cell','add','subcategory'));
    }

     public function  SubCategory(Request $request){
        return subcategory::where('category_id','=',$request->top_cat)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(productionrequest $request)
    {
      $userid = Auth::user();
      $request->merge(['branch_id'=>$userid->branch_id]);
      $production = production::create($request->all());
      return redirect()->route('production.index')
       ->with('success','Production created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(production $production)
    {
        $category = category::all();//leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Production')->select('category.*')->get();
          $cell = cell::all();
          $add = user::all();
          // print_r($production->id);die();
        return view('production.edit',compact('production','category','cell','add','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(production $production,request $request)
    {

         $this->validate($request, [
            'quantity'=>'required',
            'unit'=>'required',
        ]);

        $production = production::find($production->id);
        // $production->quality = $request->input('quality');
        $production->quantity = $request->input('quantity');
        $production->unit = $request->input('unit');
        $production->added_by = $request->input('added_by');
        $production->cell_id = $request->input('cell_id');
        $production->category_id = $request->input('category_id');
        $production->subcategory_id = $request->input('subcategory_id');


        $production->save();
         // print_r($production->added_by);die();
        return redirect()->route('production.index')
        ->with('success','Production updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(production $production)
    {
        production::find($production->id)->delete();
        return redirect()->route('production.index')
        ->with('success','Production deleted successfully');
    }

}
