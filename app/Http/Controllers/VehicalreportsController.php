<?php

namespace App\Http\Controllers;
use App\Entities\vehicalreports;
use App\Entities\subcategory;
use Illuminate\Http\Request;
use App\Http\Requests\vehicalreportsrequest;
use App\Http\Requests\vendorupdaterequest;
use Illuminate\Support\Facades\Auth;

class VehicalreportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = vehicalreports::where('vehicalreports.branch_id','!=',NULL)->where('vehicalreports.branch_id','=',$userid->branch_id)->orderBy('id','DESC')->get();
        return view('vehicalreports.index',compact('data'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('vehicalreports.create',compact('data','vehicalreports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(vehicalreportsrequest $request)
    {
       // print_r($request->all());die();
        $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
         $vehicalreports = vehicalreports::create($request->all());
     
     

       return redirect()->route('vehicalreports.index')
       ->with('success','Vendor created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(vehicalreports $vehicalreports)
    {
      
        return view('vehicalreports.edit',compact('vehicalreports','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(vehicalreports $vehicalreports,request $request)
    {

        //   $this->validate($request, [
        //     'vehical_no' => 'required',
        //     'vehical_type' => 'required',
        //     'wheels' => 'required',
        //     'driver_name' => 'required',
        //     'weight'=>'required',
        //     // 'entry_time'=>'required',
        //     // 'exit_time'=>'required',
        //     'load_type'=>'required',
        //     'status'=>'required',
        //     'type'=>'required',
        //      'remarks'=>'required',
           
            
        // ]);

     $vehicalreports->vehical_no = $request->input('vehical_no');
     $vehicalreports->vehical_type = $request->input('vehical_type');
     $vehicalreports->wheels = $request->input('wheels');
     $vehicalreports->driver_name = $request->input('driver_name');
     $vehicalreports->weight = $request->input('weight');
     $vehicalreports->entry_time = $request->input('entry_time');
     $vehicalreports->exit_time = $request->input('exit_time');
     $vehicalreports->load_type = $request->input('load_type');
      $vehicalreports->type = $request->input('type');
     $vehicalreports->status = $request->input('status');
     $vehicalreports->remarks = $request->input('remarks');

        $vehicalreports->save();
        return redirect()->route('vehicalreports.index')
        ->with('success','Vendor updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(vehicalreports $vehicalreports)
    {
        vehicalreports::find($vehicalreports->id)->delete();
        return redirect()->route('vehicalreports.index')
        ->with('success','Vehicalreports deleted successfully');
    }
}
