<?php

namespace App\Http\Controllers;
use App\Entities\foodstock;
use Illuminate\Http\Request;
use App\Http\Requests\foodstockrequest;
use App\Entities\vendor;
use App\Entities\branch;
use App\Entities\category;
use App\Entities\subcategory;
use App\Entities\cell;
use File;
use Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class FoodstockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = Auth::user();
        $data = foodstock::leftJoin('category','foodmedicalstock.category_id','=','category.id')->leftJoin('subcategory','foodmedicalstock.subcategory_id','=','subcategory.id')->where('foodmedicalstock.branch_id','!=',NULL)->where('foodmedicalstock.branch_id','=',$userid->branch_id)->select('category.category_name','subcategory.subcategory_name','foodmedicalstock.*')->get();
         return view('foodstock.index',compact('data','role'));
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendor = vendor::all();
        $category = category::all();//leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Food')->orWhere('top_categories.topcategory_name','=','Medical')->select('category.*')->get();

        return view('foodstock.create',compact('vendor','category','subcategory'));
    }

    public function  SubCategory(Request $request){
        return subcategory::where('category_id','=',$request->top_cat)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(foodstockrequest $request)
    {
        $userid = Auth::user();
        $vendorname = vendor::where('id','=',$request->vendor_id)->first();
        $request->merge(['vendor_name'=>$vendorname->vendor_name,'branch_id'=>$userid->branch_id]);

       // $foodstock = new foodstock();
       // $document_upload = $request->file('document_upload');
       //  $name = str_slug($request->foodstock).'.'.$document_upload->getClientOriginalExtension();
       //  $destinationPath = public_path('/assets/media');
       //  $imagePath = $destinationPath. "/".  $name;
       //  $document_upload->move($destinationPath, $name);
       //  $foodstock->document_upload = $name;
      
            $newfile1 ="";
            if(isset($request->images) && $request->images){
                $images = $request->file('images');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath2 = "images/users/" . $input['imagename'];
                $request->merge(['document_upload'=>$imagepath2]);

            }

                

       

        $foodstock = foodstock::create($request->all());
        return redirect()->route('foodstock.index')
        ->with('success','Foodstock created successfully'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(foodstock $foodstock)
    {
        $vendor = vendor::all();
        $category = category::all();//leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Food')->orWhere('top_categories.topcategory_name','=','Medical')->select('category.*')->get();
        return view('foodstock.edit',compact('foodstock','vendor','category','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(foodstock $foodstock,Request $request)
    {
        $this->validate($request, [
           
            'item_name'=>'required',
            
            'billing_amount'=>'required',
            'receipt_no'=>'required',
            'date'=>'required',
            'invoice_no'=>'required',
            'quality'=>'required',
            'quantity'=>'required',
            'remarks'=>'required',
            'received_by_name'=>'required',
            
          
 ]);



     $foodstock->qrcode = $request->input('qrcode');
     $foodstock->item_name = $request->input('item_name');
     $foodstock->vendor_name = $request->input('vendor_name');
    
     $foodstock->billing_amount = $request->input('billing_amount');
     $foodstock->receipt_no = $request->input('receipt_no');
     $foodstock->date = $request->input('date');
     $foodstock->invoice_no = $request->input('invoice_no');
     $foodstock->quality = $request->input('quality');
     $foodstock->quantity = $request->input('quantity');
     $foodstock->remarks = $request->input('remarks');
     $foodstock->received_by_name = $request->input('received_by_name');

        
        $newfile1 =$foodstock->document_upload;
            if(isset($request->images) && $request->images){
                $images = $request->file('images');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                 

                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath2 = "images/users/" . $input['imagename'];
              

            }


          foodstock::where('id','=',$foodstock->id)->update(['document_upload'=>$imagepath2]);

     $foodstock->category_id = $request->input('category_id');
     $foodstock->subcategory_id = $request->input('subcategory_id');

        $foodstock->save();
        return redirect()->route('foodstock.index')
        ->with('success','Foodstock updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(foodstock $foodstock)
    {
        foodstock::find($foodstock->id)->delete();
        return redirect()->route('foodstock.index')
        ->with('success','Foodstock deleted successfully');
    }
}
