<?php

namespace App\Http\Controllers;

use App\Entities\animalstock;
use App\Entities\priceconfig;
use App\Entities\production;
use App\Entities\qrcodeconnection;
use App\Entities\sales;
use App\Entities\category;
use App\Entities\salesdetail;
use App\Entities\topcategory;
use App\Entities\agent;
use App\Entities\payment;
use App\Entities\subcategory;
use App\Http\Requests\staffrequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\User;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

     $userid = Auth::user();
        $data = sales::leftJoin('top_categories','sales.topcat_id','=','top_categories.id')->leftJoin('branch','sales.branch_id','=','branch.id')->leftJoin('users','sales.user_id','=','users.id')->where('sales.status','=',1)->where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->select('top_categories.topcategory_name as itemtype','users.username as addbyname','branch.name as itemname','sales.*')->get();


        $topcat = topcategory::all();
        return view('sales.index',compact('topcat','data'));
    }

    public function Filter(Request $request)
    {
        $userid = Auth::user();
        $production  = sales::leftJoin('top_categories','sales.topcat_id','=','top_categories.id')->leftJoin('users','sales.user_id','=','users.id')->leftJoin('branch','sales.branch_id','=','branch.id')->where('sales.status','=',1)->where('sales.branch_id','!=',NULL)->where('sales.branch_id','=',$userid->branch_id)->where(function ($production) use ($request) {
            if($request->filter_category != ""){
                $production->where('sales.topcat_id','=',$request->filter_category);
            }
            if($request->date_from != "" || $request->date_to != ""){
                $production->whereBetween('sales.created_at',[$request->date_from,$request->date_to]);
            }
        })->select('top_categories.topcategory_name as itemtype','users.username as addbyname','branch.name as itemname','sales.*')->get();

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval(count($production)),
            "recordsFiltered" => intval(count($production)),
            "data"            => $production,
        );
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sales = sales::all();
        $agent=agent::all();
        $qrcode = qrcodeconnection::leftJoin('qr_code','qr_connection.qr_id','=','qr_code.id')
            ->leftJoin('animalstock','qr_connection.animal_id','=','animalstock.id')->where('qr_connection.status','=',1)
           ->leftJoin('category','animalstock.category_id','=','category.id')->leftJoin('subcategory','animalstock.subcategory_id','=','subcategory.id')
            ->leftJoin('cell_box','animalstock.cell_number','=','cell_box.id')
            ->select('category.category_name','cell_box.cell_name','subcategory.subcategory_name','qr_connection.id as qr_con','qr_code.*','animalstock.*')->get();
if($request->type == "Milk"){
production::leftJoin('category','production.category_id','=','category.id')->leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=',$request->type)->select('production.*')->get();
}
         return view('sales.create',compact('qrcode','request','sales','agent'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
        $request->merge(['user_id'=>$userid->id,'status'=>1]);
        $gettopcat = topcategory::where('topcategory_name','=',$request->top_id)->first();

        $request->merge(['topcat_id'=>$gettopcat->id,'branch_id'=>$userid->branch_id]);

        if(ucfirst($request->top_id) == "Animal"){
            $request->merge(['total_weight'=>0]);
        }
        $sale = sales::create($request->all());
        if($request->top_id == "Animal") {
            $totalweight = array();
            $salesdetail = "";
            $salesdetail = $request->input('sales');
            foreach ($salesdetail as $sales) {
                salesdetail::create(['animal_id' => $sales['acode'], 'price' => $sales['price'], 'weight' => $sales['weight'], 'sales_id' => $sale->id]);
                array_push($totalweight,$sales['weight']);
            }
            sales::where('id', '=', $sale->id)->update(['total_weight'=>array_sum($totalweight)]);
        }

        return redirect()->route('sales.index')
            ->with('success','Sale created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function GetPrice(Request $request)
    {
       $animal = animalstock::where('id','=',$request->animal_id)->first();
       return  priceconfig::where('category_id','=',$animal->category_id)->orderBy('id','DESC')->first();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(sales $sales,Request $request)
    {
    sales::find($sales->id)->leftJoin('branch','sales.branch_id','=','branch.id')->select('branch.name as bname','sales.*');
        return view('sales.edit',compact('topcategory','category','sales','role','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(sales $sales, Request $request)
    {
        sales::find($sales->id)->update($request->all());
        return redirect()->route('sales.index')
            ->with('success','Sale updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(sales $sales)
    {
        sales::where('id', '=', $sales->id)->update(['status'=>0]);
        return redirect()->route('sales.index')
            ->with('success','Sale deleted successfully');
    }
    public function addpayment(Request $request)
    {
        return view('sales.addpayment');
    }
    public function paymentstore(Request $request)
    {    
        $payment=payment::create($request->all());
        return redirect()->route('sales.index')
        ->with('success','Sale created successfully');
    }
    public function viewpayment(Request $request)
    {    
        $payment=payment::all();
        return view('sales.viewpayment',compact('payment'));
    }
}
