<?php

namespace App\Http\Controllers;

use App\Http\Requests\staffrequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\Entities\branch;
use App\User;
use Image;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
        $data = user::leftJoin('roles','users.roles','=','roles.id')->leftJoin('branch','users.branch_id','=','branch.id')->where('users.assigned_branch','LIKE','%'.$userid->branch_id.'%')->select('branch.name as branchname','roles.name as rolename','users.*')->get();
        return view('staff.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::all();
        $branch=branch::all();
         return view('staff.create',compact('role','branch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(staffrequest $request)
    {
         //print_r($request->all());die();
        $userid = Auth::user();
        $password = Hash::make('1234');
        $usercount = user::all();
        $branches = $request->assigned_branch;
        if(isset($branches[0])){
           $branch_id = $branches[0]; 
       }else{
           $branch_id = '';
       }
        
        $request->merge(['status' => 1,'username'=>$request->nick,'password'=>$password,'access_key'=>2000+count($usercount),'branch_id'=>$branch_id,'assigned_branch' => json_encode($branches)]);
        $user = user::create($request->all());
        //$user->assignRole($request->input('roles'));

            $schedulestaffs =user::find($user->id);
            $newfile = '/assets/media/upload.png';
            $imagepath1 ='';
            if(isset($request->prophoto) && $request->prophoto) {
                $images = $request->file('prophoto');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = Image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath1 = "images/users/" . $input['imagename'];
            }

            $imagepath2='';
            $newfile1 ='/assets/media/upload.png';
            if(isset($request->referencedoc1_copy) && $request->referencedoc1_copy){
                $images = $request->file('referencedoc1_copy');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = Image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath2 = "images/users/" . $input['imagename'];

            }


            $newfile2  ='/assets/media/upload.png';
            $imagepath3='';
            if(isset($request->idproofdoc_copy) && $request->idproofdoc_copy){
                $images = $request->file('idproofdoc_copy');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = Image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath3 = "images/users/" . $input['imagename'];

            }



            $newfile3 ='/assets/media/upload.png';
            $imagepath4='';
            if(isset($request->referencedoc2_copy) && $request->referencedoc2_copy){
                $images = $request->file('referencedoc2_copy');
                $input['imagename'] ="";
                $randnum = rand(11111111, 99999999);
                $input['imagename'] = $randnum.'.'.$images->getClientOriginalExtension();
                $img = Image::make($images->getRealPath());
                $destinationPath = public_path('images/users');
                $images->move($destinationPath, $input['imagename']);
                $imagepath4 = "images/users/" . $input['imagename'];

            }


            $schedulestaffs->update([
                'ref1doc_copy' => $imagepath1,
                'profileImg' => $imagepath2,
                'ref2doc_copy' =>$imagepath3,
                'idproof_copy' => $imagepath4,
            ]);

        return redirect()->route('staff.index')
            ->with('success','Staff created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        $role = Role::all();
        $branch=branch::all();
        return view('staff.edit',compact('role','user','branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(user $user, request $request)
    {
        $userid=Auth::user();
        $request->merge(['status' => 1,'branch_id'=>$userid->branch_id]);
        user::find($user->id)->update($request->all());
        return redirect()->route('staff.index')
            ->with('success','Topcategory updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        user::destroy($user->id);
        return redirect()->route('staff.index')
            ->with('success','Vendor deleted successfully');
    }
}
