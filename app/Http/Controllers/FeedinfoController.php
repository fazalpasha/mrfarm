<?php

namespace App\Http\Controllers;

use App\Feedchart;
use App\User;
use Illuminate\Http\Request;
use App\Entities\category;
use App\Entities\subcategory;
use App\Entities\cell;
use App\Entities\Feedinfo;
use Illuminate\Support\Facades\Auth;

class FeedinfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
        //$data=Feedinfo::all();
        $data=Feedinfo::leftjoin('category','feedinfo.category_id','=','category.id')->leftjoin('subcategory','feedinfo.subcategory_id','=','subcategory.id')->where('feedinfo.branch_id','!=',NULL)->where('feedinfo.branch_id','=',$userid->branch_id)->select('category.category_name','subcategory.subcategory_name','feedinfo.*')->paginate();
        //echo '<pre>';print_r($data);die();
        // $data = Feedchart::leftJoin('subcategory','feedcharts.subcategory_id','=','subcategory.id')->leftJoin('category','subcategory.category_id','=','feedcharts.cell_number')->leftJoin('users','users.id','=','feedcharts.user_id')->leftJoin('cell_box','cell_box.id','=','category.id')->select('category.category_name','subcategory.subcategory_name','users.first_name','feedcharts.*')->paginate();
        //$data='';
        //echo $data->$category_name;die();
        return view('feedinfo.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = category::get();
        //echo '<pre>';print_r($category);die();
        $cell = cell::all();
        $add = User::get();
        return view('feedinfo.create',compact('category','cell','add'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id]);
        $Feedchart = Feedinfo::create($request->all());
        return redirect()->route('feedinfo.index')
        ->with('success','Feed infocreated successfully'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function show(Feedchart $feedchart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Feedinfo $feedinfo)
    {
        \Log::info($feedinfo->category);
        $category=category::all();
        return view('feedinfo.edit', compact('category','feedinfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedinfo $feedinfo)
    {
        //echo '<pre>';print_r($request->all());die();
        //dd($feedinfo);
        //$feedinfo['title']
        $feedinfo->update($request->all());
        return redirect()->route('feedinfo.index')
        ->with('success','Vendor updated successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Feedinfo $feedinfo)
    {
        $feedinfo->delete();
        return redirect()->route('feedinfo.index')->with('success','animalstock deleted successfully');
        //echo $request->id;die();
        //$delete=Feedinfo::where('id',$request->id)->delete();
    }
}
