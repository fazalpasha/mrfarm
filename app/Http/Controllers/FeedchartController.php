<?php

namespace App\Http\Controllers;

use App\Feedchart;
use App\User;
use Illuminate\Http\Request;
use App\Entities\category;
use App\Entities\subcategory;
use App\Entities\cell;
use Illuminate\Support\Facades\Auth;

class FeedchartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userid = Auth::user();
        $data = Feedchart::leftJoin('subcategory','feedcharts.subcategory_id','=','subcategory.id')->leftJoin('category','feedcharts.category_id','=','category.id')->leftJoin('users','users.id','=','feedcharts.user_id')->leftJoin('cell_box','cell_box.id','=','category.id')->where('feedcharts.branch_id','!=',NULL)->where('feedcharts.branch_id','=',$userid->branch_id)->select('category.category_name','subcategory.subcategory_name','users.first_name','feedcharts.*')->paginate();
        return view('feed.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = category::all();//leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Food')->orWhere('top_categories.topcategory_name','=','Medical')->select('category.*')->get();
        $cell = cell::all();
        $add = User::get();
        return view('feed.create',compact('category','cell','add'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = Auth::user();
        $request->merge(['branch_id'=>$userid->branch_id,'user_id'=>$userid->id]);
        $Feedchart = Feedchart::create($request->all());
        return redirect()->route('feed.index')
        ->with('success','Feed created successfully'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function show(Feedchart $feedchart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedchart $feedchart)
    {
        $category = category::all();//leftJoin('top_categories','category.type','=','top_categories.id')->where('top_categories.topcategory_name','=','Food')->orWhere('top_categories.topcategory_name','=','Medical')->select('category.*')->get();
        $cell = cell::all();
        $add = User::get();
        return view('feed.edit',compact('feedchart','category','cell','add'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedchart $feedchart)
    {
        Feedchart::find($feedchart->id)->update($request->all());
        return redirect()->route('feed.index')
        ->with('success','Feedchart updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedchart  $feedchart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedchart $feedchart)
    {
        Feedchart::find($feedchart->id)->delete();
        return redirect()->route('feed.index')
        ->with('success','Feedchart deleted successfully');
    }
}
