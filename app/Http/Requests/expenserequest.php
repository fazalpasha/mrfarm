<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class expenserequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'=>'required',
            'time'=>'required',
            'added_by'=>'required',
            'paid_to'=>'required',
            'vendor_id'=>'required',
            'area'=>'required',
            'receipt_no'=>'required',
            'particular'=>'required',
            'payment_method'=>'required',
            'quantity'=>'required',
            'amount'=>'required',
            'total'=>'required',
            //'image'=>'required',
            'remarks'=>'required',
        ];
    }
}
