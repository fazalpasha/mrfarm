<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class stockupdaterequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'animal_name' => 'required',
            'sub_category' => 'required',
            'total_weight' => 'required',
            'vendor_name' => 'required',
            'billing_amount' => 'required',
            'receipt_no' => 'required',
            'date' => 'required',
            'remarks' => 'required',
           
        ];
    }
}
