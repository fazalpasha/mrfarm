<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class productionstockrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_of_item'=>'required',
            'quality'=>'required',
            'billing_amount'=>'required',
            'document_upload'=>'required',
            'date'=>'required',
            'received_by_name'=>'required',
            'remarks'=>'required',
            'category'=>'required',
            'vendor'=>'required',
        ];
    }
}
