<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class staffrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'age'=>'required|max:3',
            'qualification'=>'required',
            'last_name'=>'required',
            'blood_group'=>'required',
            'email' => 'email|required',
            'phone' => 'required|unique:users|max:10|min:10',
            'dob' =>'required',
            'date_of_join'=>'required',
            'roles' => 'required',
            'status' => 'required|nullable|min:1',
            'gender' => 'required|nullable|min:1',
            'salary' =>'required',
            'assigned_branch' =>'required',
        ];
    }
}
