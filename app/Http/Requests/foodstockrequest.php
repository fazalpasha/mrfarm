<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class foodstockrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qrcode'=>'required',
            'item_name'=>'required',
            // 'vendor_name'=>'required',
            // 'vendor_id'=>'required',
            'billing_amount'=>'required',
            'receipt_no'=>'required',
            'invoice_no'=>'required',
            'invoice_prefix'=>'required',
            'quality'=>'required',
            'quantity'=>'required',
            'date'=>'required',
            'remarks'=>'required',
            'received_by_name'=>'required',
           // 'image'=>'required',
           'category_id'=>'required',
           'subcategory_id'=>'required',

        ];
    }
}
