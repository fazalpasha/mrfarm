<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class vehicalreportsrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehical_no' => 'required',
            'vehical_type' => 'required',
            'wheels' => 'required',
            'driver_name' => 'required',
            'weight'=>'required',
            // 'type'=>'required',
            // 'status'=>'required',
            'remarks'=>'required',
   
        ];
    }
}
