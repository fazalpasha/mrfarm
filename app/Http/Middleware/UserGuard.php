<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'api')
    {

        $request->headers->set('Accept', 'application/json');


    	if(!$request->header('Authorization')){
           
            return response(['message' => 'token not found'],401);
            
        }else{

	        if (Auth::guard($guard)->check()) {
	           
	           		auth()->shouldUse('api');
	          	   $userdetails = auth()->user();
	           if(isset($userdetails->id)){

	           		return $next($request);

	           }else{

	           		return $next($request);
	           }
	        }

	        return response(['message' => 'invalid token'],401);

	     }

        
    }
}
