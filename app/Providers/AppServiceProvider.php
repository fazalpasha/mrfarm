<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\AttendenceRepository',
            'App\Repositories\AttendenceRepositoryInterface'
        );
        $this->app->bind(
            'App\Repositories\LeaveRepository',
            'App\Repositories\LeaveRepositoryInterface'
        );
    }
}
