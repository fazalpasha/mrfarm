<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard_name = 'guard';
    protected $fillable = [
        'username','first_name','last_name', 'email', 'phone', 'created_at', 'updated_at', 'roles', 'present_address', 'access_key', 'gender', 'acc_num', 'pf_num', 'esi_num', 'finger_keys', 'finger_hand', 'age', 'uan_no', 'qualification', 'blood_group', 'known_language', 'dob', 'date_of_join', 'nominee_name', 'relatioship', 'shift', 'cast', 'religion', 'releaved_date', 'is_leader', 'salary_id', 'marital_status', 'refered_by', 'service_id', 'date_of_leave', 'reason', 'branch_name', 'emp_id', 'service_dept', 'ifsc_num', 'city', 'state', 'ref1_rel', 'refered2_by', 'refered2_phone', 'ref2_rel', 'father_name', 'mother_name', 'bank_name', 'ref1doc_copy', 'ref2doc_copy', 'idproof_copy', 'not_ofattedance','address','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
