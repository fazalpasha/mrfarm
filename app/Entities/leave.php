<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class leave extends Model
{
    protected $table = 'staff_leaves';
    public $translatedAttributes = [];
    protected $fillable = ['user_id', 'start_date', 'end_date', 'reason', 'status','branch_id'];
}
