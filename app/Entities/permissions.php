<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class permission extends Model
{
    protected $table = 'role_has_permissions';
    public $translatedAttributes = [];
    protected $fillable = ['permission_id','role_id','branch_id'];
}
