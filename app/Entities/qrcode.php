<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class qrcode extends Model
{
    protected $table = 'qr_code';
    public $translatedAttributes = [];
    protected $fillable = ['id','qr_code', 'prefix', 'type', 'status','branch_id'];
}
