<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class sales extends Model
{
    protected $table = 'sales';
    public $translatedAttributes = [];
    protected $fillable = ['vendor_name', 'vendor_address', 'vendor_phone', 'gst', 'sub_price', 'gst_price', 'total_price', 'total_weight', 'user_id', 'vendor_id', 'topcat_id','status','discount_amount','total_animal','amount_paid','resale','paid_type','branch_id'];

    public function saleitems()
    {
        return $this->hasmany('App\Entities\salesdetail','sales_id');
    }
}
