<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class batch extends Model
{
    protected $table = 'batch';
    public $translatedAttributes = [];
    protected $fillable = ['id','batch_name', 'created_date', 'created_by' , 'category_id', 'subcategory_id', 'total_animals', 'purchased_amount', 'total_weight' , 'branch_id'];
}
