<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class topcategory extends Model
{
    protected $table = 'top_categories';
    public $translatedAttributes = [];
    protected $fillable = ['id','topcategory_name','status','created_at','updated_at','branch_id'];
}
