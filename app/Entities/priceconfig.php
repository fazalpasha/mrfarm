<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class priceconfig extends Model
{
    protected $table = 'config';
    public $translatedAttributes = [];
    protected $fillable = ['date', 'price', 'weight', 'category_id','branch_id'];
}
