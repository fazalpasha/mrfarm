<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    protected $table = 'branch';
    public $translatedAttributes = [];
    protected $fillable = ['id','name', 'email', 'mobilenumber' , 'pincode', 'city', 'state', 'country', 'ime' , 'branch_code' , 'address' , 'image' , 'lat' , 'lng'];
}
