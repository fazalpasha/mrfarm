<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class entrylogs extends Model
{
    protected $table = 'entrylogs';
    public $translatedAttributes = [];
    protected $fillable = ['id','first_name','last_name','reason','phone','created_at','updated_at','branch_id'];
}
