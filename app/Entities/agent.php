<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class agent extends Model
{
    protected $table = 'agent';
    public $translatedAttributes = [];
    protected $fillable = ['id','agent_name', 'phone', 'address' , 'photo','branch_id'];
}
