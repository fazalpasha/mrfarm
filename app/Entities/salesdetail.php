<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class salesdetail extends Model
{
    protected $table = 'sales_detail';
    public $translatedAttributes = [];
    protected $fillable = ['animal_id', 'price', 'weight', 'vendor_id', 'sales_id','name','qty','branch_id'];
}
