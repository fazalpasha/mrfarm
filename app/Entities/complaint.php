<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class complaint extends Model
{
    protected $table = 'complaint';
    public $translatedAttributes = [];
    protected $fillable = ['id','categoryname','description','image','remarks','vendor_id','created_at','updated_at','branch_id'];
}
