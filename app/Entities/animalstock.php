<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class animalstock extends Model
{
    protected $table = 'animalstock';
    public $translatedAttributes = [];
    protected $fillable = ['id','qrcode','total_weight','vendor_name','vendor_id','billing_amount','receipt_no','date','remarks','health_check','physically_cahllenge','any_allergy','pregnent','verified','cell_number','category_id','subcategory_id','branch_id','created_at','updated_at'];
}
