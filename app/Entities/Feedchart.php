<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class feedchart extends Model
{
    protected $table = 'feedcharts';
    public $translatedAttributes = [];
    protected $fillable = ['id','quantity','units','cell_number','date_given','category_id','subcategory_id','branch_id','created_at','updated_at'];
}
