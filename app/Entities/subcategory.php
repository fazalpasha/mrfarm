<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class subcategory extends Model
{
    protected $table = 'subcategory';
    public $translatedAttributes = [];
    protected $fillable = ['id','subcategory_name','category_id','status','created_at','updated_at','branch_id'];
}
