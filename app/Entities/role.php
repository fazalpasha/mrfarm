<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    public $translatedAttributes = [];
    protected $fillable = ['id','name','guard_name','slug_name','created_at','updated_at','branch_id'];


}
