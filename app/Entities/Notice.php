<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $table = 'notice';
    public $translatedAttributes = [];
    protected $fillable = ['from_user_id', 'to_user_id', 'sub_title', 'date', 'description','branch_id'];


    public function touser()
	{
	    return $this->belongsTo("App\User","to_user_id","id")->select('first_name','last_name');
	}

	public function fromuser()
	{
	    return $this->belongsTo("App\User","from_user_id","id")->select('first_name','last_name');;
	}
}
