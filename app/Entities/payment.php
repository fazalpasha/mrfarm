<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    protected $table = 'payments';
    public $translatedAttributes = [];
    protected $fillable = ['id','sale_id', 'paid_amount', 'paid_type' , 'recipt_no','date'];
}
