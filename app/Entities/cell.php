<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class cell extends Model
{
    protected $table = 'cell_box';
    public $translatedAttributes = [];
    protected $fillable = ['id','cell_name','max_allowed','details','created_at','updated_at','branch_id'];
}
