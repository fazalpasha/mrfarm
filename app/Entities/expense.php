<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class expense extends Model
{
    protected $table = 'expense';
    public $translatedAttributes = [];

    protected $fillable = ['id','date','time','added_by','paid_to','vendor_id','area','receipt_no','particular','payment_method','quantity','amount','total','image','remarks','status','created_at','branch_id'];
    
}
