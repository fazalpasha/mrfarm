<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class transport extends Model
{
    protected $table = 'transport_vehicle';
    public $translatedAttributes = [];
    protected $fillable = ['id', 'vehicle', 'total_animal', 'start_place', 'end_place', 'start_time', 'end_time', 'health_certificate', 'transport_certificate', 'remarks', 'type', 'created_at', 'updated_at','branch_id'];
}
