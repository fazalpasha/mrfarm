<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class payslip extends Model
{
    protected $table = 'staffsalaries';
    public $translatedAttributes = [];
    protected $fillable = ['id','user_id','basic','HRA','TA','DA','LTC','special_allowance','medical_allowance','proffesional_development','D_PF','D_Proff_tax','slip_month','loss_pay_perday','ot_perday','ref_date','status','year','created_at','updated_at','branch_id'];
}
