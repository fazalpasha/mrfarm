<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = 'category';
    public $translatedAttributes = [];
    protected $fillable = ['id','category_name','type','image','gender','status','created_at','updated_at','branch_id'];


    public function subcategory()
    {
        return $this->hasmany('App\Entities\subcategory','category_id');
    }

}
