<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class place extends Model
{
    protected $table = 'states';
    public $translatedAttributes = [];
    protected $fillable = ['id','name','country_id','created_at','updated_at'];
}
