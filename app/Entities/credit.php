<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class credit extends Model
{
    protected $table = 'credit';
    public $translatedAttributes = [];
    protected $fillable = ['id','date','time','payment_method','amount','paid_by','paid_to','remarks','branch_id','created_at','updated_at'];
}
