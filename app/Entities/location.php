<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class location extends Model
{
    protected $table = 'location';
    public $translatedAttributes = [];
    protected $fillable = ['id','locationname', 'district', 'state', 'description','branch_id'];
}
