<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class production extends Model
{
    protected $table = 'production';
    public $translatedAttributes = [];
    protected $fillable = ['id','quality','quantity','units','added_by','cell_id','category_id','subcategory_id','branch_id','created_at','updated_at','date','fault_in','status'];
}
