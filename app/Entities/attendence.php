<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class attendence extends Model
{
    protected $table = 'staff_attedances';
    public $translatedAttributes = [];
    protected $fillable = ['user_id', 'punch_in', 'punch_out', 'in_date', 'punch_in_time', 'punch_out_time', 'completed', 'shift_no', 'name_app', 'is_absent', 'is_manual', 'is_changed','branch_id'];
}
