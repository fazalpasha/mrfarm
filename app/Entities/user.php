<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;


class user extends Authenticatable
{
    use  Notifiable;
    use HasRoles;
    protected $table = 'users';
    public $translatedAttributes = [];
    protected $fillable = ['username','first_name','last_name', 'email', 'phone', 'password', 'remember_token', 'created_at', 'updated_at', 'roles', 'present_address','address', 'access_key', 'gender', 'acc_num', 'pf_num', 'esi_num', 'finger_keys', 'finger_hand', 'age', 'uan_no', 'qualification', 'blood_group', 'known_language', 'dob', 'date_of_join', 'nominee_name', 'relatioship', 'shift', 'cast', 'religion', 'releaved_date', 'is_leader', 'salary_id', 'salary','designation', 'marital_status', 'refered_by', 'service_id', 'date_of_leave', 'reason', 'branch_name', 'emp_id', 'service_dept', 'ifsc_num', 'city', 'state', 'ref1_rel', 'refered2_by', 'refered2_phone', 'ref2_rel', 'father_name', 'mother_name', 'bank_name', 'ref1doc_copy', 'ref2doc_copy', 'idproof_copy', 'not_ofattedance','branch_id','assigned_branch'];


}
