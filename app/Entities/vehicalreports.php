<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class vehicalreports extends Model
{
    protected $table = 'vehicalreports';
    public $translatedAttributes = [];
    protected $fillable = ['id','vehical_no','vehical_type','wheels','driver_name','weight','entry_time','exit_time','load_type','type','remarks','status','created_at','updated_at','branch_id'];
}
