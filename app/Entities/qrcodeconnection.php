<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class qrcodeconnection extends Model
{
    protected $table = 'qr_connection';
    public $translatedAttributes = [];
    protected $fillable = ['qr_id', 'animal_id', 'status','branch_id'];
}
