<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class vendor extends Model
{
    protected $table = 'vendor';
    public $translatedAttributes = [];
    protected $fillable = ['id','vendor_name','email','phone','city','address','subcategory_id','bengaluru','created_at','updated_at','branch_id'];
}
