<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class medicalstock extends Model
{
    protected $table = 'foodmedicalstock';
    public $translatedAttributes = [];
    protected $fillable = ['id','qrcode','item_name','vendor_name','vendor_id','billing_amount','receipt_no','invoice_no','invoice_prefix','quality','quantity','date','remarks','received_by_name','document_upload','category_id','subcategory_id','created_at','updated_at','branch_id'];
}
