<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Feedinfo extends Model
{
    protected $table = 'feedinfo';
    public $translatedAttributes = [];
    protected $fillable = ['id','title','description','category_id','subcategory_id','created_at','updated_at','branch_id'];

    public function Subcategory()
    {
    	return $this->hasOne('App\Entities\subcategory', 'subcategory_id');
    }
    public function category()
    {
    	return $this->hasOne('App\Entities\category', 'id');
    }
}
