<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffsalaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('basic')->default(0);
            $table->integer('HRA')->default(0);
            $table->integer('TA')->default(0);
            $table->integer('DA')->default(0);
            $table->integer('LTC')->default(0);
            $table->integer('special_allowance')->default(0);
            $table->integer('medical_allowance')->default(0);
            $table->integer('proffesional_development')->default(0);
            $table->integer('D_PF')->default(0);
            $table->integer('D_Proff_tax')->default(0);

            $table->integer('slip_month')->default(0);
            $table->integer('loss_pay_perday')->default(0);
            $table->integer('ot_perday')->default(0);
            $table->date('ref_date');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffsalaries');
    }
}
