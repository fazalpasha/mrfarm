<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQrCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qr_code', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qr_code');
            $table->string('prefix');
            $table->unsignedInteger('type')->nullable();
            $table->foreign('type')->references('id')->on('top_categories')->onDelete('set null');
            $table->tinyInteger('status');
            $table->timestamps();
        });
        Schema::create('qr_connection', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('qr_id')->nullable();
            $table->foreign('qr_id')->references('id')->on('qr_code')->onDelete('set null');
            $table->unsignedInteger('animal_id')->nullable();
            $table->foreign('animal_id')->references('id')->on('animalstock')->onDelete('set null');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qr_code');
    }
}
