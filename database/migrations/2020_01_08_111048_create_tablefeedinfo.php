<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablefeedinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedinfo', function (Blueprint $table) {
             $table->increments('id');
             $table->string('title')->nullable();
             $table->text('description')->nullable();
             $table->unsignedInteger('category_id')->nullable();
             $table->unsignedInteger('subcategory_id')->nullable();
             $table->foreign('subcategory_id')->references('id')->on('subcategory')->onDelete('set null');
             $table->foreign('category_id')->references('id')->on('category')->onDelete('set null');

             $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feedinfo', function (Blueprint $table) {
            //
        });
    }
}
