<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transport_vehicle', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vehicle');
            $table->Integer('total_animal');
            $table->string('start_place');
            $table->string('end_place');
            $table->date('start_time');
            $table->date('end_time');
            $table->string('health_certificate');
            $table->string('transport_certificate');
            $table->string('remarks');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_vehicle');
    }
}
