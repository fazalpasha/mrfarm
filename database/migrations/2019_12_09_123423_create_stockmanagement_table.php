<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockmanagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

       Schema::create('cell_box', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cell_name');
            $table->string('max_allowed');
            $table->string('details');
            $table->timestamps();
        });

        Schema::create('animalstock', function (Blueprint $table) {
             $table->increments('id');
              $table->string('qrcode');
              $table->integer('total_weight');
              $table->string('vendor_name');
              $table->integer('vendor_id')->nullable();
              $table->decimal('billing_amount',10,2); 
              $table->string('receipt_no'); 
            $table->date('date'); 
            $table->text('remarks');
            $table->boolean('health_check')->default(0);
            $table->boolean('physically_cahllenge')->default(0);
            $table->boolean('any_allergy')->default(0);
            $table->boolean('pregnent')->default(0);
            $table->boolean('verified')->default(0);
            $table->unsignedInteger('cell_number')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('subcategory_id')->nullable();
            $table->foreign('subcategory_id')->references('id')->on('subcategory')->onDelete('set null');

            $table->foreign('cell_number')->references('id')->on('cell_box')->onDelete('set null');
          
            $table->foreign('category_id')->references('id')->on('category')->onDelete('set null');
            $table->timestamps();
        });

         Schema::create('foodmedicalstock', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qrcode')->nullable();
            $table->string('item_name')->nullable();
            $table->string('vendor_name')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('billing_amount')->default(0); 
            $table->string('receipt_no')->nullable(); 
            $table->string('invoice_no')->nullable();
            $table->string('invoice_prefix')->nullable();
            $table->string('quality')->nullable(); 
            $table->decimal('quantity',10,2); 
            $table->date('date'); 
            $table->text('remarks')->nullable();
            $table->string('received_by_name')->nullable();
            $table->string('document_upload')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('subcategory_id')->nullable();
            $table->foreign('subcategory_id')->references('id')->on('subcategory')->onDelete('set null');
        
            $table->foreign('category_id')->references('id')->on('category')->onDelete('set null');
            $table->timestamps();
        });

           Schema::create('stockmanagement', function (Blueprint $table) {
              $table->increments('id');
              $table->string('qrcode');
              $table->string('item_name')->nullable();
              $table->string('vendor_name');
              $table->integer('vendor_id')->nullable();
              $table->integer('billing_amount')->default(0); 
              $table->string('receipt_no'); 
              $table->string('invoice_no'); 
              $table->string('invoice_prefix');
              $table->string('quality'); 
              $table->decimal('quantity',10,2); 
              $table->date('date'); 
              $table->text('remarks');
              $table->string('received_by_name');
                $table->string('document_upload')->nullable();
              $table->unsignedInteger('category_id')->nullable();
              $table->unsignedInteger('subcategory_id')->nullable();
              $table->foreign('subcategory_id')->references('id')->on('subcategory')->onDelete('set null');
           
              $table->foreign('category_id')->references('id')->on('category')->onDelete('set null');
               $table->timestamps();
        });

          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockmanagement');
    }
}
