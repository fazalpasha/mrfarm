<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffAttedancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_attedances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamp('punch_in')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('punch_out')->nullable();
            $table->date('in_date');
            $table->time('punch_in_time');
            $table->time('punch_out_time');
            $table->boolean('completed')->default(0);
            $table->integer('shift_no');
            $table->boolean('name_app')->default(1);
            $table->boolean('is_absent')->default(0);
            $table->boolean('is_manual')->default(0);
            $table->boolean('is_changed')->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_attedances');
    }
}
