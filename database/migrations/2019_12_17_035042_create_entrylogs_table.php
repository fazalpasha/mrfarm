<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntrylogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrylogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('reason');
            $table->timestamps();
        });

         Schema::create('vehicalreports', function (Blueprint $table) {
             $table->increments('id');
              $table->string('vehical_no');
              $table->string('vehical_type');
              $table->string('wheels');
              $table->string('driver_name');
            $table->string('weight'); 
            $table->dateTime('entry_time')->nullable(); 
            $table->dateTime('exit_time')->nullable();
                 $table->string('load_type');
            $table->string('remarks');
            $table->boolean('type')->default(0);
            $table->boolean('status')->default(0);
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrylogs');
    }
}
