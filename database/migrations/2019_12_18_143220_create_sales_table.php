<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendor_name')->nullable();
            $table->text('vendor_address')->nullable();
            $table->text('vendor_phone')->nullable();
            $table->integer('gst')->nullable();
            $table->integer('expenses');
            $table->decimal('sub_price',10,2);
            $table->decimal('gst_price',10,2);
            $table->decimal('total_price',10,2);
            $table->integer('total_weight');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->unsignedInteger('vendor_id')->nullable();
            $table->foreign('vendor_id')->references('id')->on('vendor')->onDelete('set null');
            $table->unsignedInteger('topcat_id')->nullable();
            $table->foreign('topcat_id')->references('id')->on('top_categories')->onDelete('set null');
            $table->timestamps();
        });
        Schema::create('sales_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('animal_id');
            $table->decimal('price',10,2);
            $table->integer('weight');
            $table->unsignedInteger('sales_id')->nullable();
            $table->foreign('sales_id')->references('id')->on('sales')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('config', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->decimal('price',10,2);
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
