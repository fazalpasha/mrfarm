<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  
         public function run()
    {
       $permissions = [
           'dashboard-list',
           'dashboard-create',
           'dashboard-edit',
           'dashboard-delete',

           'stock-list',
           'stock-create',
           'stock-edit',
           'stock-delete',

           'animal-list',
           'animal-create',
           'animal-edit',
           'animal-delete',


           'staff-list',
           'staff-create',
           'staff-edit',
           'staff-delete',

           'production-list',
           'production-create',
           'production-edit',
           'production-delete',

            'statistics-list',
           'statistics-create',
           'statistics-edit',
           'statistics-delete',

            'feed-list',
           'feed-create',
           'feed-edit',
           'feed-delete',

          'plants-list',
           'plants-create',
           'plants-edit',
           'plants-delete',

          'vendor-list',
           'vendor-create',
           'vendor-edit',
           'vendor-delete',

            'birth-list',
           'birth-create',
           'birth-edit',
           'birth-delete',

            'qrcode-list',
           'qrcode-create',
           'qrcode-edit',
           'qrcode-delete',

          'dailyentry-list',
           'dailyentry-create',
           'dailyentry-edit',
           'dailyentry-delete',

          'supplyvehicle-list',
           'supplyvehicle-create',
           'supplyvehicle-edit',
           'supplyvehicle-delete',


           'biometric-list',
           'biometric-create',
           'biometric-edit',
           'biometric-delete',


        ];


        foreach ($permissions as $permissions) {
             Permission::create(['name' => $permissions]);
        }
    }
    
}
