<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//api employee routes
$router->group(['prefix' =>'/authentication/employee'], function (Router $router) {

    $router->post('/branchaccess', [
        'as' => 'authentication.api.branchaccess',
        'uses' => 'Api\StaffApiController@branchaccess'
    ]);
    $router->post('/branchdetails', [
        'as' => 'authentication.api.branchdetails',
        'uses' => 'Api\StaffApiController@branchdetails'
    ]);
    $router->post('/staffassigns', [
        'as' => 'authentication.api.staffassigns',
        'uses' => 'Api\StaffApiController@staffassigns',
    ]);

    Route::group([], function(Router $router){
        $router->post('/attedance', [
            'as' => 'staff.api.staffattedance',
            'uses' => 'Api\StaffApiController@staffattedance',
            'middleware'=>'jwt.user'
        ]);
        $router->get('/leavelist', [
            'as' => 'staff.api.leavelist',
            'uses' => 'Api\StaffApiController@leavelist',
            'middleware'=>'jwt.user'
        ]);
        $router->get('/leavestatistics', [
            'as' => 'staff.api.leavestatistics',
            'uses' => 'Api\StaffApiController@leavestatistics',
        ]);
        $router->post('/applyleave', [
            'as' => 'staff.api.applyleave',
            'uses' => 'Api\StaffApiController@applyleave',
            'middleware'=>'jwt.user'
        ]);
        $router->post('/addtrace', [
            'as' => 'staff.api.addtrace',
            'uses' => 'Api\StaffApiController@addtrace',
        ]);
        $router->post('/logattedance', [
            'as' => 'staff.api.logattedance',
            'uses' => 'Api\StaffApiController@logattedance',
        ]);
        $router->post('/staffassigns', [
            'as' => 'authentication.api.staffassigns',
            'uses' => 'Api\StaffApiController@staffassigns',
        ]);
    });

});
//api end  employee routes

$router->group(['prefix' => '/authentication'], function (Router $router) {

        $router->post('/login', [
            'as' => 'authentication.api.login',
            'uses' => 'Api\BasicController@login'
        ]);


        $router->post('/forgotsendotp', [
            'as' => 'authentication.api.forgotsendotp',
            'uses' => 'Api\BasicController@forgotsendotp'
        ]);


        $router->post('/forgotverifyotp', [
            'as' => 'authentication.api.forgotverifyotp',
            'uses' => 'Api\BasicController@forgotverifyotp'
        ]);


        $router->get('/profile', [
            'as' => 'authentication.api.profile',
            'uses' => 'Api\BasicController@profile',
            'middleware'=>'jwt.user'
        ]);

        

        $router->post('/feed/add', [
            'as' => 'authentication.api.feedadd.create',
            'uses' => 'Api\BasicController@FeedchartCreate',
            'middleware'=>'jwt.user'
        ]);

        $router->post('/log/add', [
            'as' => 'authentication.api.log.create',
            'uses' => 'Api\BasicController@Entrylogadd',
            'middleware'=>'jwt.user'
        ]);


         $router->get('/log/list', [
            'as' => 'authentication.api.log.list',
            'uses' => 'Api\BasicController@Entryloglist',
            'middleware'=>'jwt.user'
        ]);

         $router->get('/qrscaned', [
            'as' => 'authentication.api.log.qrscaned',
            'uses' => 'Api\BasicController@qrscaned',
            'middleware'=>'jwt.user'
        ]);

         $router->get('/animaldetails', [
            'as' => 'authentication.api.log.animaldetails',
            'uses' => 'Api\BasicController@animaldetails',
            'middleware'=>'jwt.user'
        ]);

         $router->get('/reports', [
            'as' => 'authentication.api.log.reports',
            'uses' => 'Api\BasicController@reports',
            'middleware'=>'jwt.user'
        ]);

        $router->get('/feed/list', [
            'as' => 'authentication.api.feedadd.add',
            'uses' => 'Api\BasicController@DailyFeedList',
            'middleware'=>'jwt.user'
        ]);

        $router->post('/qranimal', [
            'as' => 'authentication.api.qranimal.add',
            'uses' => 'Api\BasicController@qranimal',
            'middleware'=>'jwt.user'
        ]);


        $router->post('/sale/store', [
            'as' => 'authentication.api.salestore.add',
            'uses' => 'Api\BasicController@salestore',
            'middleware'=>'jwt.user'
        ]);

        $router->post('/resale/store', [
            'as' => 'authentication.api.resalestore.add',
            'uses' => 'Api\BasicController@resalestore',
            'middleware'=>'jwt.user'
        ]);

        $router->post('/updatestatus', [
            'as' => 'authentication.api.updatestatus.add',
            'uses' => 'Api\BasicController@updatestatus',
            'middleware'=>'jwt.user'
        ]);

        $router->get('/dutystatus', [
            'as' => 'authentication.api.dutystatus.list',
            'uses' => 'Api\BasicController@dutystatus',
            'middleware'=>'jwt.user'
        ]);

        $router->get('/userlists', [
            'as' => 'authentication.api.userlists.list',
            'uses' => 'Api\BasicController@userlists',
            'middleware'=>'jwt.user'
        ]);

        $router->get('/sale/list', [
            'as' => 'authentication.api.saleslist.add',
            'uses' => 'Api\BasicController@saleslist',
            'middleware'=>'jwt.user'
        ]);


        $router->get('/sale/create', [
            'as' => 'authentication.api.createsales.list',
            'uses' => 'Api\BasicController@createsales',
            'middleware'=>'jwt.user'
        ]);

        $router->get('/sale/details', [
            'as' => 'authentication.api.salesdetails.list',
            'uses' => 'Api\BasicController@salesdetails',
            'middleware'=>'jwt.user'
        ]);


        $router->get('/production/list', [
            'as' => 'authentication.api.production.list',
            'uses' => 'Api\BasicController@ProductionList',
            'middleware'=>'jwt.user'
        ]);


        $router->get('/today/attedancelist', [
            'as' => 'authentication.api.today.list',
            'uses' => 'Api\StaffApiController@todayattedancelist'
        ]);


        $router->post('/production/add', [
            'as' => 'authentication.api.production.list',
            'uses' => 'Api\BasicController@ProductionCreate',
            'middleware'=>'jwt.user'
        ]);

        $router->post('/topcategory/create', [
            'as' => 'authentication.api.topcategory.create',
            'uses' => 'Api\BasicController@TopCategoryCreate'
            // 'middleware' => 'jwt.user'
        ]);

        $router->get('/topcategory/edit', [
        'as' => 'authentication.api.topcategory.edit',
        'uses' => 'Api\BasicController@TopCategoryEdit'
        ]);

        $router->post('/topcategory/update', [
        'as' => 'authentication.api.topcategory.update',
        'uses' => 'Api\BasicController@TopCategoryUpdate',
        ]);

        $router->get('/topcategory/list', [
        'as' => 'authentication.api.topcategory.list',
        'uses' => 'Api\BasicController@TopCategoryList'
        ]);

        $router->post('/category/create', [
            'as' => 'authentication.api.category.create',
            'uses' => 'Api\BasicController@CategoryCreate'
        ]);

        $router->get('/category/edit', [
        'as' => 'authentication.api.category.edit',
        'uses' => 'Api\BasicController@CategoryEdit'
        ]);

        $router->post('/category/update', [
        'as' => 'authentication.api.category.update',
        'uses' => 'Api\BasicController@CategoryUpdate',
        ]);

        $router->get('/category/list', [
        'as' => 'authentication.api.category.list',
        'uses' => 'Api\BasicController@CategoryList'
        ]);

        $router->post('/subcategory/create', [
        'as' => 'authentication.api.subcategory.create',
        'uses' => 'Api\BasicController@SubCategoryCreate'
        ]);

        $router->get('/subcategory/edit', [
        'as' => 'authentication.api.subcategory.edit',
        'uses' => 'Api\BasicController@SubCategoryEdit'
        ]);

        $router->post('/subcategory/update', [
        'as' => 'authentication.api.subcategory.update',
        'uses' => 'Api\BasicController@SubCategoryUpdate',
        ]);

        $router->get('/subcategory/list', [
        'as' => 'authentication.api.subcategory.list',
        'uses' => 'Api\BasicController@SubCategoryList'
        ]);

        
        $router->get('/notice/list', [
        'as' => 'authentication.api.notice.list',
        'uses' => 'Api\BasicController@noticelist',
        'middleware'=>'jwt.user'
        ]);


        $router->post('/uploadimg/store', [
        'as' => 'authentication.api.uploadimg.store',
        'uses' => 'Api\BasicController@uploadimg',
        'middleware'=>'jwt.user'
        ]);

        $router->post('/notice/store', [
        'as' => 'authentication.api.notice.store',
        'uses' => 'Api\BasicController@noticecreate',
        'middleware'=>'jwt.user'
        ]);



        $router->post('/cellbox/create', [
        'as' => 'authentication.api.cellbox.create',
        'uses' => 'Api\BasicController@CellBoxCreate'
        ]);

         $router->get('/cellbox/report', [
          'as' => 'authentication.api.cellbox.report',
          'uses' => 'Api\BasicController@CellBoxreport'
        ]);

        $router->get('/cellbox/edit', [
        'as' => 'authentication.api.cellbox.edit',
        'uses' => 'Api\BasicController@CellBoxEdit'
        ]);

        $router->post('/cellbox/update', [
        'as' => 'authentication.api.cellbox.update',
        'uses' => 'Api\BasicController@CellBoxUpdate',
        ]);

        $router->get('/cellbox/list', [
        'as' => 'authentication.api.cellbox.list',
        'uses' => 'Api\BasicController@CellBoxList'
        ]);

        $router->get('/division/list', [
        'as' => 'authentication.api.division.list',
        'uses' => 'Api\BasicController@CellList'
        ]);


        $router->post('/qrcode/list', [
        'as' => 'authentication.api.qrcode.create',
        'uses' => 'Api\BasicController@QRCodeList'
        ]);


        $router->post('/animalstock/create', [
        'as' => 'authentication.api.animalstock.create',
        'uses' => 'Api\BasicController@AnimalStockCreate'
        ]);


        $router->post('/foodstock/create', [
        'as' => 'authentication.api.foodstockCreate.create',
        'uses' => 'Api\BasicController@FoodStockCreate'
        ]);

        $router->get('/foodstock/list', [
        'as' => 'authentication.api.foodstocklist.create',
        'uses' => 'Api\BasicController@FoodStockList'
        ]);

        $router->get('/foodstock/details', [
        'as' => 'authentication.api.foodstockdetails.create',
        'uses' => 'Api\BasicController@FoodStockDetails'
        ]);

        $router->post('/foodstock/update', [
        'as' => 'authentication.api.foodstockUpdate.create',
        'uses' => 'Api\BasicController@FoodStockUpdate'
        ]);


         $router->get('/animalstock/edit', [
          'as' => 'authentication.api.animalstock.edit',
          'uses' => 'Api\BasicController@AnimalStockEdit'
        ]);


         $router->get('/category/filter', [
        'as' => 'authentication.api.animalstock.edit',
        'uses' => 'Api\BasicController@CategoryFilter'
        ]);

         

        $router->post('/animalstock/update', [
        'as' => 'authentication.api.animalstock.update',
        'uses' => 'Api\BasicController@AnimalStockUpdate',
        ]);

        $router->get('/animalstock/list', [
        'as' => 'authentication.api.animalstock.list',
        'uses' => 'Api\BasicController@AnimalStockList'
        ]);



        $router->get('/staff/list', [
        'as' => 'authentication.api.stafflist',
        'uses' => 'Api\MobileApiController@stafflist'
        ]);

        $router->get('/staff/details', [
        'as' => 'authentication.api.staffdetails',
        'uses' => 'Api\MobileApiController@staffdetails'
        ]);







  });
