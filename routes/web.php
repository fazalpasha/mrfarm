<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/index', function () {
    return view('dashboard');
    });
Route::view('/login', 'login');

Route::get('/logout', ['middleware' => 'auth',
    'uses' => 'Auth\AuthController@logout',
    'as' => 'logout'
]);

Route::post('/login', [
    'uses' => 'Auth\AuthController@login',
    'as'   => 'login'
]);

$router->group(['prefix' => '/dashboard', 'middleware' => 'auth'], function (Router $router) {

    Route::get('/', ['middleware' => 'auth',
        'uses' => 'AdminController@index',
        'as' => 'dashboard'
    ]);


// Route::view('/stock', 'stock', ['middleware'=>'auth']);
    Route::view('/animal', 'animal', ['middleware' => 'auth']);
    Route::view('/addanimal', 'addanimal', ['middleware' => 'auth']);
// Route::view('/addstock', 'addstock', ['middleware'=>'auth']);
    Route::view('/staff', 'staff', ['middleware' => 'auth']);

    $router->group(['prefix' => '/roles', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('roles', function ($id) {
            return app('Spatie\Permission\Models\Role')->find($id);
        });
        $router->get('/index', ['as' => 'roles.index', 'uses' => 'RoleController@index']);
        $router->get('/create', ['as' => 'roles.create', 'uses' => 'RoleController@create']);
        $router->post('/store', ['as' => 'roles.store', 'uses' => 'RoleController@store']);
        $router->get('/edit/{roles}', ['as' => 'roles.edit', 'uses' => 'RoleController@edit']);
        $router->put('/update/{roles}', ['as' => 'roles.update', 'uses' => 'RoleController@update']);
        $router->get('/delete/{roles}', ['as' => 'roles.delete', 'uses' => 'RoleController@destroy']);
    });




    //QrCode Routes
    $router->group(['prefix' => '/qrcode', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('qrcode', function ($id) {
            return app('Spatie\Permission\Models\Role')->find($id);
        });
        $router->get('/index', ['as' => 'qrcode.index', 'uses' => 'QRcodeController@index']);
        $router->get('/create', ['as' => 'qrcode.create', 'uses' => 'QRcodeController@create']);
        $router->get('/create', ['as' => 'get.count', 'uses' => 'QRcodeController@GetCount']);
        $router->post('/store', ['as' => 'qrcode.store', 'uses' => 'QRcodeController@store']);
        $router->get('/edit/{qrcode}', ['as' => 'qrcode.edit', 'uses' => 'QRcodeController@edit']);
        $router->put('/update/{qrcode}', ['as' => 'qrcode.update', 'uses' => 'QRcodeController@update']);
        $router->get('/delete/{id}', ['as' => 'qrcode.delete', 'uses' => 'QRcodeController@destroy']);
    });
    //QrCode Routes End


    $router->group(['prefix' => '/users', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('users', function ($id) {
            return app('App\user')->find($id);
        });
        $router->get('/index', ['as' => 'users.index', 'uses' => 'UserController@index']);
        $router->get('/create', ['as' => 'users.create', 'uses' => 'UserController@create']);
        $router->post('/store', ['as' => 'users.store', 'uses' => 'UserController@store']);
        $router->get('/edit/{users}', ['as' => 'users.edit', 'uses' => 'UserController@edit']);
        $router->put('/update/{users}', ['as' => 'users.update', 'uses' => 'UserController@update']);
        $router->get('/delete/{users}', ['as' => 'users.delete', 'uses' => 'UserController@destroy']);
    });


    $router->group(['prefix' => '/cell', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('cell', function ($id) {
            return app('App\Entities\cell')->find($id);
        });
        $router->get('/index', ['as' => 'cell.index', 'uses' => 'CellController@index']);
        $router->get('/create', ['as' => 'cell.create', 'uses' => 'CellController@create']);
        $router->post('/store', ['as' => 'cell.store', 'uses' => 'CellController@store']);
        $router->get('/edit/{cell}', ['as' => 'cell.edit', 'uses' => 'CellController@edit']);
        $router->put('/update/{cell}', ['as' => 'cell.update', 'uses' => 'CellController@update']);
        $router->get('/delete/{cell}', ['as' => 'cell.delete', 'uses' => 'CellController@destroy']);
    });

    $router->group(['prefix' => '/batch', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('batch', function ($id) {
            return app('App\Entities\batch')->find($id);
        });
        $router->get('/index', ['as' => 'batch.index', 'uses' => 'BatchController@index']);
        $router->get('/create', ['as' => 'batch.create', 'uses' => 'BatchController@create']);
        $router->post('/store', ['as' => 'batch.store', 'uses' => 'BatchController@store']);
        $router->get('/edit/{batch}', ['as' => 'batch.edit', 'uses' => 'BatchController@edit']);
        $router->put('/update/{batch}', ['as' => 'batch.update', 'uses' => 'BatchController@update']);
        $router->get('/delete/{batch}', ['as' => 'batch.delete', 'uses' => 'BatchController@destroy']);
    });

    $router->group(['prefix' => '/agent', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('agent', function ($id) {
            return app('App\Entities\agent')->find($id);
        });
        $router->get('/index', ['as' => 'agent.index', 'uses' => 'AgentController@index']);
        $router->get('/create', ['as' => 'agent.create', 'uses' => 'AgentController@create']);
        $router->post('/store', ['as' => 'agent.store', 'uses' => 'AgentController@store']);
        $router->get('/edit/{agent}', ['as' => 'agent.edit', 'uses' => 'AgentController@edit']);
        $router->put('/update/{agent}', ['as' => 'agent.update', 'uses' => 'AgentController@update']);
        $router->get('/delete/{agent}', ['as' => 'agent.delete', 'uses' => 'AgentController@destroy']);
    });


    $router->group(['prefix' => '/stock', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('stock', function ($id) {
            return app('App\Entities\stock')->find($id);
        });

        $router->get('/index', ['as' => 'stock.index','uses' => 'StockController@index']);
        $router->get('/create', ['as' => 'stock.create','uses' => 'StockController@create']);
        $router->post('/store', ['as' => 'stock.store','uses' => 'StockController@store']);
        $router->get('/edit/{stock}', ['as' => 'stock.edit','uses' => 'StockController@edit']);
        $router->put('/update/{stock}', ['as' => 'stock.update','uses' => 'StockController@update']);
        $router->get('/delete/{stock}', ['as' => 'stock.delete','uses' => 'StockController@destroy']);

    });

    $router->group(['prefix' => '/foodstock', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('foodstock', function ($id) {
            return app('App\Entities\foodstock')->find($id);
        });
        $router->get('/index', ['as' => 'foodstock.index', 'uses' => 'FoodstockController@index']);
        $router->get('/create', ['as' => 'foodstock.create', 'uses' => 'FoodstockController@create']);
        $router->get('food/subcategory', ['as' => 'foodstock.subcat', 'uses' => 'FoodstockController@SubCategory']);
        $router->post('/store', ['as' => 'foodstock.store', 'uses' => 'FoodstockController@store']);
        $router->get('/edit/{foodstock}', ['as' => 'foodstock.edit', 'uses' => 'FoodstockController@edit']);
        $router->put('/update/{foodstock}', ['as' => 'foodstock.update', 'uses' => 'FoodstockController@update']);
        $router->get('/delete/{foodstock}', ['as' => 'foodstock.delete', 'uses' => 'FoodstockController@destroy']);
    });

    $router->group(['prefix' => '/medicalstock', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('medicalstock', function ($id) {
            return app('App\Entities\medicalstock')->find($id);
        });
        $router->get('/index', ['as' => 'medicalstock.index', 'uses' => 'MedicalstockController@index']);
        $router->get('/create', ['as' => 'medicalstock.create', 'uses' => 'MedicalstockController@create']);
        $router->post('/store', ['as' => 'medicalstock.store', 'uses' => 'MedicalstockController@store']);
        $router->get('/edit/{foodstock}', ['as' => 'medicalstock.edit', 'uses' => 'MedicalstockController@edit']);
        $router->put('/update/{foodstock}', ['as' => 'medicalstock.update', 'uses' => 'MedicalstockController@update']);
        $router->get('/delete/{foodstock}', ['as' => 'medicalstock.delete', 'uses' => 'MedicalstockController@destroy']);
    });

    $router->group(['prefix' => '/vendor', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('vendor', function ($id) {
            return app('App\Entities\vendor')->find($id);
        });
        $router->get('/index', ['as' => 'vendor.index', 'uses' => 'VendorController@index']);
        $router->get('/create', ['as' => 'vendor.create', 'uses' => 'VendorController@create']);
        $router->post('/store', ['as' => 'vendor.store', 'uses' => 'VendorController@store']);
        $router->get('/edit/{vendor}', ['as' => 'vendor.edit', 'uses' => 'VendorController@edit']);
        $router->put('/update/{vendor}', ['as' => 'vendor.update', 'uses' => 'VendorController@update']);
        $router->get('/delete/{vendor}', ['as' => 'vendor.delete', 'uses' => 'VendorController@destroy']);
    });

    $router->group(['prefix' => '/subcategory', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('subcategory', function ($id) {
            return app('App\Entities\subcategory')->find($id);
        });
        $router->get('/index', ['as' => 'subcategory.index', 'uses' => 'SubcategoryController@index']);
        $router->get('/create', ['as' => 'subcategory.create', 'uses' => 'SubcategoryController@create']);
        $router->post('/store', ['as' => 'subcategory.store', 'uses' => 'SubcategoryController@store']);
        $router->get('/edit/{subcategory}', ['as' => 'subcategory.edit', 'uses' => 'SubcategoryController@edit']);
        $router->put('/update/{subcategory}', ['as' => 'subcategory.update', 'uses' => 'SubcategoryController@update']);
        $router->get('/delete/{subcategory}', ['as' => 'subcategory.delete', 'uses' => 'SubcategoryController@destroy']);
    });


    $router->group(['prefix' => '/category', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('category', function ($id) {
            return app('App\Entities\category')->find($id);
        });
        $router->get('/index', ['as' => 'category.index', 'uses' => 'CategoryController@index']);
        $router->get('/create', ['as' => 'category.create', 'uses' => 'CategoryController@create']);
        $router->post('/store', ['as' => 'category.store', 'uses' => 'CategoryController@store']);
        $router->get('/edit/{category}', ['as' => 'category.edit', 'uses' => 'CategoryController@edit']);
        $router->put('/update/{category}', ['as' => 'category.update', 'uses' => 'CategoryController@update']);
        $router->get('/delete/{category}', ['as' => 'category.delete', 'uses' => 'CategoryController@destroy']);
    });

    $router->group(['prefix' => '/animalstock', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('animalstock', function ($id) {
            return app('App\Entities\animalstock')->find($id);
        });
        $router->get('/index', ['as' => 'animalstock.index', 'uses' => 'AnimalstockController@index']);
        $router->get('/create', ['as' => 'animalstock.create', 'uses' => 'AnimalstockController@create']);
        $router->get('animal/subcategory', ['as' => 'animalstock.subcat', 'uses' => 'AnimalstockController@SubCategory']);
        $router->post('/store', ['as' => 'animalstock.store', 'uses' => 'AnimalstockController@store']);
        $router->get('/edit/{animalstock}', ['as' => 'animalstock.edit', 'uses' => 'AnimalstockController@edit']);
        $router->put('/update/{animalstock}', ['as' => 'animalstock.update', 'uses' => 'AnimalstockController@update']);
        $router->get('/delete/{animalstock}', ['as' => 'animalstock.delete', 'uses' => 'AnimalstockController@destroy']);
        $router->get('/filter', ['as' => 'animalstock.filter', 'uses' => 'AnimalstockController@Fliter']);
    });

    $router->group(['prefix' => '/expense', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('expense', function ($id) {
            return app('App\Entities\expense')->find($id);
        });
        $router->get('/index', ['as' => 'expense.index', 'uses' => 'ExpenseController@index']);
        $router->get('/create', ['as' => 'expense.create', 'uses' => 'ExpenseController@create']);
        $router->post('/store', ['as' => 'expense.store', 'uses' => 'ExpenseController@store']);
        $router->get('/edit/{expense}', ['as' => 'expense.edit', 'uses' => 'ExpenseController@edit']);
        $router->put('/update/{expense}', ['as' => 'expense.update', 'uses' => 'ExpenseController@update']);
        $router->get('/delete/{expense}', ['as' => 'expense.delete', 'uses' => 'ExpenseController@destroy']);
        $router->get('/filter', ['as' => 'expense.filter', 'uses' => 'ExpenseController@Filter']);
    });


    $router->group(['prefix' => '/feed', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('feed', function ($id) {
            return app('App\Entities\production')->find($id);
        });
        $router->get('/index', ['as' => 'feed.index', 'uses' => 'FeedchartController@index']);
        $router->get('/create', ['as' => 'feed.create', 'uses' => 'FeedchartController@create']);
        $router->get('feed/subcategory', ['as' => 'feed.subcat', 'uses' => 'FeedchartController@SubCategory']);
        $router->post('/store', ['as' => 'feed.store', 'uses' => 'FeedchartController@store']);
        $router->get('/edit/{feedchart
        }', ['as' => 'feed.edit', 'uses' => 'FeedchartController@edit']);

        $router->put('/feed/{feedchart}', ['as' => 'feed.update', 'uses' => 'FeedchartController@update']);

        $router->get('/feed/{feedchart}', ['as' => 'feed.delete', 'uses' => 'FeedchartController@destroy']);
    });


    $router->group(['prefix' => '/feedinfo', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('feedinfo', function ($id) {

            return app('App\Entities\Feedinfo')->find($id);
        });
        $router->get('/index', ['as' => 'feedinfo.index', 'uses' => 'FeedinfoController@index']);
        $router->get('/create', ['as' => 'feedinfo.create', 'uses' => 'FeedinfoController@create']);
        // $router->get('feed/subcategory', ['as' => 'feed.subcat', 'uses' => 'FeedchartController@SubCategory']);
        $router->post('/store', ['as' => 'feedinfo.store', 'uses' => 'FeedinfoController@store']);
        $router->get('/edit/{feedinfo}', ['as' => 'feedinfo.edit', 'uses' => 'FeedinfoController@edit']);

        $router->put('/feedinfo/{feedinfo}', ['as' => 'feedinfo.update', 'uses' => 'FeedinfoController@update']);

        $router->get('/delete/{feedinfo}', ['as' => 'feedinfo.delete', 'uses' => 'FeedinfoController@destroy']);
    });










    $router->group(['prefix' => '/production', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('production', function ($id) {
            return app('App\Entities\production')->find($id);
        });
        $router->get('/index', ['as' => 'production.index', 'uses' => 'ProductionController@index']);
        $router->get('/create', ['as' => 'production.create', 'uses' => 'ProductionController@create']);
        $router->get('production/subcategory', ['as' => 'production.subcat', 'uses' => 'ProductionController@SubCategory']);
        $router->post('/store', ['as' => 'production.store', 'uses' => 'ProductionController@store']);
        $router->get('/edit/{production}', ['as' => 'production.edit', 'uses' => 'ProductionController@edit']);
        $router->put('/update/{production}', ['as' => 'production.update', 'uses' => 'ProductionController@update']);
        $router->get('/delete/{production}', ['as' => 'production.delete', 'uses' => 'ProductionController@destroy']);
         $router->get('/filter', ['as' => 'production.filter', 'uses' => 'ProductionController@Filter']);
    });

    $router->group(['prefix' => '/staff', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('staffuser', function ($id) {
            return app('App\user')->find($id);
        });
        $router->get('/index', ['as' => 'staff.index', 'uses' => 'StaffController@index']);
        $router->get('/create', ['as' => 'staff.create', 'uses' => 'StaffController@create']);
        $router->post('/store', ['as' => 'staff.store', 'uses' => 'StaffController@store']);
        $router->get('/edit/{staffuser}', ['as' => 'staff.edit', 'uses' => 'StaffController@edit']);
        $router->put('/update/{staffuser}', ['as' => 'staff.update', 'uses' => 'StaffController@update']);
        $router->get('/delete/{staffuser}', ['as' => 'staff.delete', 'uses' => 'StaffController@destroy']);
    });


    $router->group(['prefix' => '/staffattendance', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('staffattendance', function ($id) {
            return app('App\Entities\attendence')->find($id);
        });
        $router->get('/index', ['as' => 'staffattendance.index', 'uses' => 'StaffAttedanceController@index']);
        $router->post('/attdupdate', ['as' => 'staffattendance.attdupdate', 'uses' => 'StaffAttedanceController@attdupdate']);
        $router->get('/edit/{staffattendance}', ['as' => 'staffattendance.edit', 'uses' => 'StaffAttedanceController@edit']);
        $router->put('/update/{staffattendance}', ['as' => 'staffattendance.update', 'uses' => 'StaffAttedanceController@update']);
        $router->get('/delete/{staffattendance}', ['as' => 'staffattendance.delete', 'uses' => 'StaffAttedanceController@destroy']);
    });


    $router->group(['prefix' =>'/topcategory','middleware'=>'auth'], function (Router $router) {
        $router->bind('staff', function ($id) {

            return app('App\Entities\topcategory')->find($id);
        });
        $router->get('/index', ['as' => 'topcategory.index','uses' => 'TopcategoryController@index']);
        $router->get('/create', ['as' => 'topcategory.create','uses' => 'TopcategoryController@create']);
        $router->post('/store', ['as' => 'topcategory.store','uses' => 'TopcategoryController@store']);
        $router->get('/edit/{topcategory}', ['as' => 'topcategory.edit','uses' => 'TopcategoryController@edit']);
        $router->put('/update/{topcategory}', ['as' => 'topcategory.update','uses' => 'TopcategoryController@update']);
        $router->get('/delete/{topcategory}', ['as' => 'topcategory.delete','uses' => 'TopcategoryController@destroy']);
    });

    $router->group(['prefix' =>'/entrylogs','middleware'=>'auth'], function (Router $router) {
        $router->bind('entrylogs', function ($id) {
            return app('App\Entities\entrylogs')->find($id);
        });
        $router->get('/index', ['as' => 'entrylogs.index','uses' => 'EntrylogsController@index']);
        $router->get('/create', ['as' => 'entrylogs.create','uses' => 'EntrylogsController@create']);
        $router->post('/store', ['as' => 'entrylogs.store','uses' => 'EntrylogsController@store']);
        $router->get('/edit/{entrylogs}', ['as' => 'entrylogs.edit','uses' => 'EntrylogsController@edit']);
        $router->put('/update/{entrylogs}', ['as' => 'entrylogs.update','uses' => 'EntrylogsController@update']);
        $router->get('/delete/{entrylogs}', ['as' => 'entrylogs.delete','uses' => 'EntrylogsController@destroy']);
    });

    $router->group(['prefix' =>'/vehicalreports','middleware'=>'auth'], function (Router $router) {
        $router->bind('vehicalreports', function ($id) {
            return app('App\Entities\vehicalreports')->find($id);
        });
        $router->get('/index', ['as' => 'vehicalreports.index','uses' => 'VehicalreportsController@index']);
        $router->get('/create', ['as' => 'vehicalreports.create','uses' => 'VehicalreportsController@create']);
        $router->post('/store', ['as' => 'vehicalreports.store','uses' => 'VehicalreportsController@store']);
        $router->get('/edit/{vehicalreports}', ['as' => 'vehicalreports.edit','uses' => 'VehicalreportsController@edit']);
        $router->put('/update/{vehicalreports}', ['as' => 'vehicalreports.update','uses' => 'VehicalreportsController@update']);
        $router->get('/delete/{vehicalreports}', ['as' => 'vehicalreports.delete','uses' => 'VehicalreportsController@destroy']);
    });


    $router->group(['prefix' =>'/branch','middleware'=>'auth'], function (Router $router) {
        $router->bind('staff', function ($id) {
            return app('App\Entities\branch')->find($id);
        });
        $router->get('/index', ['as' => 'branch.index','uses' => 'BranchController@index']);
        $router->get('/create', ['as' => 'branch.create','uses' => 'BranchController@create']);
        $router->post('/store', ['as' => 'branch.store','uses' => 'BranchController@store']);
        $router->get('/edit/{branch}', ['as' => 'branch.edit','uses' => 'BranchController@edit']);
        $router->put('/update/{branch}', ['as' => 'branch.update','uses' => 'BranchController@update']);
        $router->get('/delete/{branch}', ['as' => 'branch.delete','uses' => 'BranchController@destroy']);
    });

    $router->group(['prefix' => '/payslip', 'middleware' => 'auth'], function (Router $router) {
        $router->bind('payslip', function ($id) {
            return app('App\Entities\payslip')->find($id);
        });
        $router->get('/index', ['as' => 'payslip.index', 'uses' => 'PayslipController@index']);
        $router->get('/create', ['as' => 'payslip.create', 'uses' => 'PayslipController@create']);
        $router->post('/store', ['as' => 'payslip.store', 'uses' => 'PayslipController@store']);
        $router->post('/rstore', ['as' => 'payslip.rstore', 'uses' => 'PayslipController@regeneratestore']);
        $router->get('/edit/{payslip}', ['as' => 'payslip.edit', 'uses' => 'PayslipController@edit']);
        $router->put('/update/{payslip}', ['as' => 'payslip.update', 'uses' => 'PayslipController@update']);
        $router->get('/delete/{payslip}', ['as' => 'payslip.delete', 'uses' => 'PayslipController@destroy']);
        $router->get('/download/{id}', ['as' => 'payslip.download', 'uses' => 'PayslipController@download']);
    });

    $router->group(['prefix' =>'/sales','middleware'=>'auth'], function (Router $router) {
        $router->bind('sales', function ($id) {
            return app('App\Entities\sales')->find($id);
        });
        $router->get('/index', ['as' => 'sales.index','uses' => 'SaleController@index']);
        $router->get('/create', ['as' => 'sales.create','uses' => 'SaleController@create']);
        $router->post('/store', ['as' => 'sales.store','uses' => 'SaleController@store']);
        $router->get('/getprice', ['as' => 'sales.getprice','uses' => 'SaleController@GetPrice']);
        $router->get('/edit/{sales}', ['as' => 'sales.edit','uses' => 'SaleController@edit']);
        $router->put('/update/{sales}', ['as' => 'sales.update','uses' => 'SaleController@update']);
        $router->get('/delete/{sales}', ['as' => 'sales.delete','uses' => 'SaleController@destroy']);
        $router->get('/addpayment', ['as' => 'sales.addpayment','uses' => 'SaleController@addpayment']);
        $router->get('/viewpayment', ['as' => 'sales.viewpayment','uses' => 'SaleController@viewpayment']);
        $router->post('/paymentstore', ['as' => 'sales.paymentstore','uses' => 'SaleController@paymentstore']);
        $router->get('/filter', ['as' => 'sales.filter', 'uses' => 'SaleController@Filter']);
    });

    $router->group(['prefix' =>'/resale','middleware'=>'auth'], function (Router $router) {
        $router->bind('resale', function ($id) {
            return app('App\Entities\resale')->find($id);
        });
        $router->get('/index', ['as' => 'resale.index','uses' => 'ResaleController@index']);
        $router->get('/create', ['as' => 'resale.create','uses' => 'ResaleController@create']);
        $router->post('/store', ['as' => 'resale.store','uses' => 'ResaleController@store']);
        $router->get('/rgetprice', ['as' => 'resale.getprice','uses' => 'ResaleController@GetPrice']);
        $router->get('/edit/{sales}', ['as' => 'resale.edit','uses' => 'ResaleController@edit']);
        $router->put('/update/{resale}', ['as' => 'resale.update','uses' => 'ResaleController@update']);
        $router->get('/delete/{id}', ['as' => 'resale.delete','uses' => 'ResaleController@destroy']);
        $router->get('/rfilter', ['as' => 'resale.filter', 'uses' => 'ResaleController@ResaleFilter']);
    });

    $router->group(['prefix' =>'/complaint','middleware'=>'auth'], function (Router $router) {
        $router->bind('complaint', function ($id) {
            return app('App\Entities\complaint')->find($id);
        });
        $router->get('/index', ['as' => 'complaint.index','uses' => 'ComplaintController@index']);
        $router->get('/create', ['as' => 'complaint.create','uses' => 'ComplaintController@create']);
        $router->post('/store', ['as' => 'complaint.store','uses' => 'ComplaintController@store']);
        $router->get('/edit/{complaint}', ['as' => 'complaint.edit','uses' => 'ComplaintController@edit']);
        $router->put('/update/{complaint}', ['as' => 'complaint.update','uses' => 'ComplaintController@update']);
        $router->get('/delete/{complaint}', ['as' => 'complaint.delete','uses' => 'ComplaintController@destroy']);
    });


    $router->group(['prefix' =>'/transport','middleware'=>'auth'], function (Router $router) {
        $router->bind('transport', function ($id) {
            return app('App\Entities\transport')->find($id);
        });
        $router->get('/index', ['as' => 'transport.index','uses' => 'TransportController@index']);
        $router->get('/create', ['as' => 'transport.create','uses' => 'TransportController@create']);
        $router->post('/store', ['as' => 'transport.store','uses' => 'TransportController@store']);
        $router->get('/edit/{transport}', ['as' => 'transport.edit','uses' => 'TransportController@edit']);
        $router->put('/update/{transport}', ['as' => 'transport.update','uses' => 'TransportController@update']);
        $router->get('/delete/{transport}', ['as' => 'transport.delete','uses' => 'TransportController@destroy']);
    });

    $router->group(['prefix' =>'/location','middleware'=>'auth'], function (Router $router) {
        $router->bind('location', function ($id) {
            return app('App\Entities\location')->find($id);
        });
        $router->get('/index', ['as' => 'location.index','uses' => 'LocationController@index']);
        $router->get('/create', ['as' => 'location.create','uses' => 'LocationController@create']);
        $router->post('/store', ['as' => 'location.store','uses' => 'LocationController@store']);
        $router->get('/edit/{location}', ['as' => 'location.edit','uses' => 'LocationController@edit']);
        $router->put('/update/{location}', ['as' => 'location.update','uses' => 'LocationController@update']);
        $router->get('/delete/{location}', ['as' => 'location.delete','uses' => 'LocationController@destroy']);
    });

//Credit Start//
    $router->group(['prefix' =>'/credit','middleware'=>'auth'], function (Router $router) {
        $router->bind('credit', function ($id) {
            return app('App\Entities\credit')->find($id);
        });
        $router->get('/index', ['as' => 'credit.index','uses' => 'CreditController@index']);
        $router->get('/create', ['as' => 'credit.create','uses' => 'CreditController@create']);
        $router->post('/store', ['as' => 'credit.store','uses' => 'CreditController@store']);
        $router->get('/edit/{credit}', ['as' => 'credit.edit','uses' => 'CreditController@edit']);
        $router->put('/update/{credit}', ['as' => 'credit.update','uses' => 'CreditController@update']);
        $router->get('/delete/{credit}', ['as' => 'credit.delete','uses' => 'CreditController@destroy']);
    });
});


