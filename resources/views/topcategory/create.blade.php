@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Top Category</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('topcategory.index')}}">Top Category</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('topcategory.create')}}">Add TopCategory</a></li>
                </ol>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            {!! Form::open(array('route' => 'topcategory.store','method'=>'POST')) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('topcategory_name') ? ' has-error' : '' }}">
                                    {!! Form::label('topcategory_name', trans('Top Category')) !!}
                                    {!! Form::text('topcategory_name', old('topcategory_name'), ['class' => 'form-control', 'placeholder' => trans('Top Category')]) !!}
                                    {!! $errors->first('topcategory_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-md-2">
                                 <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('status')) !!}<br>
                           <label class="radio-inline"> {!! Form::radio('status', '1', true ) !!} Yes </label> <label class="radio-inline"> {!! Form::radio('status', '0' ) !!} No </label>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>
                            <a href="{{route('topcategory.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                        </div>   
                    </div>
                </div> 
            </div> 

            {!! Form::close() !!}
        </div>
    </div><!-- end row-->
</div> <!-- container-fluid -->
@endsection

@section('js_after')
<script>
  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

  });
});

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
        }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
});
</script>

@endsection