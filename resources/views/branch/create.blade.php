@extends('layouts.master')

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Branch</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                       <li class="breadcrumb-item active">Add Branch</li>
                                    </ol>


                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                {!! Form::open(array('route' => 'branch.store','method'=>'POST','files' => true)) !!}

                                <div class="card m-b-20">
                                    <div class="card-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        {!! Form::label('name', 'Name') !!}
                                        {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        {!! Form::label('email', 'Email') !!}
                                        {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('mobilenumber') ? ' has-error' : '' }}">
                                        {!! Form::label('mobilenumber', 'Mobile Number') !!}
                                        {!! Form::number('mobilenumber', old('mobilenumber'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('mobilenumber', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                

                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                                        {!! Form::label('pincode', 'Pincode') !!}
                                        {!! Form::number('pincode', old('pincode'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('pincode', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                            {!! Form::label('city', trans('Select City')) !!}
                                            <select id="city" class="form-control" name="city">
                                                <option value="">Select City</option>
                                                <option value="Bangalore">Bangalore</option>
                                                <option value="Mysore">Mysore</option>
                                                <option value="Maddur">Maddur</option>
                                                <option value="Tumkur">Tumkur</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                <div class="col-sm-3">
                                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                            {!! Form::label('state', trans('Select State')) !!}
                                            <select id="state" class="form-control" name="state">
                                                <option value="">Select State</option>
                                                <option value="Karnataka">Karnataka</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                        {!! Form::label('country', 'Country') !!}
                                        <select id="country" class="form-control" name="country">
                                                <option value="">Select Country</option>
                                                <option value="India">India</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('ime') ? ' has-error' : '' }}">
                                        {!! Form::label('ime', 'ime') !!}
                                        {!! Form::text('ime', old('ime'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('ime', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('branch_code') ? ' has-error' : '' }}">
                                        {!! Form::label('branch_code', 'Branch Code') !!}
                                        {!! Form::text('branch_code', old('branch_code'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('branch_code', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        {!! Form::label('address', 'Staff Address') !!}
                                        {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            <div class="col-sm-3">
                                   <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                       {!! Form::label('image', trans('Image ')) !!}
                                       {!! Form::file('images', ['class' => 'form-control','onchange'=>'readURL2(this)','placeholder' => trans('Image')]) !!}
                                       {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                                   </div>
                                   </div>

                            <div class="col-md-3">
                                     <div class="form-group{{ $errors->has('lat') ? ' has-error' : '' }}">
                                        {!! Form::label('lat', 'Lat') !!}
                                        {!! Form::text('lat', old('lat'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('lat', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            <div class="col-md-3">
                                     <div class="form-group{{ $errors->has('lng') ? ' has-error' : '' }}">
                                        {!! Form::label('lng', 'Lng') !!}
                                        {!! Form::text('lng', old('lng'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('lng', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>

                                          <div class="">
                                        <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                          <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
                          </div>






                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

         {!! Form::close() !!}

                    </div> <!-- container-fluid -->
@endsection
