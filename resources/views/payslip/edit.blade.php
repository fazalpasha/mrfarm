@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Category</h4>
                <ol class="breadcrumb">
                   <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                   <li class="breadcrumb-item"><a href="{{route('category.index')}}">Category</a></li>
                   <li class="breadcrumb-item active">Edit Category</li>
               </ol>
           </div>
       </div>
   </div>

    <div class="row">
        <div class="col-12">
            {!! Form::open(['route' => ['category.update', $category->id], 'method' => 'put']) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::Label('Type', 'Type:') !!}
                                    <select class="form-control" name="type">
                                        @foreach($topcategory as $key => $categorys)
                                        <option value="{{$categorys->id}}">{{$categorys->topcategory_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('category_name') ? ' has-error' : '' }}">
                                    {!! Form::label('category_name', trans('Category Name')) !!}
                                    {!! Form::text('category_name', old('category_name',$category->category_name), ['class' => 'form-control', 'placeholder' => trans('Category Name')]) !!}
                                    {!! $errors->first('category_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('Status')) !!}<br>
                                    <input name="status" type="checkbox" id="status" switch="bool">
                                    <label for="status" data-on-label="Yes"
                                    data-off-label="No"></label>
                                    {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>
                            <a href="{{route('category.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                        </div>   
                    </div>
                </div> 
            </div>
            {!! Form::close() !!}
        </div>
    </div><!-- end row-->
</div> <!-- container-fluid -->
@endsection
@section('js_after')
<script>
  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

  });
});

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
        }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
});
</script>

@endsection