@extends('layouts.master')
@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Payslip</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('payslip.index')}}">Payslip</a></li>
                </ol>
                <div class="state-information d-none d-sm-block">
                   <div class="btn-group pull-right">
                        <a href="{{ route('payslip.create') }}" class="btn btn-primary btn-lg waves-effect waves-light float-left" >
                        Create
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Payslip</h4>
                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>User Name</th>
                                <th>Basic</th>
                                <th>HRA</th>
                                <th>TA</th>
                                <th>DA</th>
                                <th>LTC</th>
                                <th>SA</th>
                                <th>MA</th>
                                <th>PD</th>
                                <th>Status</th>
                                <th>Action</th>
                              <!--   <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($data)): ?>
                                <?php foreach ($data as $payslip): ?>
                                    <tr>
                                        <td>{{$payslip->id}}</td>
                                        <td>{{$payslip->username}}</td>
                                        <td>{{$payslip->basic}}</td>
                                        <td>{{$payslip->HRA}}</td>
                                        <td>{{$payslip->TA}}</td>
                                        <td>{{$payslip->DA}}</td>
                                        <td>{{$payslip->LTC}}</td>
                                         <td>{{$payslip->special_allowance}}</td>
                                          <td>{{$payslip->medical_allowance}}</td>
                                          <td>{{$payslip->proffesional_development}}</td>
                                          <td>{{$payslip->status}}</td>
                                         <td><div class="btn-group">
                                            <button href="#" data-toggle="modal" data-target="#view_{{$payslip->id}}" class="btn btn-primary"><i class="glyphicon glyphicon-eye-open"></i>Regenerate</button>
												<div class="modal fade" id="view_{{$payslip->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        	<h3>Regenerate</h3>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            
                                                        </div>
                                                        <div class="modal-body">
                                                        	{!! Form::open(array('route' => 'payslip.rstore','method'=>'POST')) !!}
     														 <div class="modal-body">
        														<input type="hidden" name="user_id" value="{{$payslip->user_id}}">
        														<input type="hidden" name="basic" value="{{$payslip->basic}}">
														        <input type="hidden" name="HRA" value="{{$payslip->HRA}}">
        														<input type="hidden" name="TA" value="{{$payslip->TA}}">
														        <input type="hidden" name="DA" value="{{$payslip->DA}}">
     														    <input type="hidden" name="LTC" value="{{$payslip->LTC}}">
        														<input type="hidden" name="special_allowance" value="{{$payslip->special_allowance}}">
        														<input type="hidden" name="medical_allowance" value="{{$payslip->medical_allowance}}">
        														<input type="hidden" name="proffesional_development" value="{{$payslip->proffesional_development}}">
        														<input type="hidden" name="D_PF" value="{{$payslip->D_PF}}">
        														<input type="hidden" name="D_Proff_tax" value="{{$payslip->D_Proff_tax}}">
        														<input type="hidden" name="status" value="{{$payslip->status}}">
        														<input type="hidden" name="ref_date" value="{{$payslip->ref_date}}">
      			  <div class="row">
                   <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            {!! Form::label('remarks', trans('Month')) !!}
                            <select required="" class="form-control" name="slip_month" >
                                <option >Select Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                           
                            {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                   <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            {!! Form::label('remarks', trans('Year')) !!}
                            <select required="" class="form-control" name="year" >
                                <option>- Year -</option>
<option value="2025">2025</option>
<option value="2024">2024</option>
<option value="2023">2023</option>
<option value="2022">2022</option>
<option value="2021">2021</option>
<option value="2020">2020</option>
<option value="2019">2019</option>
<option value="2018">2018</option>
<option value="2017">2017</option>
<option value="2016">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
<option value="2013">2013</option>
<option value="2012">2012</option>
<option value="2011">2011</option>
<option value="2010">2010</option>
<option value="2009">2009</option>
<option value="2008">2008</option>
<option value="2007">2007</option>
<option value="2006">2006</option>
<option value="2005">2005</option>
<option value="2004">2004</option>
<option value="2003">2003</option>
<option value="2002">2002</option>
<option value="2001">2001</option>
<option value="2000">2000</option>
</select>
                        </div>
                    </div>
                </div>
      </div>
                                                            <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button></div>
     {!! Form::close() !!}
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                       <a class="btn btn-danger" target="download" type="button" href="{{ route('payslip.download', [$payslip->id]) }}">
                                            <i class="icon-download-alt"> </i> Download Pdf</a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <!-- Modal -->
				</div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
</div> <!-- container-fluid -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{ URL::asset('assets/pages/datatables.init.js')}}"></script>
@endsection