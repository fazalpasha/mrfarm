@extends('layouts.master')
@section('content')
<style type="text/css">
    td{
        padding: 10px 6px;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Payslip</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Payslip</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            {!! Form::open(array('route' => 'payslip.store','method'=>'POST')) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                   <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            {!! Form::label('remarks', trans('Month')) !!}
                            <select required="" class="form-control" name="slip_month" >
                                <option >Select Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                           
                            {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            {!! Form::label('remarks', trans('Date')) !!}
                            {!! Form::date('ref_date', old('ref_date',date('Y-m-d')), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Date')]) !!}
                            {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            {!! Form::label('remarks', trans('Year')) !!}
                            <select required="" class="form-control" name="year" >
                                <option>- Year -</option>
<option value="2025">2025</option>
<option value="2024">2024</option>
<option value="2023">2023</option>
<option value="2022">2022</option>
<option value="2021">2021</option>
<option value="2020">2020</option>
<option value="2019">2019</option>
<option value="2018">2018</option>
<option value="2017">2017</option>
<option value="2016">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
<option value="2013">2013</option>
<option value="2012">2012</option>
<option value="2011">2011</option>
<option value="2010">2010</option>
<option value="2009">2009</option>
<option value="2008">2008</option>
<option value="2007">2007</option>
<option value="2006">2006</option>
<option value="2005">2005</option>
<option value="2004">2004</option>
<option value="2003">2003</option>
<option value="2002">2002</option>
<option value="2001">2001</option>
<option value="2000">2000</option>
</select>
                        </div>
                    </div>
                </div>
             

                   <hr>

                   <div class="row">
                    <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                                {!! Form::label('remarks', trans('Staff Name')) !!}
                                <select class="form-control" id="user_id">
                                   <option value="">Select Emplyoee ID</option>
                                   @foreach($staff as $key => $emplyoee)
                                   <option value="{{$emplyoee->id}}-{{$emplyoee->name}}">{{$emplyoee->username}}</option>
                                   @endforeach
                               </select>
                               {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                           </div>
                       </div>
                       <div class="col-sm-2">
                           <input type="button" class="btn btn-primary btn-lg  text-white add-row" value="Add Row" class="form-control" style="margin-top: 22px;">
                       </div>
                        <div class="col-sm-4">
                            <table id="datatable " class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;margin-top: 18px;" >
                                <thead>
                                    <tr>
                                        <th>Select</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                                <tbody class="tablebody">
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary btn-lg  text-white delete-row" style="margin-top: 20px !important;">Delete Row</button >
                        </div>
                    </div>
                    <hr>

                

            <div class="row">
                <div class="col-sm-6">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>Earnings</th>
                                <th>INR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>    
                                <td>Basic</td>
                                <td>
                                    <div class="form-group{{ $errors->has('basic') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;">
                                        {!! Form::text('basic', old('basic'), ['class' => 'form-control earning','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");', 'placeholder' => trans('Basic'),'required' => 'required']) !!}
                                        {!! $errors->first('basic', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>
                            </tr>
                            <tr>    
                                <td>HRA</td>
                                <td>
                                    <div class="form-group{{ $errors->has('HRA') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;">
                                        {!! Form::text('HRA', old('qrcoHRAde'), ['class' => 'form-control earning','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('HRA'),'required' => 'required']) !!}
                                        {!! $errors->first('HRA', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td> 
                            </tr>
                            <tr>    
                                <td>TA</td>
                                <td>
                                    <div class="form-group{{ $errors->has('TA') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;">
                                        {!! Form::text('TA', old('TA'), ['class' => 'form-control earning', 'oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('TA'),'required' => 'required']) !!}
                                        {!! $errors->first('TA', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>
                            </tr>
                            <tr>    
                                <td>DA</td>
                                <td>
                                    <div class="form-group{{ $errors->has('DA') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;"> 
                                        {!! Form::text('DA', old('DA'), ['class' => 'form-control earning', 'oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('DA'),'required' => 'required']) !!}
                                        {!! $errors->first('DA', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>  
                            </tr>
                            <tr>    
                                <td>LTC</td>
                                <td>
                                    <div class="form-group{{ $errors->has('LTC') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;"> 
                                        {!! Form::text('LTC', old('LTC'), ['class' => 'form-control earning', 'oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('LTC'),'required' => 'required']) !!}
                                        {!! $errors->first('LTC', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>  
                            </tr>
                            <tr>    
                                <td>Special_allowance</td>
                                <td>
                                    <div class="form-group{{ $errors->has('special_allowance') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;"> 
                                        {!! Form::text('special_allowance', old('special_allowance'), ['class' => 'form-control earning','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('Special Allowance'),'required' => 'required']) !!}
                                        {!! $errors->first('special_allowance', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>  
                            </tr>
                            <tr>    
                                <td>Medica Allowance</td>
                                <td>
                                    <div class="form-group{{ $errors->has('medical_allowance') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;"> 
                                        {!! Form::text('medical_allowance', old('medical_allowance'), ['class' => 'form-control earning','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");', 'placeholder' => trans('Medical Allowance'),'required' => 'required']) !!}
                                        {!! $errors->first('medical_allowance', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>  
                            </tr>
                            <tr>    
                                <td>Professional Development</td>
                                <td>
                                    <div class="form-group{{ $errors->has('proffesional_development') ? ' has-error' : '' }}" style="margin-bottom: 0rem !important;"> 
                                        {!! Form::text('proffesional_development', old('proffesional_development'), ['class' => 'form-control earning','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('Professional Development'),'required' => 'required']) !!}
                                        {!! $errors->first('proffesional_development', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>  
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-sm-6">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>Deduction</th>
                                <th>INR</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>    
                                <td>PF</td>
                                <td>
                                    <div class="form-group{{ $errors->has('D_PF') ? ' has-error' : '' }}"> 
                                        {!! Form::text('D_PF', old('D_PF'), ['class' => 'form-control deduction','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('PF'),'required' => 'required']) !!}
                                        {!! $errors->first('D_PF', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>
                            </tr>
                            <tr>    
                                <td>PROP TAX</td>
                                <td>
                                    <div class="form-group{{ $errors->has('D_Proff_tax') ? ' has-error' : '' }}">
                                        {!! Form::text('D_Proff_tax', old('D_Proff_tax'), ['class' => 'form-control deduction','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('PROP_TAX'),'required' => 'required']) !!}
                                        {!! $errors->first('D_Proff_tax', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>

                            </tr>
                            <tr>    
                                <td>Status</td>
                                <td>
                                    <div class="form-group{{ $errors->has('Status') ? ' has-error' : '' }}">
                                <select required="" class="form-control" name="status" >
                                <option >Select</option>
                                <option value="Paid">Paid</option>
                                <option value="Unpaid">Unpaid</option>
                            </select>
                           
                            {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                    <tr>
                        <th> Earnings</th>
                        <th>Dedeciton</th>
                        <th>Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>    
                        <td id="earningresult"></td>
                        <td id="deductionresult"></td>
                        <td id="totalsalary"></td>

                    </tr>
                </tbody>
            </table>

            <hr>
            <div class="">
                <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                <a href="{{route('payslip.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
            </div>   
        </div>
    </div> 
</div> 
{!! Form::close() !!}
</div>
</div><!--end row-->
</div> <!-- container-fluid -->
@endsection
@section('js_after')
<style>
    form{
        margin: 20px 0;
    }
    form input, button{
        padding: 5px;
    }
    table{
        width: 100%;
        margin-bottom: 20px;
        border-collapse: collapse;
    }
    table, th, td{
        border: 1px solid #cdcdcd;
    }
    table th, table td{
        padding: 10px;
        text-align: left;
    }
</style>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    $(document).ready(function(){
        $(".add-row").click(function() {
            var row = $("table .tablebody tr").length;
            var userstr = $("#user_id").val();
            var user_id = userstr.split("-");

          
            $("table .tablebody").append("<tr><td><input type='checkbox' name='record'></td><td><input type='hidden' name='group["+ row +"][userid]' value='"+user_id[0] +"'> " + user_id[0]+'-'+user_id[1] + "</td></tr>");

        });

        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table .tablebody").find('input[name="record"]').each(function(){
                if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });

        $('.form-group').on('input','.earning',function(){
            var totalSum1=0;
            $('.form-group .earning').each(function(){
                var inputVal1=$(this).val();
                if($.isNumeric(inputVal1)){
                    totalSum1 +=parseFloat(inputVal1);
                }

            });
            $('#earningresult').text(totalSum1);
             $('#totalsalary').text(totalSum1);

        });
        $('.form-group').on('input','.deduction',function(){
            var totalSum=0;
            var val = $('#earningresult').text();
            $('.form-group .deduction').each(function(){
                var inputVal=$(this).val();
                if($.isNumeric(inputVal)){
                    totalSum +=parseFloat(inputVal);
                }

            });
            $('#deductionresult').text(totalSum);
             deductionval = parseFloat(val) - parseFloat(totalSum);
             $('#totalsalary').text(deductionval);

        });
    });    
</script>

@endsection
