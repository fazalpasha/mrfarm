@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Batch</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('batch.index')}}">Batch</a></li>
                        <li class="breadcrumb-item active">Add</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                {!! Form::open(array('route' => 'batch.store','method'=>'POST','files' =>'true')) !!}

                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('batch_name') ? ' has-error' : '' }}">
                                        {!! Form::label('batch_name', 'Batch Name') !!}<br>
                                        <input  name="batch_name" type="text" id="batch_name" class="form-control">
                                        {!! $errors->first('batch_name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('created_date') ? ' has-error' : '' }}" >
                                        {!! Form::label('created_date', 'Created Date') !!}<br>
                                        <input  name="created_date" type="date" id="date" class="form-control">
                                        {!! $errors->first('created_date', '<span class="help-block">:message</span>') !!}
                                </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('created_by') ? ' has-error' : '' }}">
                                        {!! Form::label('created_by', 'Created By') !!}<br>
                                        <input  name="created_by" type="text" id="created_by" class="form-control">
                                        {!! $errors->first('batch_name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                <div class="form-group">
                                    {!! Form::Label('Category Name','Category Name:') !!}
                                    <select required class="form-control" id="topcat" name="category_id">
                                        <option value="">Select Category</option>
                                        @foreach($category as $key => $categorys)
                                        <option <?php if(isset($request->category_id))
                                        {
                                         ?> <?php if($request->category_id == $categorys->id) {?> selected <?php }?> <?php }?>value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>

             <div class="col-sm-4">
              <div class="form-group {{ $errors->has('subcategory_id') ? ' has-error' : '' }}" >
                {!! Form::Label('Sub Category Name','Sub Category Name:') !!}
                <select required class="form-control" id="category" name="subcategory_id">
                </select>
                {!! $errors->first('subcategory_id', '<span class="help-block">:message</span>') !!}
              </div>
            </div>
                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('total_animals') ? ' has-error' : '' }}">
                                        {!! Form::label('total_animal', 'Total Animal') !!}<br>
                                        <input  name="total_animals" type="text" id="tot" class="form-control">
                                        {!! $errors->first('total_animals', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('purchased_amount') ? ' has-error' : '' }}">
                                        {!! Form::label('purchased_amount', 'Purchased Amount') !!}<br>
                                        <input  name="purchased_amount" type="text" id="purchased_amount" class="form-control">
                                        {!! $errors->first('purchased_amount', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('total_weight') ? ' has-error' : '' }}" >
                                        {!! Form::label('total_weight', 'Total Weight') !!}<br>
                                        <input  name="total_weight" type="text" id="total_weight" class="form-control">
                                        {!! $errors->first('total_weight', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                            </div>

                            <div class="">
                                <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                                <a type="button" class="btn btn-danger btn-lg float-right text-white " href="{{route('batch.index')}}">Cancel</a>
                            </div>

                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

                {!! Form::close() !!}

            </div> <!-- container-fluid -->
        </div>
    </div>

@endsection
@section('js_after')
<script>
  <?php if(isset($request->category_id)){ ?>
    $( document ).ready(function() {

      var top_cat = "{{$request->category_id}}";
      ajax(top_cat);
    });
  <?php }?>
  $('#topcat').on('change',function(){
    var top_cat = $(this).val();
    ajax(top_cat);

  });
  function ajax(top_cat) {

    url = '<?= route('animalstock.subcat') ?>';
    $.get(url, {"top_cat": top_cat}, categorylist, 'json');

    function categorylist(d, s) {

      var elSel = document.getElementById('category');
      var i;
      for (i = elSel.length; i >= 0; i--) {
        elSel.remove(i);
      }
      if (d.length > 0) {
        $("#category").append('<option value="">Select Sub Category</option>');
      } else {
        $("#category").append('<option value="">No Sub Category</option>');
      }

      $.each(d,
        function () {

          var str = this.id;
          var name = this.subcategory_name;

          var option = new Option(str, name);
                        // var dropdownList = $("#ValidCityName")[0];
                        var dropdownList = document.getElementById("category");
                        //dropdownList.add(option, null);
                        var option = document.createElement("option");

                        if (typeof name == 'undefined') {
                          name = '';
                        }
                        option.appendChild(document.createTextNode(name));
                        // or alternately
                        //option.text = data[i].name;
                        option.value = str;


                        dropdownList.appendChild(option);

                        if (name == 'error') {

                        } else {


                        }
                      }
                      );


    }
  }


  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

    });
  });

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
          }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
  });
</script>
@endsection
