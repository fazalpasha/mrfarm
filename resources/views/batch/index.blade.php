@extends('layouts.master')

@section('css')
        <!-- DataTables -->
        <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Batch</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('batch.index')}}">Batch</a></li>


                                    </ol>
                                <div class="state-information d-none d-sm-block">
                     <div class="btn-group pull-right">
                    <a href="{{ route('batch.create') }}" class="btn btn-primary btn-lg waves-effect waves-light float-left" >
                        Create
                    </a>
                </div>
               </div>

                                </div>
                            </div>
                        </div><br>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-20">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Batch</h4>


                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Batch Name</th>
                                                <th>Created By</th>
                                                <th>Category Name</th>
                                                <th>Subcategory Name</th>
                                                <th>Total Animals</th>
                                                <th>Action</th>


                                            </tr>
                                            </thead>


                                            <tbody>
                                            <?php if (isset($data)): ?>
                <?php foreach ($data as $batch): ?>
                <tr>
                    <td>{{$batch->id}}</td>
                    <td>{{$batch->batch_name}}</td>
                    <td>{{$batch->created_by}}</td>
                    <td>{{$batch->cname}}</td>
                    <td>{{$batch->sname}}</td>
                    <td>{{$batch->total_animals}}</td>
                    <td> <div class="btn-group">
                                        <a href="{{ route('batch.edit', [$batch->id]) }}" class="btn btn-primary btn-flat"><i class="ion-edit"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-message="Are you Sure You Want to Delete This Batch Record ?" data-target="#modal-delete-confirmation" data-action-target="{{ route('batch.delete', [$batch->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                          </td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                        <!-- end row -->



                    </div> <!-- container-fluid -->
@endsection

@section('script')
        <!-- Required datatable js -->
        <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

        <!-- Datatable init js -->
        <script src="{{ URL::asset('assets/pages/datatables.init.js')}}"></script>
@endsection
