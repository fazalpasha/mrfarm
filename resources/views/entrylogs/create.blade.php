@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Entry Logs</h4>
                <ol class="breadcrumb">
                   <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                   <li class="breadcrumb-item active"><a href="{{route('entrylogs.index')}}">Entry Logs</a></li>
                   <li class="breadcrumb-item active">Add Entry Logs</a></li>
               </ol>
           </div>
       </div>
   </div>

   <div class="row">
        <div class="col-12">
            {!! Form::open(array('route' => 'entrylogs.store','method'=>'POST')) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">

                             <div class="col-sm-4">
                            <div class="form-group">
                              {!! Form::label('Type', 'Visit Type') !!}<br>
                              <select required class="form-control" name="type">
                           <option value="Person">Meeting</option>
                           <option value="Transport">Transports</option>
                           <option value="Transport">Other</option>
                          
                           
                         </select>
                            </div>
                          </div>

                        <div class="col-sm-4">
                        <div class="form-group">
                        {!! Form::label('Vehicle', 'Vehicle') !!}<br>
                        <select required class="form-control" name="vehicle">
                        <option value="">Select Type</option>
                        <option value="">Other Vehicle</option>
                        <?php if(isset($vehical)){?>
                                                <?php foreach ($vehical as $vehicals){ ?>
                                                    <option value="{{$vehicals->vehical_type}}">{{$vehicals->vehical_type}}</option>
                                                <?php }?>
                                            <?php }?>
                        </select>
                        </div>
                        </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    {!! Form::label('first_name', trans('First Name')) !!}
                                    {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => trans('First Name')]) !!}
                                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    {!! Form::label('last_name', trans('Last Name')) !!}
                                    {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => trans('Last Name')]) !!}
                                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    {!! Form::label('phone', trans('Phone')) !!}
                                    {!! Form::text('phone', old('phone'), ['class' => 'form-control','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','maxlength'=> 10, 'placeholder' => trans('phone number')]) !!}
                                    {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                             <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('vehicle_no') ? ' has-error' : '' }}">
                                    {!! Form::label('vehicle_no', trans('Vehicle Number')) !!}
                                    {!! Form::text('vehicle_no', old('vehicle_no'), ['class' => 'form-control','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','maxlength'=> 10, 'placeholder' => trans('vehicle number')]) !!}
                                    {!! $errors->first('vehicle_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                                    {!! Form::label('reason', trans('Reason')) !!}
                                    {!! Form::textarea('reason', old('reason'), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Reason')]) !!}
                                    {!! $errors->first('reason', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>
                            <a href="{{route('entrylogs.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                        </div>  
                    </div>
                </div>
            </div> 
            {!! Form::close() !!}
        </div>
    </div><!-- end row-->
</div> <!-- container-fluid -->
@endsection