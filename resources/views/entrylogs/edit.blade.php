@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Entry Logs</h4>
                <ol class="breadcrumb">
                   <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                   <li class="breadcrumb-item"><a href="{{route('entrylogs.index')}}">Entry Logs</a></li>
                   <li class="breadcrumb-item active">Edit Entry Logs</li>
               </ol>
           </div>
       </div>
   </div>

    <div class="row">
        <div class="col-12">
            {!! Form::open(['route' => ['entrylogs.update', $entrylogs->id], 'method' => 'put']) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                {!! Form::label('first_name', trans('First Name')) !!}
                                {!! Form::text('first_name', old('first_name',$entrylogs->first_name), ['class' => 'form-control', 'placeholder' => trans('First Name')]) !!}
                                {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    {!! Form::label('last_name', trans('Last Name')) !!}
                                    {!! Form::text('last_name', old('last_name',$entrylogs->last_name), ['class' => 'form-control', 'placeholder' => trans('Last Name')]) !!}
                                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    {!! Form::label('phone', trans('Phone')) !!}
                                    {!! Form::text('phone', old('phone',$entrylogs->phone), ['class' => 'form-control','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','maxlength'=> 10, 'placeholder' => trans('phone number')]) !!}
                                    {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                                    {!! Form::label('reason', trans('Reason')) !!}
                                    {!! Form::textarea('reason', old('reason',$entrylogs->reason), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Reason')]) !!}
                                    {!! $errors->first('reason', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>
                            <a href="{{route('entrylogs.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                        </div>    
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
            {!! Form::close() !!}
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection