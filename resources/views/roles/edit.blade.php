@extends('layouts.master')

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Roles</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">Users</a></li>
                                        <li class="breadcrumb-item active">Role</li>
                                         <li class="breadcrumb-item active">Edit Role</li>
                                    </ol>
            
                                   
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                       
        
                        <div class="row">
                            <div class="col-lg-12">

<!-- {!! Form::open(array('route' => 'roles.store','method'=>'put')) !!} -->
 {!! Form::open(['route' => ['roles.update', $role->id], 'method' => 'put']) !!}
                                <div class="card m-b-20">
                                    <div class="card-body">
        
                                        
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Data</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Permissions</a>
                                            </li>
                                           
                                           
                                        </ul>
        
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active p-3" id="home1" role="tabpanel">
                                                <p>
                                                   <div class="tab-pane active" id="tab_1-1">
                                                   <div class="box-body"> 
                                                   <div class="row">  
                                                <div class="col-md-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    {!! Form::label('name', trans('Role Name')) !!}
                                    {!! Form::text('name', old('name', $role->name), ['class' => 'form-control', 'placeholder' => trans('Role Name')]) !!}
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> 

                             <div class="col-md-4">
                                <div class="form-group{{ $errors->has('slug_name') ? ' has-error' : '' }}">
                                    {!! Form::label('slug_name', trans('Role Slug')) !!}
                                    {!! Form::text('slug_name', old('slug_name', $role->slug_name), ['class' => 'form-control', 'placeholder' => trans('Role Slug')]) !!}
                                    {!! $errors->first('slug_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> 
                        </div>
                    </div>
                </div><br>
                         
                                            

                </p>
                                            </div>
                                        <div class="tab-pane p-3" id="profile1" role="tabpanel">
                                            <div class="form-group">
                                            <div class="row">
                                          
                                                
                                                @foreach($permission as $value)
                                                  <div class="col-md-3">
                                               <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                                               {{ $value->name }}</label>
                                               </div>
                                               @endforeach
                                            
                                            </div>
                                            </div>
                                        </div>

                                         <div class="">
                                        <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>

                          <a href="{{route('roles.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                          </div>   


                                    </div>
        
                                    </div>
                                </div>
                            </div>
        
        {!! Form::close() !!}
        
        
                     
            


                    </div> <!-- container-fluid -->
@endsection