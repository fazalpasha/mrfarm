@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
        <h4 class="page-title">Complaint</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{route('complaint.index')}}">Complaint</a></li>
          <li class="breadcrumb-item active">Add</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- end row -->

  <div class="row">
    <div class="col-12">
      {!! Form::open(array('route' => 'complaint.store','method'=>'POST','files' =>'true')) !!}

      <div class="card m-b-20">
        <div class="card-body">

            

          <div class="box-body">
            <div class="row">
              <div class="col-sm-3">
             <div class="form-group">
                                    {!! Form::Label('Category Name','Category Name:') !!}
                                    <select required class="form-control" id="topcat" name="categoryname">
                                        <option value="">Select Category</option>
                                        @foreach($category as $key => $categorys)
                                        <option <?php if(isset($request->category_id))
                                        {
                                         ?> <?php if($request->category_id == $categorys->id) {?> selected <?php }?> <?php }?>value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                                        @endforeach
                                      </select>
             </div>
         </div>

            <div class="col-sm-3">
              <div class="form-group">
                {!! Form::label('Description', 'Description') !!}<br>
                  <input  name="description" type="textarea" id="desc" class="form-control">
                </div>
            </div>

<div class="col-sm-3">
                                   <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                       {!! Form::label('image', trans('Image ')) !!}
                                       {!! Form::file('images', ['class' => 'form-control','onchange'=>'readURL2(this)','placeholder' => trans('Image')]) !!}
                                       {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                                   </div>
                                   </div>

<div class="col-sm-3">
              <div class="form-group">
                {!! Form::label('Vendor', 'Vendor') !!}<br>
                  <select required class="form-control" name="vendor_id">
               <option value="">Select</option>
               <?php if(isset($vendor)){?>
                                                <?php foreach ($vendor as $vendors){ ?>
                                                    <option value="{{$vendors->id}}">{{$vendors->vendor_name}}</option>
                                                <?php }?>
                                            <?php }?>
             </select>
                </div>
            </div>


<div class="col-sm-12">
  <div class="form-group">
    {!! Form::label('remarks', trans('Remarks')) !!}
    {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control','rows' =>3,'required', 'placeholder' => trans('Remarks')]) !!}
    {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
  </div>
</div>

</div>

<div class="">
  <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

  <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
</div>

</div>
</div> <!-- end col -->
</div> <!-- end row -->

{!! Form::close() !!}

</div> <!-- container-fluid -->
</div>
</div>

@endsection
@section('js_after')
<script>
  <?php if(isset($request->category_id)){ ?>
    $( document ).ready(function() {

      var top_cat = "{{$request->category_id}}";
      ajax(top_cat);
    });
  <?php }?>
  $('#topcat').on('change',function(){
    var top_cat = $(this).val();
    ajax(top_cat);

  });
  function ajax(top_cat) {

    url = '<?= route('animalstock.subcat') ?>';
    $.get(url, {"top_cat": top_cat}, categorylist, 'json');

    function categorylist(d, s) {

      var elSel = document.getElementById('category');
      var i;
      for (i = elSel.length; i >= 0; i--) {
        elSel.remove(i);
      }
      if (d.length > 0) {
        $("#category").append('<option value="">Select Sub Category</option>');
      } else {
        $("#category").append('<option value="">No Sub Category</option>');
      }

      $.each(d,
        function () {

          var str = this.id;
          var name = this.subcategory_name;

          var option = new Option(str, name);
                        // var dropdownList = $("#ValidCityName")[0];
                        var dropdownList = document.getElementById("category");
                        //dropdownList.add(option, null);
                        var option = document.createElement("option");

                        if (typeof name == 'undefined') {
                          name = '';
                        }
                        option.appendChild(document.createTextNode(name));
                        // or alternately
                        //option.text = data[i].name;
                        option.value = str;


                        dropdownList.appendChild(option);

                        if (name == 'error') {

                        } else {


                        }
                      }
                      );


    }
  }


  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

    });
  });

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
          }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
  });
</script>

@endsection
