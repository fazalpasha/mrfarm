@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css')}}">
@endsection

@section('content')
<style>
  .mb-4, .my-4 {
      margin-bottom: -0.5rem !important;
  }
  </style>
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Dashboard</h4>

                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                          <div class="col-md-2 text-center">
                                    <div class="card m-b-20">
                                        <div class="card-body" style="background-color: #20AFD9;">
                                            <h4 class="mt-0 font-30" style="color: white;" >120</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">Sale</h4>
                                          </div>
                                        </div>
                                    </div>

                                     <div class="col-md-2 text-center">
                                    <div class="card m-b-20">
                                        <div class="card-body"style="background-color: #00617D;">
                                            <h4 class="mt-0  font-30" style="color: white;" >180 L</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">Milk</h4>
                                          </div>
                                        </div>
                                    </div>

                                     <div class="col-md-2 text-center">
                                    <div class="card m-b-20">
                                        <div class="card-body" style="background-color: #20AFD9;">
                                            <h4 class="mt-0 font-30" style="color: white;" >10</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">Cell</h4>
                                          </div>
                                        </div>
                                    </div>

                                     <div class="col-md-2 text-center" >
                                    <div class="card m-b-20">
                                        <div class="card-body" style="background-color: #00617D;">
                                            <h4 class="mt-0 font-30" style="color: white;" >₹ 1200</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">Sales</h4>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>


                                    <div class="col-md-6">
                                    <div class="row">
                                     <div class="col-md-2 text-center" >
                                    <div class="card m-b-20">
                                        <div class="card-body" style="background-color: #20AFD9;">
                                            <h4 class="mt-0 font-30" style="color: white;" >₹ 2000</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">Total Animals</h4>
                                          </div>
                                        </div>
                                    </div>

                                     <div class="col-md-2 text-center">
                                    <div class="card m-b-20">
                                        <div class="card-body" style="background-color: #00617D;">
                                            <h4 class="mt-0 font-30" style="color: white;" >23</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">New Animals</h4>
                                          </div>
                                        </div>
                                    </div>

                                     <div class="col-md-2 text-center" >
                                    <div class="card m-b-20">
                                        <div class="card-body" style="background-color: #20AFD9;">
                                            <h4 class="mt-0 font-30" style="color: white;" >2</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">Births</h4>
                                          </div>
                                        </div>
                                    </div>

                                     <div class="col-md-2 text-center" >
                                    <div class="card m-s-20 rounded">
                                        <div class="card-body" style="background-color: #00617D;">
                                            <h4 class="mt-0 font-30" style="color: white;" >10</h4>
                                          </div>
                                          <div class="card-body">
                                            <h4 class="font-16">Entry</h4>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                 </div>

                                </div>



                            </div>
                            <!-- end row -->


            <div class="row">
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center" style="height: 107px;">
                       <img src="assets/images/pawprint.svg" style="    margin-top: 20px;"><br>
                        <h6 style="font-size: 16px;" >Animals</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
              <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center" style="height: 107px;"><br>
                       <img src="assets/images/bar-chart.svg"><br>
                        <h6   style="font-size: 16px;">Stock Statistics</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>

              <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto"><br>
                      <div class="stats-small__data text-center" style="height: 82px;"><br>
                       <img src="assets/images/group.svg" style="    margin-top: -31px;">
                        <h6  style="font-size: 16px;">Employee Profile</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>


              <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/log-in.svg"><br>
                        <h6  style="font-size: 16px;">Entry Logs</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/milk.svg"><br>
                        <h6  style="font-size: 16px;">Milk Statistics</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
               <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/bar-chart-reload.svg"><br>
                        <h6  style="font-size: 16px;">Feed Chart</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>


               <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/shopping-list.svg"><br>
                        <h6  style="font-size: 16px;">Attendence</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
               <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/check-list.svg"><br>
                        <h6   style="font-size: 16px;">Report / Complaints</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div class="row">
               <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/get-money.svg"><br>
                        <h6 style="font-size: 16px;">Sales</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>


               <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small" style="height: 104px;">
                  <div class="card-body p-0 d-flex" style="    margin-top: -16px;">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/bar-chart.svg"><br>
                        <h6   style="font-size: 16px;">Expense</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
              <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex" style="    margin-top: -3px;">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/account.svg"><br>
                        <h6 style="font-size: 16px;">Division</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
               <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                  <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                      <div class="stats-small__data text-center"><br>
                      <img src="assets/images/write-board.svg"><br>
                        <h6 style="font-size: 16px;">Notice Board</h6>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>

            </div>

@endsection

@section('script')
<!--Morris Chart-->
<script src="{{ URL::asset('assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script>

<script src="{{ URL::asset('assets/pages/dashboard.js')}}"></script>
@endsection
