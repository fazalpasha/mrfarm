@extends('layouts.master')
@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Food Stock</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('stock.index')}}">Stock Management</a></li>
                    <li class="breadcrumb-item">Food Stock</li>
                </ol>
                <div class="state-information d-none d-sm-block">
                    <div class="btn-group pull-right">
                        <a href="{{ route('foodstock.create') }}" class="btn btn-primary btn-lg waves-effect waves-light float-left" >
                        Create </a>
                    </div>
                </div>
            </div>
        </div>
    </div><br>

    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Stock</h4>
                    <table id="datatable" class="table table-striped table-bordered nowrap dataTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Category Name</th>
                                <th>Subcategory Name</th>
                                <th>Item Name</th>
                                <th>Vendor Name</th>
                                <th>Received By Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($data)): ?>
                                <?php foreach ($data as $foodstock): ?>
                                    <tr>
                                        <td>{{$foodstock->id}}</td>
                                        <td>{{$foodstock->category_name}}</td>
                                        <td>{{$foodstock->subcategory_name}}</td>
                                        <td>{{$foodstock->item_name}}</td>
                                        <td>{{$foodstock->vendor_name}}</td>
                                        <td>{{$foodstock->received_by_name}}</td>
                                        <!-- <td>{{$foodstock->document_upload}}</td> -->
                                        <td> <div class="btn-group">
                                            <a href="{{ route('foodstock.edit', [$foodstock->id]) }}" class="btn btn-primary btn-flat"><i class="ion-edit"></i></a>
                                            <button class="btn btn-danger btn-flat"  data-message="Are you Sure You Want to Delete This FoodMedicalstock Record ?" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('foodstock.delete', [$foodstock->id]) }}"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

</div> <!-- container-fluid -->
@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ URL::asset('assets/plugins/datatables/tableresponse.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{ URL::asset('assets/pages/datatables.init.js')}}"></script>
@endsection