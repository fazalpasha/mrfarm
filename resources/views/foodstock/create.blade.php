@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
        <h4 class="page-title">Food Stock</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{route('stock.index')}}">Stock Management</a></li>
          <li class="breadcrumb-item"><a href="{{route('foodstock.index')}}">Food Stock</a></li>
          <li class="breadcrumb-item active">Add Stock</li>
        </ol>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::open(array('route' => 'foodstock.store','files' => true,'method'=>'POST')) !!}
      <div class="card m-b-20">
        <div class="card-body">
          <div class="box-body">
            <div class="row">

                                 <div class="col-sm-3">
                                <div class="form-group">
                  {!! Form::Label('Category Name', 'Category Name:') !!}
                  <select class="form-control" id="topcat" name="category_id">
                    <option value="">Select Category</option>
                   @foreach($category as $key => $categorys)
                   <option value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                   @endforeach
                  </select>
                                </div>
                            </div>


           <div class="col-sm-3">
            <div class="form-group">
              {!! Form::Label('Sub Category Name', 'Sub Category Name:') !!}
              <select required class="form-control" id="category" name="subcategory_id">
              </select>
            </div>
          </div>


          <div class="col-sm-3">
            <div class="form-group{{ $errors->has('qrcode') ? ' has-error' : '' }}">
              {!! Form::label('qrcode', trans('Qr Code')) !!}
              {!! Form::text('qrcode', old('qrcode'), ['class' => 'form-control', 'placeholder' => trans('Qr Code')]) !!}
              {!! $errors->first('qrcode', '<span class="help-block">:message</span>') !!}
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group{{ $errors->has('item_name') ? ' has-error' : '' }}">
              {!! Form::label('item_name', trans('Item Name')) !!}
              {!! Form::text('item_name', old('item_name'), ['class' => 'form-control', 'placeholder' => trans('Item Name')]) !!}
              {!! $errors->first('item_name', '<span class="help-block">:message</span>') !!}
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group {{ $errors->has('vendor_name') ? ' has-error' : '' }}">
              {!! Form::Label('Vendor', 'Vendor:') !!}
              <select  class="form-control select2" name="vendor_id">
               <option value="">Select Vendor</option>
               @foreach($vendor as $key => $vendors)
               <option <?php if(isset($request->vendor_id)){ ?> <?php if($request->vendor_id == $vendors->id) {?> selected <?php }?> <?php }?>value="{{$vendors->id}}">#MRVE{{$vendors->id}}--{{$vendors->vendor_name}}--{{$vendors->phone}}--{{$vendors->city}}</option>
               @endforeach
             </select>
             {!! $errors->first('vendor_name', '<span class="help-block">:message</span>') !!}
           </div>
         </div>

         <div class="col-sm-3">
          <div class="form-group{{ $errors->has('billing_amount') ? ' has-error' : '' }}">
            {!! Form::label('billing_amount', trans('Billing Amount')) !!}
            {!! Form::text('billing_amount', old('billing_amount'), ['class' => 'form-control', 'placeholder' => trans('Billing Amount')]) !!}
            {!! $errors->first('billing_amount', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

        <div class="col-sm-3">
            {!! Form::label('receipt_no', trans('Receipt No')) !!}
            <?php if(isset($request->receipt_no)){ ?>
              {!! Form::text('receipt_no', old('receipt_no',$request->receipt_no), ['class' => 'form-control','required', 'placeholder' => trans('Receipt No')]) !!}
            <?php }else{?>
              {!! Form::text('receipt_no', old('receipt_no'), ['class' => 'form-control','required', 'placeholder' => trans('Receipt No')]) !!}
            <?php }?>
            {!! $errors->first('receipt_no', '<span class="help-block">:message</span>') !!}
          </div>
       

        <div class="col-sm-3">
          <div class="form-group{{ $errors->has('invoice_no') ? ' has-error' : '' }}">
            {!! Form::label('invoice_no', trans('Invoice No')) !!}
            {!! Form::text('invoice_no', old('invoice_no'), ['class' => 'form-control', 'placeholder' => trans('Invoice No')]) !!}
            {!! $errors->first('invoice_no', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

          <input type="hidden" name="invoice_prefix" value="0">
          <div class="col-sm-3">
          <div class="form-group{{ $errors->has('quality') ? ' has-error' : '' }}">
            {!! Form::label('quality', trans('Quality')) !!}
            {!! Form::text('quality', old('quality'), ['class' => 'form-control', 'placeholder' => trans('Quality')]) !!}
            {!! $errors->first('quality', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
            {!! Form::label('quantity', trans('Quantity')) !!}
            {!! Form::text('quantity', old('quantity'), ['class' => 'form-control', 'placeholder' => trans('Quantity')]) !!}
            {!! $errors->first('quantity', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
            {!! Form::label('date', trans('Date')) !!}
            {!! Form::date('date', old('date',date('Y-m-d')), ['class' => 'form-control', 'placeholder' => trans('Date')]) !!}
            {!! $errors->first('date', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group{{ $errors->has('received_by_name') ? ' has-error' : '' }}">
            {!! Form::label('received_by_name', trans('Received By Name')) !!}
            {!! Form::text('received_by_name', old('received_by_name'), ['class' => 'form-control', 'placeholder' => trans('Received By Name')]) !!}
            {!! $errors->first('received_by_name', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

                                   <div class="col-sm-3">
                                   <div class="form-group{{ $errors->has('document_upload') ? ' has-error' : '' }}">
                                       {!! Form::label('document_upload', trans('Document Upload ')) !!}
                                       <img width ="100px" id="img3">
                                       {!! Form::file('images', ['class' => 'form-control','onchange'=>'readURL1(this)','placeholder' => trans('Document Upload')]) !!}
                                       {!! $errors->first('document_upload', '<span class="help-block">:message</span>') !!}
                                   </div>
                                   </div>

        <div class="col-sm-12">
          <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
            {!! Form::label('remarks', trans('Remarks')) !!}
            {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Remarks')]) !!}
            {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

      </div>

                     <div class="">
                     <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                     <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
                 </div>

    </div>
  </div> <!-- end col -->
</div> 
<!-- end row -->
{!! Form::close() !!}
</div>
@endsection
@section('js_after')
<script>
  <?php if(isset($request->category_id)){ ?>
    $( document ).ready(function() {
      var top_cat = "{{$request->category_id}}";
      ajax(top_cat);
    });
  <?php }?>
  $('#topcat').on('change',function(){
    var top_cat = $(this).val();
    ajax(top_cat);

  });
  function ajax(top_cat) {

    url = '<?= route('foodstock.subcat') ?>';
    $.get(url, {"top_cat": top_cat}, categorylist, 'json');

    function categorylist(d, s) {

      var elSel = document.getElementById('category');
      var i;
      for (i = elSel.length; i >= 0; i--) {
        elSel.remove(i);
      }
      if (d.length > 0) {
        $("#category").append('<option value="">Select Sub Category</option>');
      } else {
        $("#category").append('<option value="">No Sub Category</option>');
      }

      $.each(d,
        function () {
          var str = this.id;
          var name = this.subcategory_name;
          var option = new Option(str, name);
          var dropdownList = document.getElementById("category");
          var option = document.createElement("option");
          if (typeof name == 'undefined') {
            name = '';
          }
          option.appendChild(document.createTextNode(name));
          option.value = str;
          dropdownList.appendChild(option);
          if (name == 'error') {
          } else {
          }
        }
        );
    }
  }

  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();
    });
  });

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
  }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
  });
</script>

@endsection