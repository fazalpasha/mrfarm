@extends('layouts.master')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Vehical Report</h4>
                <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>

                 <li class="breadcrumb-item active"><a href="{{route('vehicalreports.index')}}">Vehical Report</a></li>
                 <li class="breadcrumb-item active">Add Vehical Report</a></li>
             </ol>


         </div>
     </div>
 </div>
 <!-- end row -->

 <div class="row">
    <div class="col-12">
        {!! Form::open(array('route' => 'vehicalreports.store','method'=>'POST')) !!}

        <div class="card m-b-20">
            <div class="card-body">

                <div class="box-body">
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('vehical_no') ? ' has-error' : '' }}">
                                {!! Form::label('vehical_no', trans('Vehical No')) !!}
                                {!! Form::text('vehical_no', old('vehical_no'), ['class' => 'form-control', 'placeholder' => trans('Vehical No')]) !!}
                                {!! $errors->first('vehical_no', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('vehical_type') ? ' has-error' : '' }}">
                                {!! Form::label('vehical_type', trans('Vehical Type')) !!}
                                {!! Form::text('vehical_type', old('vehical_type'), ['class' => 'form-control', 'placeholder' => trans('Vehical Type')]) !!}
                                {!! $errors->first('vehical_type', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('driver_name') ? ' has-error' : '' }}">
                                {!! Form::label('driver_name', trans('Driver Name')) !!}
                                {!! Form::text('driver_name', old('driver_name'), ['class' => 'form-control', 'placeholder' => trans('Driver Name')]) !!}
                                {!! $errors->first('driver_name', '<span class="help-block">:message</span>') !!}
                            </div>
                           </div>

                            <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                                {!! Form::label('weight', trans('Vehical Weight')) !!}
                                {!! Form::text('weight', old('weight'), ['class' => 'form-control', 'placeholder' => trans('Weight')]) !!}
                                {!! $errors->first('weight', '<span class="help-block">:message</span>') !!}
                            </div>
                           </div>

                            <!-- <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('load_type') ? ' has-error' : '' }}">
                                {!! Form::label('load_type', trans('Load Type')) !!}
                                {!! Form::text('load_type', old('load_type'), ['class' => 'form-control', 'placeholder' => trans('Load Type')]) !!}
                                {!! $errors->first('load_type', '<span class="help-block">:message</span>') !!}
                            </div>
                           </div> -->

                            <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('wheels') ? ' has-error' : '' }}">
                                {!! Form::label('wheels', trans('Wheels')) !!}
                                {!! Form::text('wheels', old('wheels'), ['class' => 'form-control', 'placeholder' => trans('Wheels')]) !!}
                                {!! $errors->first('wheels', '<span class="help-block">:message</span>') !!}
                            </div>
                           </div>

                          

                         <div class="col-md-3">
                                 <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    {!! Form::label('type', trans('Type')) !!}<br>
                           <label class="radio-inline"> {!! Form::radio('type', '1', true ) !!} Yes </label> <label class="radio-inline"> {!! Form::radio('type', '0' ) !!} No </label>
                                </div>
                            </div>

                           <div class="col-md-3">
                                 <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('Status')) !!}<br>
                           <label class="radio-inline"> {!! Form::radio('status', '1', true ) !!} Yes </label> <label class="radio-inline"> {!! Form::radio('status', '0' ) !!} No </label>
                                </div>
                            </div>
                        </div>


                             <div class="col-sm-12">
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            {!! Form::label('remarks', trans('Remarks')) !!}
                            {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Remarks')]) !!}
                            {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>



                       

                            




                </div>

                <div class="">
                    <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                     <a href="{{route('vehicalreports.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                </div>   






            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    {!! Form::close() !!}

</div> <!-- container-fluid -->
@endsection
@section('js_after')

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>

<script>
$(function() {
  $('.datetimepicker').datetimepicker({format: 'YYYY-MM-DD HH:MM:SS'});
  
  $('.datetimepicker-addon').on('click', function() {
    
    $(this).prev('input.datetimepicker').data('DateTimePicker').toggle();
  });
});
</script>
@endsection
