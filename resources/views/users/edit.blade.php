@extends('layouts.master')

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Users</h4>
                                    <ol class="breadcrumb">
                                       <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                                        
                                        <li class="breadcrumb-item active">Edit User</li>
                                    </ol>
            
                                   
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                       
        
                        <div class="row">
                            <div class="col-lg-12">

<!-- {!! Form::open(array('route' => 'users.store','method'=>'put')) !!} -->
 {!! Form::open(['route' => ['users.update', $user->id], 'method' => 'put']) !!}

                                <div class="card m-b-20">
                                    <div class="card-body">
        
                                        
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Data</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Roles</a>
                                            </li>
                                             <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#profile2" role="tab">Change Password</a>
                                            </li>
                                            
                                           
                                           
                                        </ul>
        
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active p-3" id="home1" role="tabpanel">
                                                <p>
                                                   <div class="tab-pane active" id="tab_1-1">
                                                   <div class="box-body"> 
                                                   <div class="row">  
                                                <div class="col-md-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    {!! Form::label('name', trans('First Name')) !!}
                                    {!! Form::text('name', old('name',$user->name), ['class' => 'form-control', 'placeholder' => trans('First Name')]) !!}
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> 

                            <div class="col-md-4">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    {!! Form::label('last_name', trans('Last Name')) !!}
                                    {!! Form::text('last_name', old('last_name',$user->last_name), ['class' => 'form-control', 'placeholder' => trans('Last Name')]) !!}
                                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div> 

                             <div class="col-md-4">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {!! Form::label('email', trans('Email')) !!}
                                    {!! Form::text('email', old('email',$user->email), ['class' => 'form-control', 'placeholder' => trans('Email')]) !!}
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                      

                       
                    </div>
                </div><br>
                         
                                            

                </p>
                                            </div>
                                        <div class="tab-pane p-3" id="profile1" role="tabpanel">
                                            <div class="form-group">
                                            <div class="row">
                                                 <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::Label('Role','Role:') !!}
                             <select class="form-control" name="roles">
                             @foreach($roles as $key => $role)
                           <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                            </select>
                             </div>
                         </div>
                                            </div>
                                            </div>
                                        </div>

                                         <div class="tab-pane p-3" id="profile2" role="tabpanel">
                                            <div class="form-group">
                                             <div class="row">
                              <div class="col-md-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    {!! Form::label('password', trans('Password')) !!}
                                    <input type="password" name="password" class="form-control" placeholder="password">
                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                             <div class="col-md-6">
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    {!! Form::label('password_confirmation', trans('Password Conformation')) !!}
                                   <input type="password" name="password_confirmation" class="form-control" placeholder="confirm password">
                                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                     </div>
                </div>



                                         <div class="">
                                        <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>

                          <a href="{{route('users.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                          </div>

                           </div>
                        </div>
                    </div>
                </div>
        
        {!! Form::close() !!}
        
        
                     
            


                    </div> <!-- container-fluid -->
                    
@endsection