@extends('layouts.master')

@section('css')
@endsection

@section('content')
<style>
  .mb-4, .my-4 {
    margin-bottom: -0.5rem!important;
  }
  .metismenu > li {
    border-bottom:1px solid #eee;
  }

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<div class="container-fluid">

  <!-- <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
        <h4 class="page-title">Dashboard</h4>
        <ol class="breadcrumb">

        </ol>

      </div>
    </div>
  </div> -->
  <!-- end row -->

  <div class="row" style="margin-top: 10px;">
              <div class="col-xl-2 col-md-6">
                  <div class="card newrad mini-stat bg-warning">
                      <div class="card-body mini-stat-img">
                          <div class="mini-stat-icon">
                             <i class="mdi mdi-tag-text-outline float-right"></i>
                          </div>
                          <div class="text-white">
                              <h6 class="text-uppercase mb-3">ToTal Animals</h6>
                              <h4 class="mb-4">
                               <?php echo $count; ?>
                              </h4>
                              </span>
                          </div>
                      </div>
                  </div>
              </div>


              <div class="col-xl-2 col-md-6">
                  <div class="card newrad mini-stat bg-info">
                      <div class="card-body mini-stat-img">
                          <div class="mini-stat-icon">
                              <i class="mdi mdi-buffer float-right"></i>
                          </div>
                          <div class="text-white">
                              <h6 class="text-uppercase mb-3">Total Staff</h6>
                              <h4 class="mb-4">
                               <?php echo $totstaff ?></h4>
                            </span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-xl-2 col-md-6">
                  <div class="card newrad mini-stat bg-info">
                      <div class="card-body mini-stat-img">
                          <div class="mini-stat-icon">
                              <i class="mdi mdi-buffer float-right"></i>
                          </div>
                          <div class="text-white">
                              <h6 class="text-uppercase mb-3">Total Cells</h6>
                              <h4 class="mb-4">
                               <?php echo $totcell ?></h4>
                            </span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-xl-2 col-md-6">
                  <div class="card newrad mini-stat bg-info">
                      <div class="card-body mini-stat-img">
                          <div class="mini-stat-icon">
                              <i class="mdi mdi-buffer float-right"></i>
                          </div>
                          <div class="text-white">
                              <h6 class="text-uppercase mb-3">Total Sale</h6>
                              <h4 class="mb-4">
                               <?php echo $totsale ?></h4>
                            </span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-xl-2 col-md-6">
                  <div class="card newrad mini-stat bg-info">
                      <div class="card-body mini-stat-img">
                          <div class="mini-stat-icon">
                              <i class="mdi mdi-buffer float-right"></i>
                          </div>
                          <div class="text-white">
                              <h6 class="text-uppercase mb-3">Total Vendors</h6>
                              <h4 class="mb-4">
                               <?php echo $totvendor ?></h4>
                            </span>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-xl-2 col-md-6">
                  <div class="card newrad mini-stat bg-danger">
                      <div class="card-body mini-stat-img">
                          <div class="mini-stat-icon">
                              <i class="mdi mdi-tag-text-outline float-right"></i>
                          </div>
                          <div class="text-white">
                              <h6 class="text-uppercase mb-3">Total Expense</h6>
                              <h4 class="mb-4">₹ <?php echo $totexpense ?></h4>
                              </span>
                          </div>
                      </div>
                  </div>
              </div>

            
                          



<!-- Modal -->

          <div>
        </div>
      </div>
</div>

<div class="container-fluid" style="display: none;">
<div class="row">
  <div class="col-xl-4">
    <div class="card m-b-20">
      <div class="card-body">
        <h4 class="mt-0 header-title">Total Income</h4>
        <div class="row text-center m-t-20">
          <div class="col-6">
            <h5 class="">₹ <?php echo $totincome ?>
            </h5>
            <p class="text-muted"></p>
          </div>

<script src="{{ URL::asset('assets/plugins/chart.js/gochart.min.js')}}"></script>
<div id="piechart" style="position: relative; width: 500px; height: 330px;"></div>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Month', 'Income'],
       <?php
        if(isset($saleschart)){
      if(count($saleschart) > 0){
          foreach ($saleschart as $item){
              echo "['".$item->monthname."', ".$item->Count."],";
          }
      }}
      ?>


     
    ]);
var options = {
        width: 300,
        height: 300,
        is3D: true,
    };
    

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data,options);
}
</script>
            </div>
          </div>
        </div>
      </div>
<div class="col-xl-4">
  <div class="card m-b-20">
      <div class="card-body">
            <h4 class="mt-0 header-title">Visitors</h4>
            
                                            <div class="row text-center m-t-20">
                                                <div class="col-4">
                                                    <h5 class=""><?php echo $ventrylogs ?></h5>
                                                    <p class="text-muted">Total Members</p>
                                                </div>
                                            </div>



<div id="linechart"></div>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([

      ['', 'Members'],
      <?php
        if(isset($entrychart)){
      if(count($entrychart) > 0){
          foreach ($entrychart as $item){
              echo "['".$item->monthname."', ".$item->Count."],";
          }
      }}
      ?>
    ]);

    var options = {
        width: 280,
        height: 285,
        is3D: true,
    };
    
    var chart = new google.visualization.BarChart(document.getElementById('linechart'));
    
    chart.draw(data, options);
}
</script>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="card m-b-20">
                                        <div class="card-body">
                                            <h4 class="mt-0 header-title">Total Expenses</h4>

                                            <div class="row text-center m-t-20">
                                                <div class="col-6">
                                                    <h5 class="">₹ <?php echo $totexpense ?></h5>
                                                </div>
                                               </div>


<div id="barchart" style="position: relative; width: 220px; height: 340px;"></div>

<script type="text/javascript">
google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Month', 'Expenses'],
      <?php
        if(isset($expenses)){
      if(count($expenses) > 0){
          foreach ($expenses as $item){
              echo "['".$item->monthname."', ".$item->Count."],";
          }
      }}
      ?>
    ]);

    var options = {
        width: 280,
        height: 300,
        is3D: true,
    };
    var chart = new google.charts.Bar(document.getElementById('barchart'));

    chart.draw(data,options);
}
</script>
                                        </div>
                                    </div>
                                </div>

                            </div>

</div>
<!-- end row -->

<div class="container-fluid">
<div class="row">
  <div class="col-md-6">

    <div class="row">
      <div class="col-md-3">
        <a href="dashboard/animalstock/index">
          <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
               <img src="assets/images/pawprint.svg"><br>
               <h6 style="font-size: 16px;">Animals</a></h6>
             </div>

           </div>

         </div>
       </div>
     </div>
     <div class="col-md-3">
      <a href="dashboard/foodstock/index">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center"><br>
             <img src="assets/images/bar-chart.svg"><br>
             <h6   style="font-size: 16px;">Stock Statistics</h6>
           </div></a>

         </div>

       </div>
     </div>
   </div>

   <div class="col-md-3">
    <a href="dashboard/staff/index">
    <div class="stats-small stats-small--1 card card-small">
      <div class="card-body p-0 d-flex">
        <div class="d-flex flex-column m-auto">
          <div class="stats-small__data text-center"><br>
           <img src="assets/images/group.svg" ><br>
           <h6  style="font-size: 16px;">Employees</h6>
         </div></a>

       </div>

     </div>
   </div>
 </div>


 <div class="col-md-3">
  <a href="dashboard/entrylogs/index">
  <div class="stats-small stats-small--1 card card-small">
    <div class="card-body p-0 d-flex">
      <div class="d-flex flex-column m-auto">
        <div class="stats-small__data text-center"><br>
          <img src="assets/images/log-in.svg"><br>
          <h6  style="font-size: 16px;">Entry Logs</h6>
        </div></a>

      </div>

    </div>
  </div>
</div>
</div>
</div>
<!--  <div class="row"> -->
  <div class="col-md-6">
   <div class="row">

     <div class="col-md-3">
      <a href="dashboard/sales/index">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center"><br>
              <img src="assets/images/milk.svg"><br>
              <h6  style="font-size: 16px;">Milk Statistics</h6>
            </div></a>
          </div>

        </div>
      </div>
    </div>
    <div class="col-md-3">
      <a href="dashboard/feed/index">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center"><br>
              <img src="assets/images/bar-chart-reload.svg"><br>
              <h6  style="font-size: 16px;">Feed Chart</h6>
            </div></a>

          </div>

        </div>
      </div>
    </div>


    <div class="col-md-3">
      <a href="dashboard/staffattendance/index">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center"><br>
              <img src="assets/images/shopping-list.svg"><br>
              <h6  style="font-size: 16px;">Attendence</h6>
            </div></a>

          </div>

        </div>
      </div>
    </div>

    <div class="col-md-3">
      <a href="dashboard/complaint/index">
      <div class="stats-small stats-small--1 card card-small">
        <div class="card-body p-0 d-flex">
          <div class="d-flex flex-column m-auto">
            <div class="stats-small__data text-center"><br>
              <img src="assets/images/check-list.svg"><br>
              <h6   style="font-size: 16px;">Complaints</h6>
            </div></a>

          </div>

        </div>
      </div>
    </div>
  </div>
</div>
</div>


<div class="row">
  <div class="col-md-6">

    <div class="row">

      <div class="col-md-3">
        <a href="dashboard/sales/index">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/get-money.svg"><br>
                <h6 style="font-size: 16px;">Sales</h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>



      <div class="col-md-3">
        <a href="dashboard">
        <div class="stats-small stats-small--1 card card-small" style="height: 104px;">
          <div class="card-body p-0 d-flex" >
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/bar-chart.svg"><br>
                <h6   style="font-size: 16px;">Expense</h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>
      <div class="col-md-3">
        <a href="dashboard/cell/index">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex" style="    margin-top: -3px;">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/account.svg"><br>
                <h6 style="font-size: 16px;">Division</h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>
       <div class="col-md-3">
        <a href="dashboard/cell/index">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/write-board.svg"><br>
                <h6 style="font-size: 16px;">Cell Box </h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

        <div class="col-md-6">

    <div class="row">
       <div class="col-md-3">
        <a href="dashboard/payslip/index">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/write-board.svg"><br>
                <h6 style="font-size: 16px;">Payslips</h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>
       <div class="col-md-3">
        <a href="dashboard/vendor/index">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/write-board.svg"><br>
                <h6 style="font-size: 16px;">Vendors</h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>
      <div class="col-md-3">
        <a href="dashboard/vehicalreports/index">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/write-board.svg"><br>
                <h6 style="font-size: 16px;">Vehicles</h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>

       <div class="col-md-3">
        <a href="dashboard">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center"><br>
                <img src="assets/images/write-board.svg"><br>
                <h6 style="font-size: 16px;">Notice Board</h6>
              </div></a>

            </div>

          </div>
        </div>
      </div>
    </div>



  </div>

</div>

                              <div class="row">
                                <div class="col-xl-6">
                                    <div class="card m-b-20">
                                        <div class="card-body">
                                            <h4 class="mt-0 m-b-30 header-title">Latest Sales</h4>

                                            <div class="table-responsive">
                                                <table class="table table-vertical">
                                                  <th>S.No</th>
                                                  <th>Sold to</th>
                                                  <th>Total Weight</th>
                                                  <th>Total Price</th>
                                                  <th>Date</th>

                            <?php foreach ($latestsale as $item){?>
                                                    <tbody>
                                                    <tr>
                                                      <td>{{ $item->id }}</td>
                                                        <td>{{$item->vendor_name}}
                                                        </td>
                                                        <td></i>{{$item->total_weight}} </td>
                                                        <td>
                                                        {{$item->total_price }}

                                                        </td>
                                                        <td>
                                                            {{$item->created_at->format('d/m/Y')}}
                                                        </td>
                                                        <?php }?>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-6">
                                    <div class="card m-b-20">
                                        <div class="card-body">
                                            <h4 class="mt-0 m-b-30 header-title">Latest Animal Added</h4>

                                            <div class="table-responsive">
                                                <table class="table table-vertical mb-1">
                                                  <th>S.No</th>
                                                  <th>Name</th>
                                                  <th>Price</th>
                                                  <th>Date</th>

                                                     <?php foreach ($latestanimal as $item){?>
                                                    <tbody>
                                                    <tr>
                                                        <td>{{ $item->id}}</td>
                                                        <td>
                                                            <img src="{{env('APP_URl').'/'.$item->image}}" alt="user-image" class="thumb-sm mr-2 rounded-circle">{{ $item->category_name}}

                                                        </td>
                                                        <td>{{$item->billing_amount}}
                                                        </td>
                                                        <td>
                                                            {{date('d-m-Y', strtotime($item->date))}}
                                                        </td>
                                                        <?php  }?>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
  </div>
</div>



<!-- end row -->

<!-- container-fluid -->
@endsection

@section('script')

<script src="{{ URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script>
<script src="{{ URL::asset('assets/pages/dashboard.js')}}"></script>
@endsection
