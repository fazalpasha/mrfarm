@extends('layouts.master-without-nav')


@section('content')
        <!-- Begin page -->
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center m-0">
                        <a href="index" class="logo logo-admin"><img src="{{URL::asset('assets/images/logo-mr.png')}}" height="100" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                        <h4 class="text-muted font-18 m-b-5 text-center">Welcome Back !</h4>
                       <!--  <p class="text-muted text-center">Sign in to continue to Mr Farms</p> -->

                      <!--   <form class="form-horizontal m-t-30" method="POST" action="{{ url('/login') }}"> -->
                           <!--   {{ csrf_field() }} -->
                              
                           {!! Form::open(['route' => ['login'], 'method' => 'post','files' => true]) !!}
                                        <div class="py-3">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                {!! Form::text("email", old("email"), ['class' => 'form-control form-control-lg form-control-alt', 'placeholder' => trans('email or phone number')]) !!}
                                                {!! $errors->first("email", '<span class="help-block parsley-required animated fadeIn">:message</span>') !!}
                                            </div>
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <input type="password" class="form-control form-control-alt form-control-lg" id="login-password" name="password" placeholder="Password">
                                                {!! $errors->first("password", '<span class="help-block parsley-required animated fadeIn">:message</span>') !!}
                                            </div>

                                        </div>

                            <div class="form-group row m-t-20">
                                <div class="col-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customControlInline">
                                        <label class="custom-control-label" for="customControlInline">Remember me</label>
                                    </div>
                                </div>
                                <div class="col-6 text-right">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>

                            <!-- <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="pages-recoverpw" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                </div>
                            </div> -->
                        </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <!-- <p>Don't have an account ? <a href="pages-register" class="font-14 text-primary"> Signup Now </a> </p> -->
                <p>
                © {{date('Y')}} MR Farm<span class="d-none d-sm-inline-block"> - Crafted with<i class="mdi mdi-heart text-danger"></i> by<a href="http://bigome.com/"> Bigome</a></span>.
                </p>
            </div>

        </div>
        
@endsection