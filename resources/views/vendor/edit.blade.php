@extends('layouts.master')

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Vendor</h4>
                                    <ol class="breadcrumb">
                                       <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('vendor.index')}}">vendor</a></li>
                                        
                                        <li class="breadcrumb-item active">Edit Vendor</li>
                                    </ol>
            
                                   
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                 {!! Form::open(['route' => ['vendor.update', $vendor->id], 'method' => 'put']) !!}
                          
                                <div class="card m-b-20">
                                    <div class="card-body">
        
                        <div class="box-body">
                        <div class="row">
                           
                           
                           
                            
                             <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('vendor_name') ? ' has-error' : '' }}">
                                    {!! Form::label('vendor_name', trans('Vendor Name')) !!}
                                    {!! Form::text('vendor_name', old('vendor_name',$vendor->vendor_name), ['class' => 'form-control', 'placeholder' => trans('Vendor Name')]) !!}
                                    {!! $errors->first('vendor_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                             <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {!! Form::label('email', trans('Email')) !!}
                                    {!! Form::text('email', old('email',$vendor->email), ['class' => 'form-control', 'placeholder' => trans('Email')]) !!}
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    {!! Form::label('phone', trans('Phone')) !!}
                                    {!! Form::text('phone', old('phone',$vendor->phone), ['class' => 'form-control','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','maxlength'=> 10, 'placeholder' => trans('phone number')]) !!}
                                    {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                              <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    {!! Form::label('city', trans('City')) !!}
                                    {!! Form::text('city', old('city',$vendor->city), ['class' => 'form-control', 'placeholder' => trans('City')]) !!}
                                    {!! $errors->first('city', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                               <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::Label('SubCategory Name', 'SubCategory Name:') !!}
                             <select class="form-control" name="id">
                             @foreach($subcategory as $key => $subcategorys)
                           <option value="{{$subcategorys->id}}">{{$subcategorys->subcategory_name}}</option>
                            @endforeach
                            </select>
                             </div>
                         </div>

                         <div class="col-sm-12">
                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    {!! Form::label('address', trans('Address')) !!}
                                    {!! Form::textarea('address', old('address',$vendor->address), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Address')]) !!}
                                    {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                </div>
                           </div>
                           
                            
                            
                         
                            
                    </div>
                            
                                          <div class="">
                                        <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>

                          <a href="{{route('vendor.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                          </div>   
                                        
                                        
                                       
                                       
                                       
                                       
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

         {!! Form::close() !!}

                    </div> <!-- container-fluid -->
@endsection