@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Credit</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('credit.index')}}">Credit</a></li>
                        <li class="breadcrumb-item active">Add</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                {!! Form::open(array('route' => 'credit.store','method'=>'POST','files' =>'true')) !!}

                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}" >
                                        {!! Form::label('date', 'Date') !!}<br>
                                        <input  name="date" type="date" id="date" class="form-control">
                                        {!! $errors->first('date', '<span class="help-block">:message</span>') !!}
                                </div>
                                </div>

                                    <div class="col-sm-4">
                                        <div class="form-group {{ $errors->has('time') ? ' has-error' : '' }}" >
                                            {!! Form::label('time', 'Time') !!}<br>
                                            <input  name="time" type="time" id="time" class="form-control">

                                            {!! $errors->first('time', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    <div class="form-group">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('payment_method') ? ' has-error' : '' }}">
                                        {!! Form::label('payment_method', 'Payment Method') !!}<br>
                                        <select class="form-control" name="payment_method">
                                            <option value="">Select Payment Mode</option>
                                            <option value="cash">cash</option>
                                            <option value="cheque">Cheque</option>
                                            <option value="card">Card</option>
                                            <option value="online payment">online Payment</option>
                                        </select>
                                        {!! $errors->first('payment_method', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                                        {!! Form::label('amount', 'Amount') !!}<br>
                                        <input  name="amount" type="number" id="amt" class="form-control">
                                        {!! $errors->first('amount', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('paid_by') ? ' has-error' : '' }}" >
                                        {!! Form::label('paid_by', 'Paid By') !!}<br>
                                        <input  name="paid_by" type="text" id="paid_by" class="form-control">
                                        {!! $errors->first('paid_by', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('paid_to') ? ' has-error' : '' }}" >
                                        {!! Form::label('paid_to', 'Paid To') !!}<br>
                                        <input  name="paid_to" type="text" id="paid_to" class="form-control">
                                        {!! $errors->first('paid_to', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('remarks') ? ' has-error' : '' }}">
                                        {!! Form::label('remarks', trans('Remarks')) !!}
                                        {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control','rows' =>3,'required', 'placeholder' => trans('Remarks')]) !!}
                                        {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            </div>

                            <div class="">
                                <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                                <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
                            </div>

                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

                {!! Form::close() !!}

            </div> <!-- container-fluid -->
        </div>
    </div>

@endsection
@section('js_after')
@endsection
