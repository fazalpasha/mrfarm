@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Type</h4>
                <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                 <li class="breadcrumb-item"><a href="{{route('qrcode.index')}}">QRCode</a></li>

                 <li class="breadcrumb-item active">Edit QR Code</li>
             </ol>
         </div>
     </div>
 </div>
 <!-- end row -->

<div class="row">
        <div class="col-md-3 pl-1 pr-1">
            <div class="card m-b-20">
                {!! Form::open(['route' => ['qrcode.update', $qrcode->id], 'method' => 'put']) !!}
                <div class="card-body">
                    <h5>Create QRCode</h5>
                    {!! Form::open(['route' => ['qrcode.store'], 'method' => 'post','files' => true]) !!}
                    <div class="col-sm-12 p-0">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            {!! Form::label('type', trans('type')) !!}
                            <select class="form-control type" name="type">
                                <option value="">Select Type</option>
                                <?php if(isset($topcat)){ ?>
                                    <?php foreach ($topcat as $topcats){ ?>

                                        <option value="{{$topcats->id}}"> {{$topcats->topcategory_name}}</option>
                                    <?php }?>
                                <?php }?>
                            </select>
                            {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="col-sm-12 p-0">
                        <div class="form-group{{ $errors->has('Prefix') ? ' has-error' : '' }}">
                            {!! Form::label('prefix', trans('Prefix')) !!}
                            {!! Form::text('prefix', old('prefix',$qrcode->prefix), ['class' => 'form-control', 'placeholder' => trans('Prefix')]) !!}
                            {!! $errors->first('prefix', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 p-1">
                            <div class="form-group{{ $errors->has('range_from') ? ' has-error' : '' }}">
                                {!! Form::label('range_from', trans('Range From')) !!}
                                {!! Form::number('range_from', old('range_from',$qrcode->range_from), ['class' => 'form-control max', 'placeholder' => trans('Range From'),'oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");']) !!}
                                {!! $errors->first('range_from', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-sm-6 p-1">
                            <div class="form-group{{ $errors->has('range_from') ? ' has-error' : '' }}">
                                {!! Form::label('range_to', trans('Range To')) !!}
                                {!! Form::text('range_to', old('range_to',$qrcode->range_to), ['class' => 'form-control', 'placeholder' => trans('Range To'),'oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");']) !!}
                                {!! $errors->first('range_to', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                    </div>
                    <button  type="submit" class="btn btn-primary btn-lg waves-effect waves-light align-items-center ">Generate QRCode</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
</div> <!-- container-fluid -->
@endsection