@extends('layouts.master')

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Type</h4>
                <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                 <li class="breadcrumb-item active"><a href="{{route('type.index')}}">Type</a></li>
                 <li class="breadcrumb-item active">Add Type</li>
             </ol>
         </div>
     </div>
 </div>
 <!-- end row -->

 <div class="row">
    <div class="col-12">
        {!! Form::open(array('route' => 'type.store','method'=>'POST')) !!}

        <div class="card m-b-20">
            <div class="card-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                {!! Form::label('type', trans('Type')) !!}
                                {!! Form::text('type', old('type'), ['class' => 'form-control', 'placeholder' => trans('Type')]) !!}
                                {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                        <a href="{{route('type.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

{!! Form::close() !!}

</div> <!-- container-fluid -->
@endsection
