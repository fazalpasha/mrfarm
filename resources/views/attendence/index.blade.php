@extends('layouts.master')

@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .hide{
        display: none!important;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Attendance </h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Attendances </a></li>
                </ol>
                <div class="state-information d-none d-sm-block">
                </div>


            </div>
        </div>
    </div><br>
    <!-- end row -->

    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">
                <div class="box-header">

                    <div class="tab-content">
                        <form  action="{{ route('staffattendance.index') }}" method="get">
                            <div class="row">

                                <div class="col-sm-3">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        {!! Form::label('Securityid', trans('Select Month')) !!}
                                        {!! Form::selectMonth('month',date('m'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        {!! Form::label('from', trans('Select Year')) !!}
                                        <select class="form-control" name="year">

                                            <option selected>{{date('Y')}}</option>
                                            <option value="2018">2018</option>
                                            <option value="2017">2017</option>

                                        </select>
                                        {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>


                                                <!--  <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('Address') ? ' has-error' : '' }}">
                                    {!! Form::label('to', trans('To Date')) !!}
                                                {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('user::users.form.last-name')]) !!}
                                                {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>  -->
                                                <div class="col-sm-1">
                                                    <br>
                                                    <button type="submit" class="btn btn-primary btn-rounded btn-rounded">{{ trans('Search') }}</button>
                                                </div>
                                            </form>

                                            <div class="col-sm-2">
                                                <br>
                                                <a href="#" class="btn btn-primary btn-rounded btn-rounded" data-toggle="modal" data-target=".bs-attedance-modal-sm" >{{ trans('Manual Entry') }}</a>
                                            </div>


                                            <div class="col-sm-3">
                                                <label>P => Present</label><br>
                                                <label style="color: red">A => Absent</label><br>
                                                <label style="color: orange">L => Leave</label><br>
                                                <label style="color: blue">W => Week-Off</label><br>
                                                <label style="color: #530866e6">H => Half-Day</label>
                                                <br>
                                                <label style="color: #0e6b90e6">TWD => Total-Working-Days</label>
                                            </div>
                                        </div>


                                        <div class="row" style="overflow-x: auto;clear: both;">
                                        </div>
                                    </div>

                                    <!-- /.box-header -->
                                    <div class="box-body" style="overflow: scroll;">
                                        @section('styles')

                                        @stop
                                        <style type="text/css">
                                            td {
                                                min-width: 30px;
                                                border: 1px solid #cacaca;
                                                padding: 8px;
                                                text-align: center;
                                            }
                                            th {
                                                min-width: 30px;
                                                border: 1px solid #cacaca;
                                                padding: 8px;
                                                margin: 4px;
                                                font-size: 14px;
                                                font-family: sans-serif;
                                                text-align: center;
                                            }
                                            .buttonclass{
                                                border-radius: 0;
                                                -webkit-box-shadow: none;
                                                -moz-box-shadow: none;
                                                box-shadow: none;
                                                border-width: 1px;
                                            }
                                            .table_card{
                                                box-shadow: 0px 1px 10px 0px #cacaca !important;
                                            }
                                        </style>
                                        <div style="width: 100%;">

                                            <div style="width: 70%;float: left;">
                                                <h4 style="padding-bottom: 15px;"> Attendance  : <b>{{$dateline}} </b> :</h4>
                                            </div>
                                            <?php if($enableattedance):?>
                                                <div style="width: 30%;float: right;">
                                                    <form>
                                                        <input type="hidden" name="location_id" value="{{$request->location_id}}">
                                                        <input type="hidden" name="security_id" value="{{$request->security_id}}">
                                                        <input type="hidden" name="month" value="{{$request->month}}">
                                                        <input type="hidden" name="year" value="{{$request->year}}">
                                                        <button name="submit" value="csvexport" class="buttonclass btn-rounded" style="padding: 8px;background-color: #367fa9;color: #ffffff;">Export Excel</button>
                                                    </form>

                                                </div>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2" style="z-index: 9;width: 29% !important;padding-left: 12px !important;padding-right:40 !important">
                                            <table style="float: left;background: white;">
                                                <tr>

                                                    <?php
                                                    $sln =0;
                                                    foreach ($header as $key => $value):?>

                                                        <?php if($sln < 3):?>
                                                            <th class="tables">{{$value}} </th>

                                                        <?php endif ;?>
                                                        <?php

                                                        $sln++;
                                                    endforeach; ?>
                                                </tr>


                                                <?php

                                                $index = 0;
                                                foreach ($productRecset as $key => $value):?>
                                                    <?php
                                                    $colorset = ($index % 2 == 1)?"style='background-color:#f1f1f1;height: 80px;'":"style='height: 80px;'";
                                                    $index++;
                                                    $sl=0;
                                                    ?>
                                                    <tr {!! $colorset !!}>
                                                        <?php foreach ($value as $k => $attendence):?>


                                                            <?php

                                                            if(isset($attendence) && $attendence == "A"):$color ='red';elseif(isset($attendence) && $attendence == "L"):$color = 'yellow';elseif(isset($attendence) && $attendence == "P"):$color ='green'; else: $color ='black'; endif;?>

                                                            <?php if($sl < 3):?>

                                                                <td class="tables" style="color: {{$color}}"><b>{{strip_tags($attendence)}}</b></td>



                                                            <?php endif;



                                                            $sl++;?>

                                                        <?php endforeach ;?>
                                                    </tr>
                                                <?php  endforeach; ?>


                                            </table>

                                        </div>
                                        <div class="col-md-10" style="overflow: scroll;margin-left: -21px;width: 72% !important">
                                            <table class="table_card">
                                                <tr>
                                                    <?php
                                                    $sln =0;
                                                    foreach ($header as $key => $value):?>

                                                        <?php if($sln >= 3):?>
                                                            <th class="tables">{{$value}} </th>

                                                        <?php endif ;?>
                                                        <?php

                                                        $sln++;
                                                    endforeach; ?>
                                                </tr>


                                                <?php

                                                $index = 0;
                                                foreach ($productRecset as $key => $value):?>
                                                    <?php
                                                    $color = ($index % 2 == 1)?"style='background-color:#f1f1f1;height: 80px;'":"style='height: 80px;'";
                                                    $index++;
                                                    $sl=0;
                                                    ?>
                                                    <tr {!! $color !!}>
                                                        <?php foreach ($value as $k => $attendence):?>


                                                            <?php

                                                            if(isset($attendence) && $attendence == "A"):$color ='red';elseif(isset($attendence) && $attendence == "L"):$color = 'yellow';elseif(isset($attendence) && $attendence == "P"):$color ='green'; else: $color ='black'; endif;?>

                                                            <?php if($sl >= 3):?>

                                                                <td class="tables" style="color: {{$color}}"><b>{{strip_tags($attendence)}}</b></td>


                                                            <?php endif;


                                                            $sl++;?>

                                                        <?php endforeach ;?>
                                                    </tr>
                                                <?php  endforeach; ?>


                                            </table>
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>
                            </div>


                            <div class="modal fade bs-attedance-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog modal-lg" role="document" style="width: 40%">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                            <h4 class="modal-title">Manual Entry :</h4>
                                        </div>
                                        {!! Form::open(['route' => ['staffattendance.attdupdate'], 'method' => 'post','files' => true]) !!}
                                        <div class="modal-body" style="text-align: center;">
                                            <div class="box-body">


                                                <div class="row" style="padding-top: 10px;">
                                                    <div class="col-md-4">
                                                        {!! Form::label('Emp_name', trans('Select Staff')) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select class="form-control" name="staffid" required="required">
                                                            <option>Select Staff</option>
                                                            <?php foreach ($staffs as $staff):?>

                                                                <option value="{{$staff->id}}">{{$staff->first_name}}</option>

                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top: 10px;">
                                                    <div class="col-md-4">
                                                        {!! Form::label('date', trans('Select Date')) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::date('date', old('date'), ['class' => 'form-control','required' => 'required']) !!}
                                                    </div>
                                                </div>


                                                <div class="row" style="padding-top: 10px;">
                                                    <div class="col-md-4">
                                                        {!! Form::label('client_name', trans('Mark Attedance')) !!}
                                                    </div>
                                                    <?php if($enableattedance == false):?>
                                                        <div class="col-md-8">
                                                            <select name="attedance" class="form-control" onchange="getval(this)" required="required">
                                                                <option> Select Status</option>
                                                                <option value="P"> Present</option>
                                                                <option value="A">Absent</option>
                                                                <option value="L">Leave</option>
                                                                <option value="W">Week-OFF</option>
                                                                <option value="H">Half-Day</option>
                                                            </select>
                                                        </div>
                                                        <?php else : ?>
                                                            <div class="col-md-8">
                                                                <select name="attedance" class="form-control" onchange="getval(this)" required="required">
                                                                    <option> Select Status</option>
                                                                    <option value="W">Week-OFF</option>
                                                                </select>
                                                            </div>

                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="row hide" style="padding-top: 10px;" id="idintime">
                                                        <div class="col-md-4">
                                                            {!! Form::label('date', trans('In Time')) !!}
                                                        </div>
                                                        <div class="col-md-8">
                                                            {!! Form::time('intime', old('intime'), ['class' => 'form-control','id' => 'intime']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="row hide" style="padding-top: 10px;" id="idouttime">
                                                        <div class="col-md-4">
                                                            {!! Form::label('date', trans('Out Time')) !!}
                                                        </div>
                                                        <div class="col-md-8">
                                                            {!! Form::time('outtime', old('outtime'), ['class' => 'form-control','id' => 'outtime']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                                <button class="btn btn-default pull-left" data-dismiss="modal"> Cancel</button>
                                                <button type="submit" class="btn btn-primary"> Submit</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end row -->

                            <!-- end row -->



                        </div> <!-- container-fluid -->





                        @endsection

                        @section('script')
                        <script type="text/javascript">

                            function getval(id){
                                if(id.value == 'P' || id.value == 'H'){
                                    $('#idintime').removeClass('hide');
                                    $('#idouttime').removeClass('hide');
                                    $("#intime").attr('required', '');
                                    $("#outtime").attr('required', '');
                                }else{
                                    $('#idintime').addClass('hide');
                                    $('#idouttime').addClass('hide');
                                    $("#intime").removeAttr('required');
                                    $("#outtime").removeAttr('required');
                                }
                            }


                        </script>
                        <!-- Required datatable js -->
                        <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
                        <!-- Buttons examples -->
                        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
                        <!-- Responsive examples -->
                        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
                        <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

                        <!-- Datatable init js -->
                        <script src="{{ URL::asset('assets/pages/datatables.init.js')}}"></script>
                        @endsection
