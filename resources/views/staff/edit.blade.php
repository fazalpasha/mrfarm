@extends('layouts.master')

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Staff Management</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('staff.index')}}">Staff</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">edit</a></li>

                    </ol>


                </div>
            </div>
        </div>
        <!-- end row -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-20">
                    <div class="card-body">
                    {!! Form::open(['route' => ['staff.update', $user->id], 'method' => 'put']) !!}
                    <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Personal Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Official Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#messages1" role="tab">Bank details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#settings1" role="tab">Reference Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#settings2" role="tab">Uploades</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div class="tab-pane active p-3" id="home1" role="tabpanel">
                                <div class="box-body">

                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                {!! Form::label('first_name', trans('First Name *')) !!}
                                                {!! Form::text('first_name', old('first_name',$user->first_name), ['class' => 'form-control', 'placeholder' => trans('First Name')]) !!}
                                                {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                {!! Form::label('last_name', trans('Last Name ')) !!}
                                                {!! Form::text('last_name', old('last_name',$user->last_name), ['class' => 'form-control', 'placeholder' => trans('Last name')]) !!}
                                                {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('nick') ? ' has-error' : '' }}">
                                                {!! Form::label('nick', trans('Nick Name *')) !!}
                                                {!! Form::text('nick', old('nick',$user->nick), ['class' => 'form-control', 'placeholder' => trans('nick name')]) !!}
                                                {!! $errors->first('nick', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                {!! Form::label('phone', trans('Phone *')) !!}
                                                {!! Form::text('phone', old('phone',$user->phone), ['class' => 'form-control','pattern'=>'[23456789][0-9]{9}','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('Phone Number'),'maxlength'=>'10']) !!}
                                                {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                {!! Form::label('email', trans('Email *')) !!}
                                                {!! Form::email('email', old('email',$user->email), ['class' => 'form-control','placeholder' => trans('Email')]) !!}
                                                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                                                {!! Form::label('age', trans('Age ')) !!}
                                                {!! Form::number('age', old('age',$user->age), ['class' => 'form-control','onkeypress' => 'return isNumber(event);', 'placeholder' => trans('Enter Age')]) !!}
                                                {!! $errors->first('age', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                                {!! Form::label('dob', trans('Date of Birth *')) !!}
                                                {!! Form::date('dob', old('dob',$user->dob), ['class' => 'form-control','placeholder' => trans('Enter date of birth')]) !!}
                                                {!! $errors->first('dob', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group{{ $errors->has('date_of_join') ? ' has-error' : '' }}">
                                                {!! Form::label('date_of_join', trans('Date of Join *')) !!}
                                                {!! Form::date('date_of_join', old('date_of_join',$user->date_of_join), ['class' => 'form-control' ,'placeholder' => trans('Enter join date')]) !!}
                                                {!! $errors->first('date_of_join', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group{{ $errors->has('blood_group') ? ' has-error' : '' }}">
                                                {!! Form::label('Blood Group', trans('Blood Group')) !!}
                                                <select class="form-control" name="blood_group">
                                                    <option value="O+">O+</option>
                                                    <option value="O+">O-</option>
                                                    <option value="A-">A-</option>
                                                    <option value="A+">A+</option>
                                                    <option value="O+">B+</option>
                                                    <option value="A-">B-</option>
                                                    <option value="A+">AB+</option>
                                                    <option value="A+">AB-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-2">
                                            <div class="form-group{{ $errors->has('qualification') ? ' has-error' : '' }}">
                                                {!! Form::label('qualification', trans('Qualification')) !!}
                                                {!! Form::text('qualification', old('qualification',$user->qualification), ['class' => 'form-control', 'placeholder' => trans('Enter Qualification ')]) !!}
                                                {!! $errors->first('qualification', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                                                {!! Form::label('father_name', trans('Father Name')) !!}
                                                {!! Form::text('father_name', old('father_name',$user->father_name), ['class' => 'form-control', 'placeholder' => trans('Father Name')]) !!}
                                                {!! $errors->first('father_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group{{ $errors->has('mother_name') ? ' has-error' : '' }}">
                                                {!! Form::label('mother_name', trans('Mother Name')) !!}
                                                {!! Form::text('mother_name', old('mother_name',$user->mother_name), ['class' => 'form-control','onkeypress' => 'return isNumber(event);', 'placeholder' => trans('Mother Name')]) !!}
                                                {!! $errors->first('mother_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                                {!! Form::label('state', trans('Select State')) !!}
                                                <select id="state" class="form-control" name="state">
                                                    <option value="">Select State</option>
                                                    <option value="Karanataka">Karanataka</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                                {!! Form::label('city', trans('Select City')) !!}
                                                <select id="city" class="form-control" name="city">
                                                    <option value="">Select State</option>
                                                    <option value="Bangalore">Bangalore</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                {!! Form::label('address', trans('Permanent Address *')) !!}
                                                {!! Form::textarea('address', old('address',$user->address), ['class' => 'form-control','rows' => 3, 'placeholder' => trans('Contact Address')]) !!}
                                                {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group{{ $errors->has('present_address') ? ' has-error' : '' }}">
                                                {!! Form::label('present_address', trans('Present Address')) !!}
                                                {!! Form::textarea('present_address', old('present_address',$user->present_address), ['class' => 'form-control','rows' => 3, 'placeholder' => trans('Present Address')]) !!}
                                                {!! $errors->first('present_address', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                                {!! Form::label('gender', trans('Gender')) !!}
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-primary btn-rounded  btn-md form-check-label active">
                                                        {!! Form::radio('gender', 1, ['class' => 'form-control', 'placeholder' => trans('gender')]) !!}
                                                        {!! $errors->first('gender', '<span class="help-block">:message</span>') !!}Female
                                                    </label>
                                                    &nbsp;&nbsp;
                                                    <label class="btn btn-primary btn-rounded  btn-md form-check-label">
                                                        {!! Form::radio('gender', 0, ['class' => 'form-control', 'placeholder' => trans('gender')]) !!}
                                                        {!! $errors->first('gender', '<span class="help-block">:message</span>') !!}Male
                                                    </label>


                                                </div>
                                                {!! $errors->first('gender', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('marital_status') ? ' has-error' : '' }}">
                                                {!! Form::label('marital_status', trans('Marital Status')) !!}
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-primary btn-rounded  btn-md form-check-label active">
                                                        {!! Form::radio('marital_status', 1, ['class' => 'form-control']) !!}
                                                        {!! $errors->first('marital_status', '<span class="help-block">:message</span>') !!}Married
                                                    </label>
                                                    &nbsp;&nbsp;
                                                    <label class="btn btn-primary btn-rounded  btn-md form-check-label">
                                                        {!! Form::radio('marital_status', 0, ['class' => 'form-control']) !!}
                                                        {!! $errors->first('marital_status', '<span class="help-block">:message</span>') !!}Single
                                                    </label>
                                                </div>
                                                {!! $errors->first('marital_status', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="tab-pane p-3" id="profile1" role="tabpanel">
                                <div class="box-body">
                                    <div class="row">


                                    <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                                        {!! Form::label('branch_id', trans('Select Workplace(Branch *)')) !!}
                                        <select class="form-control" name="branch_id">
                                            <option  value="">Select Branch</option>
                                            <?php if(isset($branch)){?>
                                                <?php foreach ($branch as $branchs){ ?>
                                                    <option value="{{$branchs->id}}">{{$branchs->name}}</option>
                                                <?php }?>
                                            <?php }?>
                                        </select>
                                        {!! $errors->first('branch_id', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('salary_id') ? ' has-error' : '' }}">
                                        {!! Form::label('salary_id', trans('Select Designation *')) !!}
                                        <select class="form-control" name="salary_id">
                                            <option value="">Select Designation</option>
                                            <option value="1">Manager</option>
                                            <option value="2">Staff</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('type_of_service') ? ' has-error' : '' }}">
                                        {!! Form::label('Service Type', trans('Select Service Department ')) !!}
                                        <select class="form-control" name="service_dept">
                                            <option>Select Department</option>
                                            <option value="Department1">Department1</option>
                                            <option value="Department2">Department2</option>
                                        </select>
                                        {!! $errors->first('service_dept', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
                                        {!! Form::label('salary', trans('Salary*')) !!}
                                        {!! Form::text('salary', old('salary'), ['class' => 'form-control','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");', 'placeholder' => trans('salary')]) !!}
                                        {!! $errors->first('salary', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                           
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('date_of_leave') ? ' has-error' : '' }}">
                                        {!! Form::label('date_of_leave', trans('Date of Leaving ')) !!}
                                        {!! Form::date('date_of_leave', old('date_of_leave'), ['class' => 'form-control', 'placeholder' => trans('Enter date of leaving')]) !!}
                                        {!! $errors->first('date_of_leave', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                                        {!! Form::label('Service Type', trans('Select Role ')) !!}
                                        <select class="form-control" name="roles">
                                            <option value="">Select Role</option>
                                            <?php if(isset($role)){?>
                                                <?php foreach ($role as $roles){ ?>
                                                    <option value="{{$roles->id}}">{{$roles->name}}</option>
                                                <?php }?>
                                            <?php }?>
                                        </select>
                                        {!! $errors->first('service_dept', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
                                        {!! Form::label('reason', trans('Reason For Leaving ')) !!}
                                        {!! Form::textarea('reason', old('reason'), ['class' => 'form-control','rows' => 3, 'placeholder' => trans('Reason For Leaving')]) !!}
                                        {!! $errors->first('reason', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                                        {!! Form::label('comment', trans('comment')) !!}
                                        {!! Form::textarea('comment', old('comment'), ['class' => 'form-control','rows' => 3, 'placeholder' => trans('comment')]) !!}
                                        {!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-3 mt-3">
                                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                        {!! Form::label('status', trans('Status')) !!}
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-primary btn-rounded  btn-md form-check-label">
                                                {!! Form::radio('status', 1, ['class' => 'form-control', 'placeholder' => trans('user::users.form.status')]) !!}
                                                {!! $errors->first('status', '<span class="help-block">:message</span>') !!}Enable
                                            </label>
                                            &nbsp;&nbsp;
                                            <label class="btn btn-primary btn-rounded  btn-md form-check-label">
                                                {!! Form::radio('status', 0, ['class' => 'form-control', 'placeholder' => trans('user::users.form.status')]) !!}
                                                {!! $errors->first('status', '<span class="help-block">:message</span>') !!}Disable
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                            <div class="tab-pane p-3" id="messages1" role="tabpanel">

                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
                                                {!! Form::label('bank_name', trans('Bank Name')) !!}
                                                {!! Form::text('bank_name', old('bank_name',$user->bank_name), ['class' => 'form-control','onkeypress' => 'return isNumber(event);', 'placeholder' => trans('Bank Name')]) !!}
                                                {!! $errors->first('bank_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('branch_name') ? ' has-error' : '' }}">
                                                {!! Form::label('branch_name', trans('Branch Name')) !!}
                                                {!! Form::text('branch_name', old('branch_name',$user->branch_name), ['class' => 'form-control','onkeypress' => 'return isNumber(event);', 'placeholder' => trans('Branch Name')]) !!}
                                                {!! $errors->first('bank_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('acc_num') ? ' has-error' : '' }}">
                                                {!! Form::label('acc_num', trans('Account Number')) !!}
                                                {!! Form::text('acc_num', old('acc_num',$user->acc_num), ['class' => 'form-control','onkeypress' => 'return isNumber(event);', 'placeholder' => trans('Account Number')]) !!}
                                                {!! $errors->first('acc_num', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('ifsc_num') ? ' has-error' : '' }}">
                                                {!! Form::label('ifsc_num', trans('IFSC Code')) !!}
                                                {!! Form::text('ifsc_num', old('ifsc_num',$user->ifsc_num), ['class' => 'form-control','style'=>'text-transform:uppercase;' ,'placeholder' => trans('IFSC Number ')]) !!}
                                                {!! $errors->first('ifsc_num', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('esi_num') ? ' has-error' : '' }}">
                                                {!! Form::label('esi_num', trans('ESI Number')) !!}
                                                {!! Form::text('esi_num', old('esi_num',$user->esi_num), ['class' => 'form-control', 'placeholder' => trans('ESI Number ')]) !!}
                                                {!! $errors->first('esi_num', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('pf_num') ? ' has-error' : '' }}">
                                                {!! Form::label('PF Number', trans('PF Number')) !!}
                                                {!! Form::text('pf_num', old('pf_num',$user->pf_num), ['class' => 'form-control', 'placeholder' => trans('PF Number')]) !!}
                                                {!! $errors->first('pf_num', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('uan_no') ? ' has-error' : '' }}">
                                                {!! Form::label('UAN', trans('UAN')) !!}
                                                {!! Form::text('uan_no', old('uan_no',$user->uan_no), ['class' => 'form-control','onkeypress' => 'return isNumber(event);', 'placeholder' => trans('UAN Number ')]) !!}
                                                {!! $errors->first('uan_no', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('nominee_name') ? ' has-error' : '' }}">
                                                {!! Form::label('nominee_name', trans('Nominee Name')) !!}
                                                {!! Form::text('nominee_name', old('nominee_name',$user->nominee_name), ['class' => 'form-control', 'placeholder' => trans('Nominee Name')]) !!}
                                                {!! $errors->first('nominee_name', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('relationship') ? ' has-error' : '' }}">
                                                {!! Form::label('relationship', trans('Relationship')) !!}
                                                <select class="form-control" name="relatioship">
                                                    <option value="">Select Relationship</option>
                                                    <option value="friend">Friend</option>
                                                    <option value="relative">Relative</option>
                                                    <option value="father">Father</option>
                                                    <option value="mother">Mother</option>
                                                    <option value="husband">Husband</option>
                                                    <option value="wife">Wife</option>
                                                    <option value="son">Son</option>
                                                    <option value="daughter">Daughter</option>
                                                    <option value="brother">Brother</option>
                                                    <option value="sister">Sister</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane p-3" id="settings1" role="tabpanel">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <h5>REFERENCE 1</h5>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('refered_by') ? ' has-error' : '' }}">
                                                {!! Form::label('refered_by', trans('Name')) !!}
                                                {!! Form::text('refered_by', old('refered_by',$user->refered_by), ['class' => 'form-control', 'placeholder' => trans('Nominee Name')]) !!}
                                                {!! $errors->first('refered_by', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('refered_phone') ? ' has-error' : '' }}">
                                                {!! Form::label('refered_phone  ', trans('Reference Phone No.')) !!}
                                                {!! Form::text('refered_phone', old('refered_phone',$user->refered_phone), ['class' => 'form-control','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','pattern'=>'[23456789][0-9]{9}','maxlength'=>'10','placeholder' => trans('Reference Number ')]) !!}
                                                {!! $errors->first('refered_phone   ', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('ref1_rel') ? ' has-error' : '' }}">
                                                {!! Form::label('relationship', trans('Relationship')) !!}
                                                <select class="form-control" name="ref1_rel">
                                                    <option value="">Select Relationship</option>
                                                    <option value="friend">Friend</option>
                                                    <option value="relative">Relative</option>
                                                    <option value="father">Father</option>
                                                    <option value="mother">Mother</option>
                                                    <option value="husband">Husband</option>
                                                    <option value="wife">Wife</option>
                                                    <option value="son">Son</option>
                                                    <option value="daughter">Daughter</option>
                                                    <option value="brother">Brother</option>
                                                    <option value="sister">Sister</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <h5>REFERENCE 2</h5>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group{{ $errors->has('refered2_by') ? ' has-error' : '' }}">
                                                {!! Form::label('refered2_by', trans('Name')) !!}
                                                {!! Form::text('refered2_by', old('refered2_by',$user->refered2_by), ['class' => 'form-control', 'placeholder' => trans('Nominee Name')]) !!}
                                                {!! $errors->first('refered2_by', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('refered2_phone') ? ' has-error' : '' }}">
                                                {!! Form::label('refered2_phone', trans('Reference Phone No.')) !!}
                                                {!! Form::text('refered2_phone', old('refered2_phone',$user->refered2_phone), ['class' => 'form-control','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','pattern'=>'[23456789][0-9]{9}','maxlength'=>'10','placeholder' => trans('Reference Number ')]) !!}
                                                {!! $errors->first('refered2_phone', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('ref2_rel') ? ' has-error' : '' }}">
                                                {!! Form::label('relationship', trans('Relationship')) !!}
                                                <select class="form-control" name="ref2_rel">
                                                    <option value="">Select Relationship</option>
                                                    <option value="friend">Friend</option>
                                                    <option value="relative">Relative</option>
                                                    <option value="father">Father</option>
                                                    <option value="mother">Mother</option>
                                                    <option value="husband">Husband</option>
                                                    <option value="wife">Wife</option>
                                                    <option value="son">Son</option>
                                                    <option value="daughter">Daughter</option>
                                                    <option value="brother">Brother</option>
                                                    <option value="sister">Sister</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane p-3" id="settings2" role="tabpanel">
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('prophoto') ? ' has-error' : '' }}">
                                                {!! Form::label('prophoto', trans('Photo')) !!}
                                                <div><img width ="100px" id="img1"></div>
                                                {!! Form::file('prophoto', ['class' => 'form-control','onchange'=>'readURL(this)','placeholder' => trans('prophoto')]) !!}
                                                {!! $errors->first('prophoto', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('referencedoc1_copy') ? ' has-error' : '' }}">
                                                {!! Form::label('referencedoc1_copy', trans('Resume Upload')) !!}
                                                <div><img width ="100px" id="img2"></div>
                                                {!! Form::file('referencedoc1_copy', ['class' => 'form-control','onchange'=>'readURL1(this)', 'placeholder' => trans('user::users.form.referencedoc1_copy')]) !!}
                                                {!! $errors->first('referencedoc1_copy', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('idproofdoc_copy') ? ' has-error' : '' }}">
                                                {!! Form::label('idproofdoc_copy', trans('ID Proof ')) !!}
                                                <div><img width ="100px" id="img3"></div>
                                                {!! Form::file('idproofdoc_copy', ['class' => 'form-control','onchange'=>'readURL2(this)','placeholder' => trans('user::users.form.idproofdoc_copy')]) !!}
                                                {!! $errors->first('idproofdoc_copy', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group{{ $errors->has('referencedoc2_copy') ? ' has-error' : '' }}">
                                                {!! Form::label('referencedoc2_copy', trans('Address Proof ')) !!}
                                                <div><img width ="100px" id="img4"></div>
                                                {!! Form::file('referencedoc2_copy', ['class' => 'form-control','onchange'=>'readURL3(this)','placeholder' => trans('user::users.form.referencedoc1_copy')]) !!}
                                                {!! $errors->first('referencedoc2_copy', '<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="">
                                <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>

                                <a href="{{route('staff.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                            </div>

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>






        </div> <!-- container-fluid -->
@endsection
