@extends('layouts.master')

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Location</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                       <li class="breadcrumb-item active">Add Location</li>
                                    </ol>


                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                {!! Form::open(array('route' => 'location.store','method'=>'POST','files' => true)) !!}

                                <div class="card m-b-20">
                                    <div class="card-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('locationname') ? ' has-error' : '' }}">
                                        {!! Form::label('locationname', 'Location Name') !!}
                                        {!! Form::text('locationname', old('locationname'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('locationname', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                                        {!! Form::label('district', 'District') !!}
                                        {!! Form::text('district', old('district'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('district', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-3">
              <div class="form-group">
                {!! Form::label('State', 'State') !!}<br>
                  <select required class="form-control" name="state">
               <option value="">Select State</option>
               <?php if(isset($place)){?>
                                                <?php foreach ($place as $places){ ?>
                                                    <option value="{{$places->name}}">{{$places->name}}</option>
                                                <?php }?>
                                            <?php }?>
             </select>
                </div>
            </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        {!! Form::label('description', 'Description') !!}
                                        {!! Form::textarea('description', old('description'), ['class' => 'form-control']) !!}
                                        {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                </div>

                                          <div class="">
                                        <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                          <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
                          </div>






                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

         {!! Form::close() !!}

                    </div> <!-- container-fluid -->
@endsection
