@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="page-title-box">
        <h4 class="page-title">Transport Report</h4>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{route('transport.index')}}">Transport</a></li>
          <li class="breadcrumb-item active">Add</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- end row -->

  <div class="row">
    <div class="col-12">
      {!! Form::open(array('route' => 'transport.store','method'=>'POST','file' =>'true')) !!}

      <div class="card m-b-20">
        <div class="card-body">

          <div class="box-body">
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  {!! Form::label('Type', 'Type') !!}<br>
                  <select required class="form-control" name="type">
               <option value="">Select Type</option>
               <option value="Collected from Vehicle1">Collected from Vehicle1</option><option value="Dispatched">Dispatched</option>
               <option value="Moving/Merging to the Vehicle">Moving/Merging to the Vehicle</option>
               <option value="Collected from Vehicle2">Collected from Vehicle2</option>
               
             </select>
                </div>
              </div>

            <div class="col-sm-4">
              <div class="form-group">
                {!! Form::label('Vehicle', 'Vehicle') !!}<br>
                  <select required class="form-control" name="vehicle">
               <option value="">Select Type</option>
               <?php if(isset($vehical)){?>
                                                <?php foreach ($vehical as $vehicals){ ?>
                                                    <option value="{{$vehicals->vehical_type}}">{{$vehicals->vehical_type}}</option>
                                                <?php }?>
                                            <?php }?>
             </select>
                </div>
            </div>



            <div class="col-sm-4">
             <div class="form-group {{ $errors->has('id') ? ' has-error' : '' }}">
              {!! Form::Label('Total Animal', 'Total Animal:') !!}
              <input  name="total_animal" type="number" id="total_animal" class="form-control">
             {!! $errors->first('id', '<span class="help-block">:message</span>') !!}
           </div>
         </div>

            <div class="col-sm-4">
              <div class="form-group">
                {!! Form::label('Start Place', 'Start Place') !!}<br>
                  <select required class="form-control" name="start_place">
               <option value="">Select Place</option>
               <?php if(isset($place)){?>
                                                <?php foreach ($place as $places){ ?>
                                                    <option value="{{$places->name}}">{{$places->name}}</option>
                                                <?php }?>
                                            <?php }?>
             </select>
                </div>
            </div>

            <div class="col-sm-4">
              <div class="form-group">
                {!! Form::label('Destination Place', 'Destination Place') !!}<br>
                  <select required class="form-control" name="end_place">
               <option value="">Select Place</option>
               <?php if(isset($place)){?>
                                                <?php foreach ($place as $places){ ?>
                                                    <option value="{{$places->name}}">{{$places->name}}</option>
                                                <?php }?>
                                            <?php }?>
             </select>
                </div>
            </div>

         <div class="col-sm-4">
          <div class="form-group">
                {!! Form::label('Start Time', 'Start Time') !!}<br>
                  <input  name="start_time" type="time" id="start_time" class="form-control">
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
                {!! Form::label('End Time', 'End Time') !!}<br>
                  <input  name="end_time" type="time" id="end_time" class="form-control">
          </div>
        </div>

        <div class="col-md-4">
  <div class="form-group">
                {!! Form::label('Animal Health', 'Animal Health') !!}
                  <input name="health_certificate" type="file" id="img" class="form-control">
      </div>
</div>

<div class="col-md-4">
  <div class="form-group">
                {!! Form::label('Transport', 'Transport') !!}
                  <input name="transport_certificate" type="file" id="img" class="form-control">
      </div>
</div>

<div class="col-sm-12">
  <div class="form-group">
    {!! Form::label('remarks', trans('Remarks')) !!}
    {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control','rows' =>3,'required', 'placeholder' => trans('Remarks')]) !!}
    {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
  </div>
</div>

</div>

<div class="">
  <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

  <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
</div>

</div>
</div> <!-- end col -->
</div> <!-- end row -->

{!! Form::close() !!}

</div> <!-- container-fluid -->
</div>
</div>

@endsection
@section('js_after')
<script>
  <?php if(isset($request->category_id)){ ?>
    $( document ).ready(function() {

      var top_cat = "{{$request->category_id}}";
      ajax(top_cat);
    });
  <?php }?>
  $('#topcat').on('change',function(){
    var top_cat = $(this).val();
    ajax(top_cat);

  });
  function ajax(top_cat) {

    url = '<?= route('animalstock.subcat') ?>';
    $.get(url, {"top_cat": top_cat}, categorylist, 'json');

    function categorylist(d, s) {

      var elSel = document.getElementById('category');
      var i;
      for (i = elSel.length; i >= 0; i--) {
        elSel.remove(i);
      }
      if (d.length > 0) {
        $("#category").append('<option value="">Select Sub Category</option>');
      } else {
        $("#category").append('<option value="">No Sub Category</option>');
      }

      $.each(d,
        function () {

          var str = this.id;
          var name = this.subcategory_name;

          var option = new Option(str, name);
                        // var dropdownList = $("#ValidCityName")[0];
                        var dropdownList = document.getElementById("category");
                        //dropdownList.add(option, null);
                        var option = document.createElement("option");

                        if (typeof name == 'undefined') {
                          name = '';
                        }
                        option.appendChild(document.createTextNode(name));
                        // or alternately
                        //option.text = data[i].name;
                        option.value = str;


                        dropdownList.appendChild(option);

                        if (name == 'error') {

                        } else {


                        }
                      }
                      );


    }
  }


  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

    });
  });

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
          }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
  });
</script>

@endsection
