@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Category</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Add Category</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            {!! Form::open(array('route' => 'category.store','method'=>'POST','files' => true)) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::Label('Type', 'Type:') !!}
                                    <select class="form-control" name="type">
                                        @foreach($topcategory as $key => $categorys)
                                        <option value="{{$categorys->id}}">{{$categorys->topcategory_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('category_name', trans('Category Name')) !!}
                                    <input type="text" name="category_name" placeholder="Enter Name" class="form-control">
                                </div>
                            </div>


                            <div class="col-sm-4">
                                   <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                       {!! Form::label('image', trans('Image ')) !!}
                                       {!! Form::file('images', ['class' => 'form-control','onchange'=>'readURL2(this)','placeholder' => trans('Image')]) !!}
                                       {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                                   </div>
                                   </div>


                            <div class="col-md-2">
                                 <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('status')) !!}<br>
                           <label class="radio-inline"> {!! Form::radio('status', '1', true ) !!} Yes </label> <label class="radio-inline"> {!! Form::radio('status', '0' ) !!} No </label>

                                </div>
                            </div>

                             <div class="col-md-2">
                                 <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                    {!! Form::label('gender', trans('gender')) !!}<br>
                           <label class="radio-inline"> {!! Form::radio('gender', 'Male', true ) !!} Male </label> <label class="radio-inline"> {!! Form::radio('gender', 'Female' ) !!} Female </label>

                                </div>
                            </div>
                          </div>
                        


                        <div class="">
                            <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                            <a href="{{route('category.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                        </div>   
                    </div>
                 </div> 
            </div> 
            {!! Form::close() !!}
        </div>
    </div><!--end row-->
</div> <!-- container-fluid -->
@endsection
@section('js_after')
<script>
  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

  });
});

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
        }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
});
</script>

@endsection