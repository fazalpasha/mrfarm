@extends('layouts.master')
@section('content')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title"> Feed Description</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Add Feed Description</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            {!! Form::open(array('route' =>'feedinfo.store','method'=>'POST')) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    {!! Form::Label('Category Name','Category Name:') !!}
                                    <select required class="form-control" id="topcat" name="category_id">
                                        <option value="">Select Category</option>
                                        @foreach($category as $key => $categorys)
                                        <option <?php if(isset($request->category_id)){ ?> <?php if($request->category_id == $categorys->id) {?> selected <?php }?> <?php }?>value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-3">
                                <div class="form-group">
                                    {!! Form::Label('Sub Category Name', 'Sub Category Name:') !!}
                                    <select class="form-control" id="category" name="subcategory_id">
                                    </select>
                                </div>
                            </div>

              <!--               <div class="col-sm-3">
                                <div class="form-group">
                                    {!! Form::Label('Cell', 'Cell:') !!}
                                    <select required class="form-control select2" name="cell_id">
                                      <option value="">Select Cell</option>
                                      @foreach($cell as $key => $cells)
                                      <option value="{{$cells->id}}">{{$cells->cell_name}}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div> -->

  <!--                         <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }}">
                                {!! Form::label('unit', trans('Unit')) !!}
                                {!! Form::text('unit', old('unit'), ['class' => 'form-control', 'placeholder' => trans('Unit')]) !!}
                                {!! $errors->first('unit', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div> -->

                        <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('Label') ? ' has-error' : '' }}">
                                {!! Form::label('Label', trans('Label')) !!}
                                {!! Form::text('title', old('Label'), ['class' => 'form-control', 'placeholder' => trans('Label')]) !!}
                                {!! $errors->first('label', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group{{ $errors->has('quality') ? ' has-error' : '' }}">
                               {!! Form::label('Description', trans('Description')) !!}
                               <textarea id="messageArea" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message.."></textarea>
                            </div>
                        </div>

         <!--                <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::Label('Added By', 'Added By:') !!}
                                <select class="form-control select2" name="added_by">
                                    @foreach($add as $key => $addedby)
                                 <option value="{{$addedby->id}}">{{$addedby->username}}</option>
                                 @endforeach
                             </select>
                         </div>
                     </div> -->
                 </div>

                 <div class="">
                     <button type="submit" class="btn btn-primary btn-lg  text-white final_submit">Create</button>

                     <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
                 </div>         

             </div>
         </div> <!-- end col -->
     </div> <!-- end row -->

     {!! Form::close() !!}

 </div> 
</div>
</div>
@endsection

@section('js_after')
<script type="text/javascript">
         CKEDITOR.replace( 'messageArea',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          })
</script> 
<script>
  <?php if(isset($request->category_id)){ ?>
    $( document ).ready(function() {
      var top_cat = "{{$request->category_id}}";
      ajax(top_cat);
  });
<?php }?>
$('#topcat').on('change',function(){
    var top_cat = $(this).val();
    ajax(top_cat);

});
function ajax(top_cat) {

    url = '<?= route('production.subcat') ?>';
    $.get(url, {"top_cat": top_cat}, categorylist, 'json');

    function categorylist(d, s) {

      var elSel = document.getElementById('category');
      var i;
      for (i = elSel.length; i >= 0; i--) {
        elSel.remove(i);
    }
    if (d.length > 0) {
        $("#category").append('<option value="">Select Sub Category</option>');
    } else {
        $("#category").append('<option value="">No Sub Category</option>');
    }

    $.each(d,
        function () {
          var str = this.id;
          var name = this.subcategory_name;
          var option = new Option(str, name);
          var dropdownList = document.getElementById("category");
          var option = document.createElement("option");
          if (typeof name == 'undefined') {
            name = '';
        }
        option.appendChild(document.createTextNode(name));
        option.value = str;
        dropdownList.appendChild(option);
        if (name == 'error') {
        } else {
        }
    }
    );
}
}

</script>

@endsection