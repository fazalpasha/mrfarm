@extends('layouts.master')

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Animal Stock</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('stock.index')}}">Stock Management</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('animalstock.index')}}">Animal Stock</a></li>
                                        <li class="breadcrumb-item active">Edit Stock</li>
                                    </ol>
            
                                   
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                {!! Form::open(['route' => ['animalstock.update', $animalstock->id], 'method' => 'put']) !!}

                                <div class="card m-b-20">
                                    <div class="card-body">
        
                        <div class="box-body">
                      <div class="row">
              <div class="col-sm-4">
                <div class="form-group" {{ $errors->has('category_id') ? ' has-error' : '' }}>
                  {!! Form::Label('Category Name','Category Name:') !!}
                  <select required class="form-control" id="topcat" name="category_id">
                   <option value="">Select Category</option>
                   @foreach($category as $key => $categorys)
                   <option <?php if(isset($animalstock->category_id)){ ?> <?php if($animalstock->category_id == $categorys->id) {?> selected <?php }?> <?php }?>value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                   @endforeach
                 </select>
                 {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
               </div>
             </div>

             <div class="col-sm-4">
              <div class="form-group {{ $errors->has('subcategory_id') ? ' has-error' : '' }}" >
                {!! Form::Label('Sub Category Name','Sub Category Name:') !!}
                <select required class="form-control" id="category" name="subcategory_id">
                </select>
                {!! $errors->first('subcategory_id', '<span class="help-block">:message</span>') !!}
              </div>
            </div>



           <div class="col-sm-4">
                                <div class="form-group">
                                     {!! Form::Label('Qrcode', 'Qrcode:') !!}
                                    <select class="form-control" name="qrcode">
                                         <?php if (isset($qrcode)) { ?>
                                <?php foreach($qrcode as $qr) { ?>
                                        <option <?php if($qr->id == $animalstock->qrcode){ ?> selected <?php } ?> value="{{$qr->id}}">{{$qr->prefix}}{{$qr->qr_code}}</option>
                                        <?php }?>
                                        <?php }?>
                                    </select>
                                     {!! $errors->first("qrcode", '<span class="help-block">:message</span>') !!}
                              </div>
                          </div>
             <div class="col-sm-4">
                                <div class="form-group">
                                     {!! Form::Label('Cell Name', 'Cell Name:') !!}
                                    <select class="form-control" name="cell_id">
                                         <?php if (isset($cell)) { ?>
                                <?php foreach($cell as $cells) { ?>
                                        <option <?php if($cells->id == $animalstock->cell_name){ ?> selected <?php } ?> value="{{$cells->id}}">{{$cells->cell_name}}</option>
                                        <?php }?>
                                        <?php }?>
                                    </select>
                                     {!! $errors->first("cell_name", '<span class="help-block">:message</span>') !!}
                              </div>
                          </div>

            <div class="col-sm-4">
              <div class="form-group{{ $errors->has('total_weight') ? ' has-error' : '' }}">
                {!! Form::label('total_weight', trans('Weight')) !!}
                {!! Form::text('total_weight', old('total_weight',$animalstock->total_weight), ['class' => 'form-control','required', 'placeholder' => trans('Total Weight')]) !!}
                {!! $errors->first('total_weight', '<span class="help-block">:message</span>') !!}
              </div>
            </div>

            <div class="col-sm-4">
             <div class="form-group {{ $errors->has('vendor_id') ? ' has-error' : '' }}">
              {!! Form::Label('Vendor', 'Vendor:') !!}
              <select required class="form-control select2" name="vendor_id">
               <option value="">Select Vendor</option>
               @foreach($vendor as $key => $vendors)
               <option <?php if(isset($animalstock->vendor_id)){ ?> <?php if($animalstock->vendor_id == $vendors->id) {?> selected <?php }?> <?php }?>value="{{$vendors->id}}">#MRVE{{$vendors->id}}--{{$vendors->vendor_name}}--{{$vendors->phone}}--{{$vendors->city}}</option>
               @endforeach
             </select>
             {!! $errors->first('vendor_id', '<span class="help-block">:message</span>') !!}
           </div>
         </div>

         <div class="col-sm-4">
          <div class="form-group{{ $errors->has('billing_amount') ? ' has-error' : '' }}">
            {!! Form::label('billing_amount', trans('Billing Amount')) !!}
            {!! Form::text('billing_amount', old('billing_amount',$animalstock->billing_amount), ['class' => 'form-control','required', 'placeholder' => trans('Billing Amount')]) !!}
            {!! $errors->first('billing_amount', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group{{ $errors->has('receipt_no') ? ' has-error' : '' }}">
            {!! Form::label('receipt_no', trans('Receipt No')) !!}
            <?php if(isset($animalstock->receipt_no)){ ?>
              {!! Form::text('receipt_no', old('receipt_no',$animalstock->receipt_no), ['class' => 'form-control','required', 'placeholder' => trans('Receipt No')]) !!}
            <?php }else{?>
              {!! Form::text('receipt_no', old('receipt_no',$animalstock->receipt_no), ['class' => 'form-control','required', 'placeholder' => trans('Receipt No')]) !!}
            <?php }?>
            {!! $errors->first('receipt_no', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
            {!! Form::label('date', trans('Date')) !!}
            {!! Form::date('date', old('date',date('Y-m-d')), ['class' => 'form-control','required', 'placeholder' => trans('Date')]) !!}
            {!! $errors->first('date', '<span class="help-block">:message</span>') !!}
          </div>
        </div>

        <div class="col-md-2">
         <div class="form-group{{ $errors->has('health_check') ? ' has-error' : '' }}">
          {!! Form::label('health_check', trans('Health Check')) !!}<br>
          <input  name="health_check" type="checkbox" id="health_check" switch="bool">
          <label for="health_check" data-on-label="Yes"
          data-off-label="No"></label>
        </div>
      </div>

      <div class="col-md-2">
       <div class="form-group{{ $errors->has('physically_cahllenge') ? ' has-error' : '' }}">
        {!! Form::label('physically_cahllenge', trans('Physically Challenged')) !!}<br>
        <input  name="physically_cahllenge" type="checkbox" id="physically_cahllenge" switch="bool">
        <label for="physically_cahllenge" data-on-label="Yes"
        data-off-label="No"></label>
      </div>
    </div>

    <div class="col-md-2">
     <div class="form-group{{ $errors->has('any_allergy') ? ' has-error' : '' }}">
      {!! Form::label('any_allergy', trans('Any Allergy')) !!}<br>
      <input  name="any_allergy" type="checkbox" id="any_allergy" switch="bool" >
      <label for="any_allergy" data-on-label="Yes"
      data-off-label="No"></label>
    </div>
  </div>

  <div class="col-md-2">
   <div class="form-group{{ $errors->has('pregnent') ? ' has-error' : '' }}">
    {!! Form::label('pregnent', trans('Pregnent')) !!}<br>
    <input name="pregnent" type="checkbox" id="pregnent" switch="bool">
    <label for="pregnent" data-on-label="Yes"
    data-off-label="No"></label>

  </div>
</div>

<div class="col-md-2">
 <div class="form-group{{ $errors->has('verified') ? ' has-error' : '' }}">
  {!! Form::label('verified', trans('Verified')) !!}<br>
  <input  name="verified" type="checkbox" id="verified" switch="bool">
  <label for="verified" data-on-label="Yes"
  data-off-label="No"></label>
</div>
</div>
<div class="col-md-2">
  <div class="form-group{{ $errors->has('repeat') ? ' has-error' : '' }}">
    {!! Form::label('repeat', trans('Do You Want Repeat Entry')) !!}<br>
    <input <?php if(isset($request->repeat)){ ?> <?php if($request->repeat == 1) {?> checked <?php }?> <?php }?> name="repeat" type="checkbox" id="repeat" switch="bool">
    <label for="repeat" data-on-label="Yes"
    data-off-label="No"></label>
  </div>
</div>

<div class="col-sm-12">
  <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
    {!! Form::label('remarks', trans('Remarks')) !!}
    {!! Form::textarea('remarks', old('remarks',$animalstock->remarks), ['class' => 'form-control','rows' =>3,'required', 'placeholder' => trans('Remarks')]) !!}
    {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
  </div>
</div>

</div>

                            
                                          <div class="">
                                        <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>

                          <a href="{{route('animalstock.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                          </div>   
                                        
                                        
                                       
                                       
                                       
                                       
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

         {!! Form::close() !!}

                    </div> <!-- container-fluid -->
@endsection
@section('js_after')
<script>
  <?php if(isset($animalstock->category_id)){ ?>
    $( document ).ready(function() {

      var top_cat = "{{$animalstock->category_id}}";
      ajax(top_cat);
    });
  <?php }?>
  $('#topcat').on('change',function(){
    var top_cat = $(this).val();
    ajax(top_cat);

  });
  function ajax(top_cat) {

    url = '<?= route('animalstock.subcat') ?>';
    $.get(url, {"top_cat": top_cat}, categorylist, 'json');

    function categorylist(d, s) {

      var elSel = document.getElementById('category');
      var i;
      for (i = elSel.length; i >= 0; i--) {
        elSel.remove(i);
      }
      if (d.length > 0) {
        $("#category").append('<option value="">Select Sub Category</option>');
      } else {
        $("#category").append('<option value="">No Sub Category</option>');
      }

      $.each(d,
        function () {
          var str = this.id;
          var name = this.subcategory_name;
          var option = new Option(str, name);
          var dropdownList = document.getElementById("category");
          var option = document.createElement("option");
          if (typeof name == 'undefined') {
            name = '';
        }
        option.appendChild(document.createTextNode(name));
        option.value = str;
        var subcat = "{{$animalstock->subcategory_id}}";
        if( subcat == str){
            option.setAttribute("selected","selected");
        }
        dropdownList.appendChild(option);
        if (name == 'error') {
        } else {


                        }
                      }
                      );


    }
  }


  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

    });
  });

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
          }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
  });
</script>

@endsection