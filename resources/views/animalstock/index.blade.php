@extends('layouts.master')

@section('css')
        <!-- DataTables -->
        <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">

                                    <h4 class="page-title">Animal Stock</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('stock.index')}}">Stock Management</a></li>
                                         <li class="breadcrumb-item">Animal Stock</li>


                                    </ol>
                                <div class="state-information d-none d-sm-block">
                     <div class="btn-group pull-right">
                    <a href="{{ route('animalstock.create') }}" class="btn btn-primary btn-lg waves-effect waves-light float-left" >
                        Create
                    </a>
                </div>
               </div>

                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-20">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Animal</h4>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Category</label>
                                                    <select class="form-control" id="filter_category">
                                                        <option value="">Select Category</option> @foreach($category as $key => $categorys)
                                                        <option <?php if(isset($request->category_id)){?> <?php if($request->category_id == $categorys->id) {?> selected <?php }?> <?php }?>value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                                                    @endforeach
                                                     </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Cell</label>
                                                <select class="form-control" id="filter_cell">
                                                        <option value="">Select Cell</option>
                                                        @foreach($cell as $key => $cells)
                                                        <option <?php if(isset($request->cell_number)){?> <?php if($request->cell_number == $cells->id) {?> selected <?php }?> <?php }?>value="{{$cells->id}}">{{$cells->cell_name}}</option>
                                                    @endforeach
                                                     </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Date From </label>
                                                    <input id="from" type="date" name="data_from" class="form-control">
                                                </div>
                                            </div> 
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Date to </label>
                                                    <input type="date" id="to" name="data_from" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label></label><br>
                                                    <button id="filter" class="btn btn-primary btn-lg" type="button">Fliter</button>
                                                 </div>
                                            </div>
                                        </div>

                                        <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">

                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Category Name</th>
                                                <th>Subcategory Name</th>
                                                <th>Qrcode</th>
                                                <th>Cell Number</th>
                                                <th>Weight</th>
                                                <th>Vendor</th>
                                                <th>Billing Amount</th>
                                                <th>Date</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>


                                            <tbody>
                                            <?php if (isset($data)): ?>

                <?php foreach ($data as $animalstock): ?>
                     <tr>
                   
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>


                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                        <!-- end row -->



                    </div> <!-- container-fluid -->
@endsection

@section('script')
        <!-- Required datatable js -->
        <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

        <!-- Datatable init js -->


        <script>
            $(document).ready(function(){
                fill_datatable();

                function fill_datatable(filter_category = '',datefrom='',dateto='',filter_cell='')
                {
                    var Table = $('#datatable').DataTable({
                        lengthChange: false,
                        buttons: ['copy', 'excel', 'pdf', 'colvis'],
                        "processing" : true,
                        "serverSide" : true,
                         "autoWidth": true,
                        "ajax" : {
                            url:"{{route('animalstock.filter')}}",
                            type:"get",
                            data:{
                                filter_category:filter_category,filter_cell,date_from:datefrom,date_to:dateto
                            }
                        },
                        "columns": [
                            { "data": "id"},
                            { "data": "category_name"},
                            { "data": "subcategory_name"},
                            { "data": "qrcode"},
                            { "data": "cell_name"},
                            { "data": "total_weight"},
                            { "data": "vendor_name"},
                            { "data": "billing_amount"},
                            { "data": "date"},
                            {
                                "render": function (data, type, JsonResultRow, meta) {
                                    return '<div class="btn-group"><a href="{{env('APP_URl')}}/dashboard/animalstock/edit/'+JsonResultRow.id+'" class="btn btn-primary btn-flat"><i class="ion-edit"></i></a><button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{env('APP_URl')}}/dashboard/animalstock/delete/'+JsonResultRow.id+'"><i class="fa fa-trash"></i></button></div>';
                                }
                            },

                        ]
                    });
                   
                }

                $('#filter').on('click',function () {
                    var filter_category = $('#filter_category :selected').val();
                    var filter_cell = $('#filter_cell :selected').val();
                    var fromd = $('#from').val();
                    var to = $('#to').val()
                    $('#datatable').DataTable().destroy();
                    fill_datatable("",filter_category,filter_cell,fromd,to);
                });
            });


        </script>

@endsection
