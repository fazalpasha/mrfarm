@extends('layouts.master')
@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Expense</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('expense.index')}}">Expense</a></li>
                </ol>
                <div class="state-information d-none d-sm-block">
                 <div class="btn-group pull-right">
                    <a href="{{ route('expense.create') }}" class="btn btn-primary btn-lg waves-effect waves-light float-left" >
                        Create
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card m-b-20">
            <div class="card-body">
                <h4 class="mt-0 header-title">Expense</h4>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Payment Method</label>
                            <select class="form-control" id="filter_category">
                                <option value="">Select Payment Method</option>
                                <option value="cash">cash</option>
                                <option value="cheque">Cheque</option>
                                <option value="card">Card</option>
                                <option value="online payment">online Payment</option>
                            </select></div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group"><label>Date From </label><input id="from" type="date" name="data_from"
                                                                                class="form-control"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group"><label>Date to </label><input type="date" id="to" name="data_from"
                                                                              class="form-control"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group"><label></label><br>
                            <button id="filter" class="btn btn-primary btn-lg" type="button">Fliter</button>
                        </div>
                    </div>
                </div>
                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Receipt No</th>
                            <th>Particular</th>
                            <th>Paid to</th>
                            <th>Paid By</th>
                            <th>Payment Method</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                            <th>Total</th>
                            <th>Date & Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                </tbody>
            </table>
                <div id="total_order"></div>
        </div>
    </div>
</div> <!-- end col -->
</div> <!-- end row -->

<!-- end row -->
</div> <!-- container-fluid -->

@endsection

@section('script')
<!-- Required datatable js -->
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(document).ready(function(){
        fill_datatable();

        function fill_datatable(filter_category = '',datefrom='',dateto='')
        {
            var Table = $('#datatable').DataTable({
                "processing" : true,
                "serverSide" : true,
                "ajax" : {
                    url:"{{route('expense.filter')}}",
                    type:"get",
                    data:{
                        filter_category:filter_category,date_from:datefrom,date_to:dateto
                    }
                },
                "columns": [
                    { "data": "id"},
                    { "data": "receipt_no"},
                    { "data": "particular"},
                    { "data": "vendor_name"},
                    { "data": "first_name"},
                    { "data": "payment_method"},
                    { "data": "quantity"},
                    { "data": "amount"},
                    { "data": "total"},
                    { "data": "date"},
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return '<div class="btn-group"><a href="{{env('APP_URl')}}/dashboard/expense/edit/'+JsonResultRow.id+'" class="btn btn-primary btn-flat"><i class="ion-edit"></i></a><button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{env('APP_URl')}}/dashboard/expense/delete/'+JsonResultRow.id+'"><i class="fa fa-trash"></i></button></div>';
                        }
                    },

                ],drawCallback:function(data)
                {
                    $('.grandtotal').remove();
                    if(data.json.grandtotal === null){
                        $('tbody').remove();
                        $('#datatable_paginate').prepend('<div class="grandtotal d-lg-inline-flex"><h4><small>Grand Total </small> ₹ 0</h4></div>');
                    }else {
                        $('#datatable_paginate').prepend('<div class="grandtotal d-lg-inline-flex"><h4><small>Grand Total </small> ₹ ' + data.json.grandtotal + '</h4></div>');
                    }
                }
            });
        }

        $('#filter').on('click',function () {
            var filter_category = $('#filter_category :selected').val();
            var fromd = $('#from').val();
            var to = $('#to').val()
            $('#datatable').DataTable().destroy();
            fill_datatable(filter_category,fromd,to);
        });
    });


</script>
@endsection
