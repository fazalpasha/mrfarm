@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Expense</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('expense.index')}}">Expense</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                {!! Form::open(['route' => ['expense.update', $expense->id], 'method' => 'put','file' =>'true']) !!}
                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}" >
                                        {!! Form::label('date', 'Date') !!}<br>
                                        <input  name="date" type="date" id="date" class="form-control" value="{{old('date',$expense->date)}}">
                                        {!! $errors->first('date', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('time') ? ' has-error' : '' }}" >
                                        {!! Form::label('time', 'Time') !!}<br>
                                        <input  name="time" type="time" id="time" class="form-control" value="{{old('time',$expense->time)}}">

                                        {!! $errors->first('time', '<span class="help-block">:message</span>') !!}
                                    </div>
                                    <div class="form-group">

                                    </div>
                                </div>



                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('added_by') ? ' has-error' : '' }}" > {!! Form::label('added_by', 'Spent By') !!}<br>
                                        <select class="form-control" name="added_by">
                                            <option value="">Select Employee</option>
                                            <?php if(isset($users)){ ?>
                                            <?php foreach ($users as $users){ ?>
                                            <option <?php if($expense->added_by == $users->id) {?> selected <?php }?>value="{{$users->id}}">{{$users->first_name}} {{$users->last_name}}</option>
                                            <?php }?>
                                            <?php }?>

                                        </select>

                                        {!! $errors->first('added_by', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('paid_to') ? ' has-error' : '' }}" >
                                        {!! Form::label('paid_to', 'Paid To') !!}<br>
                                        <input  name="paid_to" type="text" id="paid_to" class="form-control" value="{{old('paid_to',$expense->paid_to)}}">
                                        {!! $errors->first('paid_to', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('vendor_id') ? ' has-error' : '' }}" > {!! Form::label('vendor_id', 'Vendor Name') !!}<br><select class="form-control" name="vendor_id">
                                            <option value="">Select Vendor</option>
                                            <?php if(isset($vendor)){ ?>
                                            <?php foreach ($vendor as $vendors){ ?>
                                            <option <?php if($expense->vendor_id == $vendors->id) {?> selected <?php }?>value="{{$vendors->id}}">{{$vendors->vendor_name}} </option>
                                            <?php }?>
                                            <?php }?>

                                        </select>

                                        {!! $errors->first('vendor_id', '<span class="help-block">:message</span>') !!}
                                    </div>

                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('area') ? ' has-error' : '' }}" >
                                        {!! Form::label('area', 'Area') !!}<br>
                                        <input  name="area" type="text" id="area" class="form-control" value="{{old('area',$expense->area)}}">
                                        {!! $errors->first('area', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('receipt_no') ? ' has-error' : '' }}">
                                        {!! Form::label('receipt_no', 'Receipt No') !!}<br>
                                        <input  name="receipt_no" type="text" id="receipt" class="form-control" value="{{old('receipt_no',$expense->receipt_no)}}">
                                        {!! $errors->first('receipt_no', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('particular') ? ' has-error' : '' }}">
                                        {!! Form::label('particular', 'Particulars') !!}<br>
                                        <input  name="particular" type="text" id="particular" class="form-control" value="{{old('particular',$expense->particular)}}">
                                        {!! $errors->first('particular', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('payment_method') ? ' has-error' : '' }}">
                                        {!! Form::label('payment_method', 'Payment Method') !!}<br>
                                        <select class="form-control" name="payment_method">
                                            <option value="">Select Payment Mode</option>
                                            <option <?php if($expense->payment_method == "cash") {?> selected <?php }?> value="cash">cash</option>
                                            <option <?php if($expense->payment_method == "cheque") {?> selected <?php }?>value="cheque">Cheque</option>
                                            <option <?php if($expense->payment_method == "card") {?> selected <?php }?> value="card">Card</option>
                                            <option <?php if($expense->payment_method == "online payment") {?> selected <?php }?> value="online payment">online Payment</option>
                                        </select>
                                        {!! $errors->first('payment_method', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                        {!! Form::label('quantity', 'Quantity') !!}<br>
                                        <input  name="quantity" type="number" id="qty" class="form-control" value="{{old('quantity',$expense->quantity)}}">
                                        {!! $errors->first('quantity', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                                        {!! Form::label('amount', 'Amount') !!}<br>
                                        <input  name="amount" type="number" id="amt" class="form-control" value="{{old('amount',$expense->amount)}}">
                                        {!! $errors->first('amount', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('total') ? ' has-error' : '' }}">
                                        {!! Form::label('total', 'Total') !!}<br>
                                        <input  name="total" type="number" id="tot" class="form-control" value="{{old('total',$expense->total)}}">
                                        {!! $errors->first('total', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                        {!! Form::label('Bill Copy', 'Bill Copy') !!}
                                        <input name="image" type="file" id="img" class="form-control">
                                        {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('remarks') ? ' has-error' : '' }}">
                                        {!! Form::label('remarks', trans('Remarks')) !!}
                                        {!! Form::textarea('remarks', old('remarks',$expense->remarks), ['class' => 'form-control','rows' =>3,'required', 'placeholder' => trans('Remarks')]) !!}
                                        {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            </div>

                            <div class="">
                                <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>

                                <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
                            </div>

                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

                {!! Form::close() !!}

            </div> <!-- container-fluid -->
        </div>
    </div>

@endsection
@section('js_after')
@endsection
