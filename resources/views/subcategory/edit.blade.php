@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Sub Category</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('subcategory.index')}}">Sub Category</a></li>
                    <li class="breadcrumb-item active">Edit SubCategory</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            {!! Form::open(['route' => ['subcategory.update', $subcategory->id], 'method' => 'put']) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('subcategory_name') ? ' has-error' : '' }}">
                                    {!! Form::label('subcategory_name', trans('Sub Category')) !!}
                                    {!! Form::text('subcategory_name', old('subcategory_name',$subcategory->subcategory_name), ['class' => 'form-control', 'placeholder' => trans('Sub Category')]) !!}
                                    {!! $errors->first('subcategory_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::Label('Category Name', 'Category Name:') !!}
                                    <select class="form-control" name="id">
                                         <?php if (isset($category)) { ?>
                                <?php foreach($category as $key => $categorys) { ?>
                                        <option <?php if($categorys->id == $subcategory->category_name){ ?> selected <?php } ?> value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                                        <?php }?>
                                        <?php }?>
                                    </select>
                                     {!! $errors->first("category_name", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                           <div class="col-md-2">
                                 <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', trans('status')) !!}<br>
                           <label class="radio-inline"> {!! Form::radio('status', '1', true ) !!} Yes </label> <label class="radio-inline"> {!! Form::radio('status', '0' ) !!} No </label>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>
                            <a href="{{route('subcategory.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                        </div>   
                    </div>
                </div> 
            </div> 
            {!! Form::close() !!}
        </div>
    </div><!-- end row-->
</div> <!-- container-fluid -->
@endsection
@section('js_after')
<script>
  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

  });
});

$("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
        }).change();

$(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
});
</script>

@endsection