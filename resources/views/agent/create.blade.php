@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Sale agent</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('agent.index')}}">Sale agent</a></li>
                        <li class="breadcrumb-item active">Add</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                {!! Form::open(array('route' => 'agent.store','method'=>'POST','files' =>'true')) !!}

                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('agent_name') ? ' has-error' : '' }}">
                                        {!! Form::label('agent_name', 'Agent Name') !!}<br>
                                        <input  name="agent_name" type="text" id="agent_name" class="form-control">
                                        {!! $errors->first('agent_name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}" >
                                        {!! Form::label('phone', 'Phone') !!}<br>
                                        <input  name="phone" type="text" id="phone" class="form-control">
                                        {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                        {!! Form::label('address', 'Address') !!}<br>
                                        <input  name="address" type="text" id="address" class="form-control">
                                        {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                <div class="form-group">
                                    
                                    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                       {!! Form::label('photo', trans('Photo ')) !!}
                                       {!! Form::file('photo', ['class' => 'form-control','onchange'=>'readURL2(this)','placeholder' => trans('Photo')]) !!}
                                       {!! $errors->first('photo', '<span class="help-block">:message</span>') !!}
                                   </div>
                                   </div>

             
                            </div>

                            <div class="">
                                <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                                <a type="button" class="btn btn-danger btn-lg float-right text-white " href="{{route('batch.index')}}">Cancel</a>
                            </div>

                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

                {!! Form::close() !!}

            </div> <!-- container-fluid -->
        </div>
    </div>

@endsection
@section('js_after')
@endsection
