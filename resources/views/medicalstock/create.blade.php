@extends('layouts.master')

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Medical Stock</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('stock.index')}}">Stock Management</a></li>
                                         <li class="breadcrumb-item"><a href="{{route('medicalstock.index')}}">Medical Stock</a></li>

                                        <li class="breadcrumb-item active">Add Stock</li>
                                    </ol>
            
                                   
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                {!! Form::open(array('route' => 'medicalstock.store','method'=>'POST')) !!}

                                <div class="card m-b-20">
                                    <div class="card-body">
        
                        <div class="box-body">
                        <div class="row">
                            
                             <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::Label('Category Name', 'Category Name:') !!}
                             <select class="form-control" name="category_id">
                             @foreach($category as $key => $categorys)
                           <option value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                            @endforeach
                            </select>
                             </div>
                         </div>

                           <div class="col-sm-3">
                            <div class="form-group">
                                {!! Form::Label('Sub Category Name', 'Sub Category Name:') !!}
                             <select class="form-control" name="subcategory_id">
                             @foreach($subcategory as $key => $categorys)
                           <option value="{{$categorys->id}}">{{$categorys->subcategory_name}}</option>
                            @endforeach
                            </select>
                             </div>
                         </div>
                         
                           <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('qrcode') ? ' has-error' : '' }}">
                                    {!! Form::label('qrcode', trans('Qr Code')) !!}
                                    {!! Form::text('qrcode', old('qrcode'), ['class' => 'form-control', 'placeholder' => trans('Qr Code')]) !!}
                                    {!! $errors->first('qrcode', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                         
                            
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('item_name') ? ' has-error' : '' }}">
                                    {!! Form::label('item_name', trans('Item Name')) !!}
                                    {!! Form::text('item_name', old('item_name'), ['class' => 'form-control', 'placeholder' => trans('Item Name')]) !!}
                                    {!! $errors->first('item_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                             <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('vendor_name') ? ' has-error' : '' }}">
                                    {!! Form::label('vendor_name', trans('Vendor Name')) !!}
                                    {!! Form::text('vendor_name', old('vendor_name'), ['class' => 'form-control', 'placeholder' => trans('Vendor Name')]) !!}
                                    {!! $errors->first('vendor_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-3">
                           <div class="form-group">
                                {!! Form::Label('Vendor Id', 'Vendor Id:') !!}
                             <select class="form-control" name="vendor_id">
                             @foreach($vendor as $key => $vendors)
                           <option value="{{$vendors->id}}">{{$vendors->id}}</option>
                            @endforeach
                            </select>
                             </div>
                         </div>

                           <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('billing_amount') ? ' has-error' : '' }}">
                                    {!! Form::label('billing_amount', trans('Billing Amount')) !!}
                                    {!! Form::text('billing_amount', old('billing_amount'), ['class' => 'form-control', 'placeholder' => trans('Billing Amount')]) !!}
                                    {!! $errors->first('billing_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                           
                             <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('receipt_no') ? ' has-error' : '' }}">
                                    {!! Form::label('receipt_no', trans('Receipt No')) !!}
                                    {!! Form::text('receipt_no', old('receipt_no'), ['class' => 'form-control', 'placeholder' => trans('Receipt No')]) !!}
                                    {!! $errors->first('receipt_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                             <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('invoice_no') ? ' has-error' : '' }}">
                                    {!! Form::label('invoice_no', trans('Invoice No')) !!}
                                    {!! Form::text('invoice_no', old('invoice_no'), ['class' => 'form-control', 'placeholder' => trans('Invoice No')]) !!}
                                    {!! $errors->first('invoice_no', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                             <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('invoice_prefix') ? ' has-error' : '' }}">
                                    {!! Form::label('invoice_prefix', trans('Invoice Prefix')) !!}
                                    {!! Form::text('invoice_prefix', old('invoice_prefix'), ['class' => 'form-control', 'placeholder' => trans('Invoice Prefix')]) !!}
                                    {!! $errors->first('invoice_prefix', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                             <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('quality') ? ' has-error' : '' }}">
                                    {!! Form::label('quality', trans('Quality')) !!}
                                    {!! Form::text('quality', old('quality'), ['class' => 'form-control', 'placeholder' => trans('Quality')]) !!}
                                    {!! $errors->first('quality', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                             <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                    {!! Form::label('quantity', trans('Quantity')) !!}
                                    {!! Form::text('quantity', old('quantity'), ['class' => 'form-control', 'placeholder' => trans('Quantity')]) !!}
                                    {!! $errors->first('quantity', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                    {!! Form::label('date', trans('Date')) !!}
                                    {!! Form::date('date', old('date'), ['class' => 'form-control', 'placeholder' => trans('Date')]) !!}
                                    {!! $errors->first('date', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('received_by_name') ? ' has-error' : '' }}">
                                    {!! Form::label('received_by_name', trans('Received By Name')) !!}
                                    {!! Form::text('received_by_name', old('received_by_name'), ['class' => 'form-control', 'placeholder' => trans('Received By Name')]) !!}
                                    {!! $errors->first('received_by_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                                 <div class="col-sm-4">
                                <div class="form-group{{ $errors->has('document_upload') ? ' has-error' : '' }}">
                                    {!! Form::label('document_upload', trans('Document Upload')) !!}
                                    {!! Form::file('document_upload', ['class' => 'form-control', 'placeholder' => trans('Document Upload')]) !!}
                                    {!! $errors->first('document_upload', '<span class="help-block">:message</span>') !!}
                                </div>
                              </div>
                           
                           
                           
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                                    {!! Form::label('remarks', trans('Remarks')) !!}
                                    {!! Form::textarea('remarks', old('remarks'), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Remarks')]) !!}
                                    {!! $errors->first('remarks', '<span class="help-block">:message</span>') !!}
                                </div>
                           </div>
                            
                    </div>
                            
                                          <div class="">
                                        <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                          <a href="{{route('medicalstock.index')}}" class="btn btn-lg btn-danger float-right">Cancel</a>
                          </div>   
                                        
                                        
                                       
                                       
                                       
                                       
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

         {!! Form::close() !!}

                    </div> <!-- container-fluid -->
@endsection