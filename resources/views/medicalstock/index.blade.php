@extends('layouts.master')

@section('css')
        <!-- DataTables -->
        <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
            <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Medical Stock</h4>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="{{route('stock.index')}}">Stock Management</a></li>
                                         <li class="breadcrumb-item active">Medical Stock</li>
                                        
                                        
                                    </ol>
                                <div class="state-information d-none d-sm-block">
                     <div class="btn-group pull-right">
                    <a href="{{ route('stock.create') }}" class="btn btn-primary btn-lg waves-effect waves-light float-left" >
                        Create
                    </a>
                </div>
               </div>
                                   
                                </div>
                            </div>
                        </div><br>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-20">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">Stock</h4>
                                        

                                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Category Id</th>
                                                <th>Subcategory Id</th>
                                                <th>Qrcode</th>
                                                <th>Item Name</th>
                                                <th>Vendor Name</th>
                                                <th>Vendor Id</th>
                                                <th>Billing Amount</th>
                                                <th>Receipt No</th>
                                                <th>Invoice No</th>
                                                <th>Invoice Prefix</th>
                                                 <th>Quality</th>
                                                <th>Quantity</th>
                                                <th>Date</th>
                                                <th>Remarks</th>
                                                 <th>Received By Name</th>
                                                   <th>Document Upload</th>
                                                 <th>Action</th>
                                                
                                            </tr>
                                            </thead>


                                            <tbody>
                                            <?php if (isset($data)): ?>
                <?php foreach ($data as $foodstock): ?>
                <tr>
                    <td>{{$foodstock->id}}</td>
                    <td>{{$foodstock->category_id}}</td>
                     <td>{{$foodstock->subcategory_id}}</td>
                    <td>{{$foodstock->qrcode}}</td>
                    <td>{{$foodstock->item_name}}</td>
                    <td>{{$foodstock->vendor_name}}</td>
                    <td>{{$foodstock->vendor_id}}</td>
                    <td>{{$foodstock->billing_amount}}</td>
                    <td>{{$foodstock->receipt_no}}</td>
                    <td>{{$foodstock->invoice_no}}</td>
                     <td>{{$foodstock->invoice_prefix}}</td>             
                    <td>{{$foodstock->quality}}</td>
                    <td>{{$foodstock->quantity}}</td>
                     <td>{{$foodstock->date}}</td>
                      <td>{{$foodstock->remarks}}</td>
                    <td>{{$foodstock->received_by_name}}</td>
                     <td>{{$foodstock->document_upload}}</td>



                           <td> <div class="btn-group">
                                        <a href="{{ route('medicalstock.edit', [$foodstock->id]) }}" class="btn btn-primary btn-flat"><i class="ion-edit"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('medicalstock.delete', [$foodstock->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                          </td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                                          
                                           
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                        <!-- end row -->



                    </div> <!-- container-fluid -->
@endsection

@section('script')
        <!-- Required datatable js -->
        <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <!-- Buttons examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
        <!-- Responsive examples -->
        <script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

        <!-- Datatable init js -->
        <script src="{{ URL::asset('assets/pages/datatables.init.js')}}"></script>
@endsection