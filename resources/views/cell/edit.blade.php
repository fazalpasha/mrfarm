@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Cell</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Edit Cell</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-12">
            {!! Form::open(['route' => ['cell.update', $cell->id], 'method' => 'put']) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('cell_name') ? ' has-error' : '' }}">
                                    {!! Form::label('cell_name', trans('Cell Name')) !!}
                                    {!! Form::text('cell_name', old('cell_name',$cell->cell_name), ['class' => 'form-control', 'placeholder' => trans('Cell Name')]) !!}
                                    {!! $errors->first('cell_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('max_allowed') ? ' has-error' : '' }}">
                                    {!! Form::label('max_allowed', trans('Max Allowed')) !!}
                                    {!! Form::number('max_allowed', old('max_allowed',$cell->max_allowed), ['class' => 'form-control', 'placeholder' => trans('Max Allowed')]) !!}
                                    {!! $errors->first('max_allowed', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                    <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
                                    {!! Form::label('details', trans('Details')) !!}
                                    {!! Form::textarea('details', old('details',$cell->details), ['class' => 'form-control','rows' =>4, 'placeholder' => trans('Details')]) !!}
                                    {!! $errors->first('details', '<span class="help-block">:message</span>') !!}

                                </div>
                            </div>

                        </div>

                        <div class="">
                            <button type="submit" class="btn btn-primary btn-lg  text-white float-right">Update</button>

                           <!--  <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button> -->
                        </div>   
                    </div>
                </div> 
            </div> 
            {!! Form::close() !!}
        </div> 
    </div>
</div>
@endsection