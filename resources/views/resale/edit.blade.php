@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Resales</h4>
                <ol class="breadcrumb">
                   <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                   <li class="breadcrumb-item"><a href="{{route('resale.index')}}">Resales</a></li>
                   <li class="breadcrumb-item active">Resales</li>
               </ol>
           </div>
       </div>
   </div>

    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
  <body style="background-color: white;">                      
<table style="border:1px;" width="100%">
    <span class="brand-text font-weight-light" style="font-size: 24px;line-height: 2.6;padding-left: 10px;font-weight: bold !important;color: #0071a9;"><left><img src="{{asset('assets/images/logo-mr.png')}}"  class="brand-image"  width="60">MR Farm</left></span>
    <tr></tr>
        <tr>
            <td><b>Billed To</b><br>{{$sales->vendor_name}}<br>{{$sales->vendor_address}}<br>{{$sales->vendor_phone}}</td><td><b>Date of Issue</b><br>{{$sales->created_at}}</td><td><b>Amount</b><br>{{$sales->total_price}}</td><td></td>
         </tr>
         </table><br>
         <table style="border:1px;" width="100%">
        <tr>
            <th><b>ID</b></th><th><b>Price</b></th><th><b>GST</b></th><th><b>Total</b></th></tr>
            <tr>
            <td>{{$sales->id}}</td><td>{{$sales->sub_price}}</td>
            <td>{{$sales->gst_price }}</td>
            <td>{{$sales->total_price}}</td>
         </tr>
         </table>
                        <div class="">
                            <button type="submit" onclick="window.print()" class="btn btn-primary btn-lg  text-white">Print</button>
                            
                        </div>   
                    </div>
                </div> 
            </div>
            
        </div>
    </div><!-- end row-->
</div>
</body> <!-- container-fluid -->
@endsection
@section('js_after')
<script>
  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

  });
});

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
        }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
});
</script>

@endsection