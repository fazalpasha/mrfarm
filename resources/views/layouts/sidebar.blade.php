<!-- Main Sidebar Container -->
@php

$c = Request::segment(1);
$m = Request::segment(2);
@endphp
<style type="text/css">
  .mdi{
    font-size: 16px;
  }
</style>
<aside class="main-sidebar sidebar-primary elevation-3">
    <!-- Brand Logo -->
    <a href="/dashboard">
      
      <span class="brand-text font-weight-light" style="font-size: 24px;line-height: 2.6;padding-left: 10px;font-weight: bold !important;color: #0071a9;"><left><img src="{{asset('assets/images/logo-mr.png')}}"  class="brand-image"  width="60">MR Farm</left></span>
    </a>    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

       --><!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
                    <a href="/dashboard" style="" class="nav-link @if($c == 'dashboard') active @endif ">
                        <i class="mdi mdi-view-dashboard"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('animalstock.index')}}"class="nav-link @if($m == 'animalstock') active @endif ">
                        <i class="mdi mdi-store"></i>
                        <p>Animal Stock</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('foodstock.index')}}" class="nav-link @if($m == 'foodstock') active @endif">
                        <i class="mdi mdi-food-variant"></i>
                        <p>Food & Medical Stock</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('production.index')}}" class="nav-link @if($m == 'production') active @endif">
                        <i class="mdi mdi-reproduction"></i>
                        <p>Production Stock</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('feed.index')}}" class="nav-link @if($m == 'feed') active @endif">
                        <i class="mdi mdi-message-draw"></i>
                        <p>Daily FeedLog</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('feedinfo.index')}}" class="nav-link @if($m == 'feedinfo') active @endif">
                        <i class="mdi mdi-reproduction"></i>
                        <p>Feed Chart</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('sales.index')}}" class="nav-link @if($m == 'sales') active @endif">
                        <i class="mdi mdi-email-outline"></i>
                        <p>Business Sales</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('resale.index')}}" class="nav-link @if($m == 'resale') active @endif">
                        <i class="mdi mdi-message-draw"></i>
                        <p>Resale</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('agent.index')}}" class="nav-link @if($m == 'agent') active @endif">
                        <i class="mdi mdi-account"></i>
                        <p>Sale Agent</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('expense.index')}}" class="nav-link @if($m == 'expense') active @endif">
                        <i class="mdi mdi-email-outline"></i>
                        <p>Expense</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('credit.index')}}" class="nav-link @if($m == 'credit') active @endif">
                        <i class="mdi mdi-message-draw"></i>
                        <p>Credits</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('staff.index')}}" class="nav-link @if($m == 'staff') active @endif">
                        <i class="mdi mdi-format-list-numbers"></i>
                        <p>Staff List</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('staffattendance.index')}}" class="nav-link @if($m == 'staffattendance') active @endif">
                        <i class="mdi mdi-account-multiple"></i>
                        <p>Staff Attendance</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('cell.index')}}" class="nav-link @if($m == 'cell') active @endif">
                        <i class="mdi mdi-checkbox-blank"></i>
                        <p>Plant Cell Box</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('batch.index')}}" class="nav-link @if($m == 'batch') active @endif">
                        <i class="mdi mdi-grid"></i>
                        <p>Batch</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('qrcode.index')}}" class="nav-link @if($m == 'qrcode') active @endif">
                        <i class="mdi mdi-qrcode"></i>
                        <p>QR-CODE List</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('payslip.index')}}" class="nav-link @if($m == 'payslip') active @endif">
                        <i class="mdi mdi-message-draw"></i>
                        <p>Payslips</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('vendor.index')}}" class="nav-link @if($m == 'vendor') active @endif">
                        <i class="mdi mdi-account"></i>
                        <p>Vendors</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('vehicalreports.index')}}" class="nav-link @if($m == 'vehicalreports') active @endif">
                        <i class="mdi mdi-car"></i>
                        <p>Vehicle List</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('transport.index')}}" class="nav-link @if($m == 'transport') active @endif">
                        <i class="mdi mdi-truck"></i>
                        <p>Transport Log</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('entrylogs.index')}}" class="nav-link @if($m == 'entrylogs') active @endif">
                        <i class="mdi mdi-clipboard-text"></i>
                        <p>Entry Logs</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('complaint.index')}}" class="nav-link @if($m == 'complaint') active @endif">
                        <i class="mdi mdi-clipboard-text"></i>
                        <p>Complaints</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('location.index')}}" class="nav-link @if($m == 'location') active @endif">
                        <i class="mdi mdi-clipboard-text"></i>
                        <p>Location</p>
                    </a>
                </li>
          <li class="nav-item @if($c == 'users') menu-open @endif">
            <a href="javascript:void(0)" class="nav-link @if($c == 'users') active @endif">
              <i class="mdi mdi-chevron-right"></i>
              <p>
                Users
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('users.index')}}" class="nav-link @if($c == 'users' && $m == 'users') active @endif">
                  <i class="mdi mdi-account"></i>
                  <p>User</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('roles.index')}}" class="nav-link @if($c == 'users' && $m == 'roles') active @endif">
                  <i class="mdi mdi-account-key"></i>
                  <p>Roles</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('topcategory.index')}}" class="nav-link @if($c == 'users' && $m == 'topcategory') active @endif">
                  <i class="mdi mdi-email-outline"></i>
                  <p>Top Category</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('category.index')}}" class="nav-link @if($c == 'users' && $m == 'category') active @endif">
                  <i class="mdi mdi-email-outline"></i>
                  <p>Category</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('subcategory.index')}}" class="nav-link @if($c == 'users' && $m == 'subcategory') active @endif">
                  <i class="mdi mdi-email-outline"></i>
                  <p>Sub Category</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('branch.index')}}" class="nav-link @if($c == 'users' && $m == 'branch') active @endif">
                  <i class="mdi mdi-email-outline"></i>
                  <p>Branch</p>
                </a>
              </li>
            </ul>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>