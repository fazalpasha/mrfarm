            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <!-- <div class="topbar-left" style="background-color: #20AFD9;"><br>
                    <a href="/dashboard" class="logo">
                        <span>
                            <img style="margin-top: -40px;
                                margin-left: -105px;" src="{{ URL::asset('assets/images/logo-mr.png')}}" alt="" height="40px;">
                        </span>
                        <div>
                           <h6 style="color: white; margin-top: -61px;
                            margin-left: 55px;">MR FARMS</h6>
                        </div>
                    </a>
                </div> -->
                

                <nav class="navbar-custom" style="background:#7a6fbe !important;">

                    <ul class="navbar-right d-flex list-inline float-right mb-0">
                        <li class="dropdown notification-list d-none d-sm-block">
                            <form role="search" class="app-search">
                                <div class="form-group mb-0"> 
                                    <input type="text" class="form-control" placeholder="Search..">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </form> 

                        </li>

                        <li class="dropdown notification-list">
                            
                           <!--  <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="ti-bell noti-icon"></i>
                                <span class="badge badge-pill badge-danger noti-icon-badge">3</span>
                            </a> -->
                            <!-- <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                             
                                <h6 class="dropdown-item-text">
                                    Notifications (258)
                                </h6>
                                <div class="slimscroll notification-item-list">
                                    
                                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details">Your order is placed<small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                    </a>
                                   
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                        <p class="notify-details">New Message received<small class="text-muted">You have 87 unread messages</small></p>
                                    </a>
                                    
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                        <p class="notify-details">Your item is shipped<small class="text-muted">It is a long established fact that a reader will</small></p>
                                    </a>
                                   
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details">Your order is placed<small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                    </a>
                                   
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-danger"><i class="mdi mdi-message"></i></div>
                                        <p class="notify-details">New Message received<small class="text-muted">You have 87 unread messages</small></p>
                                    </a>
                                </div>
                               
                                <a href="javascript:void(0);" class="dropdown-item text-center text-primary">
                                    View all <i class="fi-arrow-right"></i>
                                </a>
                            </div>  -->       
                        </li>
                          <li class="dropdown notification-list d-none d-sm-block">
                            <div class="form-group mb-0"> 
                             <button type="button" style="width:150px; height: 40px;margin-top: 15px;    border-radius: 21px;background: #ea526b;"class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                          Change Branch
                        </button>
                    </div>

                        </li>
                        <li class="dropdown notification-list">
                            <div class="dropdown notification-list nav-pro-img">
                                <a class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="{{asset('assets/images/users/user-4.jpg')}}" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->

                                    <!-- <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5"></i> Profile</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-wallet m-r-5"></i> My Wallet</a>
                                    <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="mdi mdi-settings m-r-5"></i> Settings</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5"></i> Lock screen</a>
                                    <div class="dropdown-divider"></div> -->

                                    @auth 
                                    <a class="dropdown-item text-danger" href="{{ url('logout') }}"><i class="mdi mdi-power text-danger"></i> Logout</a>
                                    @endauth
                                </div>                                                                    
                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left" style="background: #7a6fbe !important">
                            <button data-widget="pushmenu" class="button-menu-mobile open-left waves-effect" style="background: #7a6fbe !important;color: white">
                                <i class="mdi mdi-menu"></i>
                            </button>
                            <?php $userid = Auth::user();
                                    use App\Entities\branch;
                           if(isset($_GET['branch_id'])){
                            $newbranch_id=$_GET['branch_id'];
                            }else{
                                $newbranch_id=$userid->branch_id;
                            }
                           
                           $branch=branch::all();
                           $mybranch=branch::find($newbranch_id); ?>
                           <span style="color: white;font-size: 20px;">{{$mybranch->name}} - {{$mybranch->city}}</span>
                        </li>

                        <!-- <li class="d-none d-sm-block">
                            <div class="dropdown pt-3 d-inline-block">
                                <a class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Create
                                </a>
                                
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                        </li> -->
                    </ul>

                </nav>

            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Select Branch</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php foreach ($branch as $branchs) {?><a href="{{route('dashboard','branch_id='.$branchs->id)}}">
        <button style="size:20px;width:150px; height: 70px" class="btn btn-info">
          <font size="4px;"> {{$branchs->name}}</font></button></a>
        <?php } ?>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>

                        
              </div>
            <!-- Top Bar End -->
