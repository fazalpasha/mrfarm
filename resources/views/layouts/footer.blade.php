
        <footer class="footer">
                © {{date('Y')}} MR Farm<span class="d-none d-sm-inline-block"> - Crafted with<i class="mdi mdi-heart text-danger"></i> by<a href="http://bigome.com/"> Bigome</a></span>.
        </footer>
