@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Add Payment</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('sales.index')}}">Sales</a></li>
                        <li class="breadcrumb-item active">Add</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                {!! Form::open(array('route' => 'sales.paymentstore','method'=>'POST','files' =>'true')) !!}

                <div class="card m-b-20">
                    <div class="card-body">

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('paid_amount') ? ' has-error' : '' }}">
                                        {!! Form::label('paid_amount', 'Paid Amount') !!}<br>
                                        <input  name="paid_amount" type="text" id="paid_amount" class="form-control">
                                        {!! $errors->first('paid_amount', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group {{ $errors->has('paid_type') ? ' has-error' : '' }}" >
                                        {!! Form::label('paid_type', 'Created Date') !!}<br>
                                       <select class="form-control" name="paid_type">
                                            <option value="">Select Payment Mode</option>
                                            <option value="cash">cash</option>
                                            <option value="cheque">Cheque</option>
                                            <option value="card">Card</option>
                                            <option value="online payment">online Payment</option>
                                        </select>
                                        {!! $errors->first('paid_type', '<span class="help-block">:message</span>') !!}
                                </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('recipt_no') ? ' has-error' : '' }}">
                                        {!! Form::label('recipt_no', 'Recipt No') !!}<br>
                                        <input  name="recipt_no" type="text" id="recipt_no" class="form-control">
                                        {!! $errors->first('recipt_no', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                        {!! Form::label('date', 'Date') !!}<br>
                                        <input  name="date" type="date" id="tot" class="form-control">
                                        {!! $errors->first('date', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                            </div>

                            <div class="">
                                <button type="submit" class="btn btn-primary btn-lg  text-white">Create</button>

                                <a type="button" class="btn btn-danger btn-lg float-right text-white " href="{{route('sales.index')}}">Cancel</a>
                            </div>

                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

                {!! Form::close() !!}

            </div> <!-- container-fluid -->
        </div>
    </div>

@endsection
@section('js_after')
@endsection
