@extends('layouts.master')
@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinModern.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Business Sales</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('sales.index')}}">Business Sales</a></li>
                </ol>
                <div class="state-information d-none d-sm-block">
                   <div class="btn-group pull-right">
                        <button type="button"   class="btn btn-primary btn-lg fadeandscale_open waves-effect waves-light float-left" >
                        Create
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Business Sales</h4>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Category Type</label>
                                <select class="form-control" id="filter_category">
                                    <option value="">Select Type</option>
                                    <?php  foreach ($topcat as $top) {?>
                                    <option value="{{$top->id}}">{{$top->topcategory_name}}</option>
                                    <?php }?>
                                </select></div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group"><label>Date From </label><input id="from" type="date" name="data_from"
                                                                                    class="form-control"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group"><label>Date to </label><input type="date" id="to" name="data_from"
                                                                                  class="form-control"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group"><label></label><br>
                                <button id="filter" class="btn btn-primary btn-lg" type="button">Fliter</button>
                            </div>
                        </div>
                    </div>
                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Items</th>
                                <th>Total Weight</th>
                                <th>Total Price</th>
                                <th>Sold To</th>
                                <th>Sold By</th>
                                <th>Date</th>
                                <th>Branch</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
</div>
<div  id="fadeandscale">
    <div class="card m-b-20 p-4">
        <h3>Select Type Of Business Sales you Want to Add</h3>
        <hr>
        <div class="card-body">
            <div class="row">

                <?php if(isset($topcat)){ ?>
                <?php  foreach ($topcat as $top) {?>
                <a  href="{{ route('sales.create', ['type'=>$top->topcategory_name]) }}" class="btn btn-primary btn-lg ml-2 mr-2  waves-effect waves-light float-left">{{$top->topcategory_name}}</a>
                <?php }?>

                    <?php }?>
            </div>
        </div>

        <button class="fadeandscale_close slide_open btn btn-danger float-right mb-3">Close</button>
    </div>
</div><!-- container-fluid -->
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-popup-overlay/1.7.8/jquery.popupoverlay.js"></script>
    <script>
        $('#fadeandscale').popup({
            autoopen :false,
            escape: true,
            blur: false,
            scrolllock: false,
        });
    </script>

<!-- Required datatable js -->
<script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{ URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<!-- Datatable init js -->
    <script>
        $(document).ready(function(){
            fill_datatable();

            function fill_datatable(filter_category = '',datefrom='',dateto='')
            {
                var Table = $('#datatable').DataTable({
                    "processing" : true,
                    "serverSide" : true,
                    "ajax" : {
                        url:"{{route('sales.filter')}}",
                        type:"get",
                        data:{
                            filter_category:filter_category,date_from:datefrom,date_to:dateto
                        }
                    },
                    "columns": [
                        { "data": "id"},
                        { "data": "itemtype"},
                        { "data": "total_weight"},
                        { "data": "total_price"},
                        { "data": "vendor_name"},
                        { "data": "addbyname"},
                        { "data": "created_at"},
                         { "data": "itemname"},
                        {
                            "render": function (data, type, JsonResultRow, meta) {
                                return '<div class="btn-group"><a href="{{env('APP_URl')}}/dashboard/sales/edit/'+JsonResultRow.id+'" class="btn btn-primary btn-flat"><i class="ion-printer"></i></a><a href="{{route('sales.addpayment')}}" class="btn btn-primary btn-flat">Add Payments<i class="ion-printer"></i></a><a href="{{route('sales.viewpayment')}}" class="btn btn-primary btn-flat">All Payments<i class="ion-printer"></i></a><button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{env('APP_URl')}}/dashboard/sales/delete/'+JsonResultRow.id+'"><i class="fa fa-pencil">Cancel</i></button></div>';
                            }
                        },

                    ]
                });
            }

            $('#filter').on('click',function () {
                var filter_category = $('#filter_category :selected').val();
                var fromd = $('#from').val();
                var to = $('#to').val()
                $('#datatable').DataTable().destroy();
                fill_datatable(filter_category,fromd,to);
            });
        });


    </script>
@endsection
