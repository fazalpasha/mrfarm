@extends('layouts.master')

@section('content')
<?php $userid = Auth::user();
                                    use App\Entities\branch;
                           if(isset($_GET[$sales->branch_id])){
                            $newbranch_id=$_GET[$sales->branch_id];
                            }else{
                                $newbranch_id=$userid->branch_id;
                            }
                           
                           $branch=branch::all();
                           $mybranch=branch::find($newbranch_id); ?>
                           <span style="color: white;font-size: 20px;">{{$mybranch->name}} - {{$mybranch->city}}</span>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Sales</h4>
                <ol class="breadcrumb">
                   <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                   <li class="breadcrumb-item"><a href="{{route('sales.index')}}">Sales</a></li>
                   <li class="breadcrumb-item active">Sales</li>
               </ol>
           </div>
       </div>
   </div>
<style>
  hr {width: 100%;height: 4px;margin-left: auto;margin-right: auto;background-color:#0d7ab5;border: 0 none;margin-top: 10px;margin-bottom:50px;}
</style>
    <div class="row">
        <div class="col-12">

            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">

  <body style="background-color: white;">  

<table style="border:1px;" width="100%">
  <h5>{{$mybranch->name}}</h5><h6 style="text-align: right">MR FARMS PVT LTD<br>
Karanataka India</h6>
    <span class="brand-text font-weight-light" style="font-size: 24px;line-height: 2.6;padding-left: 10px;font-weight: bold !important;color: #0071a9;"><left><img src="{{asset('assets/images/logo-mr.png')}}"  class="brand-image"  width="60">MR Farm</left></span>
    </table>
    <br><br>
    <table style="border:1px;" width="100%">
        <tr>
            <td><b style="color:#0d7ab5;">Billed To</b><br>{{$sales->vendor_name}}<br>{{$sales->vendor_address}}<br>{{$sales->vendor_phone}}</td><td><b style="color:#0d7ab5;">Date of Issue</b><br>{{$sales->created_at}}</td><td><b style="color:#0d7ab5;">Amount</b><br>{{$sales->total_price}}</td><td></td>
         </tr>
         </table><br>
         <hr></hr>
         <table style="border:1px;" width="100%">
        <tr>
            <th><b style="color:#0d7ab5;">ID</b></th><th><b style="color:#0d7ab5;">Price</b></th><th><b style="color:#0d7ab5;">GST</b></th><th><b style="color:#0d7ab5;">Total</b></th></tr>
            <tr>
            <td>{{$sales->bname}}</td><td>₹{{$sales->sub_price}}.00</td>
            <td>₹{{$sales->gst_price }}.00</td>
            <td>₹{{$sales->total_price}}.00</td>
         </tr>
         <tr>
            <th></th><th></th><th><b>Expenses</b></th><th><b>₹500.00</b></th></tr>
            <?php $a=$sales->total_price;
            $b=500;
            $c=$a+$b;?>
            <tr>
            <th></th><th></th><th><b>Amount DUE</b></th><th><b>₹<?php echo $c;?>.00</b></th></tr>
         </table>
                        <div class="">
                            <button type="submit" onclick="window.print()" class="btn btn-primary btn-lg  text-white">Print</button>
                            
                        </div>   
                    </div>
                </div> 
            </div>
            
        </div>
    </div><!-- end row-->
</div>
</body> <!-- container-fluid -->
@endsection
@section('js_after')
<script>
  $(document).ready(function() {
    $('#mycheckbox').change(function() {
      $('#mycheckboxdiv').toggle();

  });
});

  $("input[type='checkbox']").on('change', function(){
    this.value = this.checked ? 1 : 0;
            // alert(this.value);
        }).change();

  $(document).on('click', 'button.removebutton', function () {
    $(this).closest('tr').remove();
    return false;
});
</script>

@endsection