@extends('layouts.master')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-sm-12">
<div class="page-title-box">
<h4 class="page-title">Business Sales</h4>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
<li class="breadcrumb-item active">Add Business Sales</li>
</ol>
</div>
</div>
</div>

<div class="row">
<div class="col-12">
{!! Form::open(array('route' => 'sales.store','method'=>'POST')) !!}

<div class="row">
<div class="col-12">
    <div class="card m-b-20">
        <div class="card-body">

            <div class="row">
                <div class="col-12">
                    <div class="invoice-title">
                        <h4 class="float-right font-16"><strong>Order # MR00{{count($sales)+1}}</strong></h4>
                        <h3 class="mt-0">
                            <h3>MR Farms Invoice</h3>
                        </h3>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            <address>
                                <strong>From :</strong><br>
                                MR FARMS PVT LTD<br>
                                Karanataka India<br>
                            </address>
                        </div>
                        <div class="col-6 text-right">
                            <address>
                                <strong>To :</strong><br>
                            </address>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label">Vendor Name *</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" placeholder="Vendor Name" required name ="vendor_name">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label">Vendor Phone</label>
                                <div class="col-sm-9">

                                    <input class="form-control" type="text" placeholder="Vendor Phone" name ="vendor_phone">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label">Vendor Address</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" type="text" placeholder="Vendor Address" name ="vendor_address"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 col-form-label">Select Agent</label>
                                <div class="col-sm-9">
                                    <select  class="form-control select2" id="code">
                                                <option value="">Select Agent</option>
                                                <?php if(isset($agent)){ ?>
                                                <?php foreach ($agent as $qr){ ?>
                                                <option value="{{$qr->id}}">{{$qr->agent_name}}</option>
                                                <?php }?>
                                                <?php }?>
                                            </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-3 required col-form-label">GST *</label>
                                <div class="col-sm-9">

                                    <select id="gst" class="form-control" name="gst">

                                        <option value="0">No GST</option>
                                        <option value="18">    GST 18 %</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 m-t-30">
                        </div>
                        <div class="col-6 m-t-30 text-right">
                            <address>
                                <strong>Date:</strong><br>
                                {{date(" F j, Y")}}<br><br>
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div>
                        <div class="p-2">
                            <h3 class="font-16"><strong> Summary</strong></h3>
                        </div>
                        <div class="">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>

                                        <td><strong>Item</strong><input type="hidden" name="top_id" value="{{$request->type}}"></td>

                                        <td ><strong><?php if($request->type == "Milk"){ ?> Litre <?php }else{ ?> Weight - Kg <?php }?></strong></td>
                                        <?php if(strtoupper($request->type) == "ANIMAL"){ ?><td colspan="1"></td>

                                        <?php }?>
                                        <td ><strong>Price</strong></td><?php if(strtoupper($request->type) == "ANIMAL"){ ?>
                                        <td class="text-right"><strong>Action</strong></td>
                                        <?php }?>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php if(strtoupper($request->type) == "MILK"){ ?>
                                    <tr>
                                        <td>{{$request->type}}</td>

                                        <td class="text-center">{!! Form::text('total_weight', old('total_weight'), ['class' => 'form-control weight','size'=>3,'id'=>'weightd','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('Litre')]) !!}<input type="hidden" class="notoday" value="0"></td>

                                        <td class="text-center">{!! Form::text('price', old('price'), ['class' => 'form-control price oneprice','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");', 'placeholder' => trans('Price')]) !!}</td>
                                    </tr>
                                    <?php }?>
                                    </tbody>
                                    <tfoot>
                                    <?php if(strtoupper($request->type) == "ANIMAL"){ ?>
                                    <tr>
                                        <td><select  class="form-control select2" id="code">
                                                <option value="">Select Item Code or Item</option>
                                                <?php if(isset($qrcode)){ ?>
                                                <?php foreach ($qrcode as $qr){ ?>
                                                <option value="{{$qr->id}}">{{$qr->prefix}}{{$qr->qr_code}}--{{$qr->category_name}}--{{$qr->subcategory_name}}--{{$qr->created_at}}</option>
                                                <?php }?>
                                                <?php }?>
                                            </select></td>
                                        <td class="text-center d-flex">{!! Form::text('total_weight', old('total_weight'), ['class' => 'form-control weight','size'=>3,'id'=>'weightd','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");','placeholder' => trans('Weight')]) !!}<i class=" pl-2 pr-2 mt-2 fa fa-times"></i></td>
                                        <td><span  class="mt-2" id="totalprice">0</span><input type="hidden" class="notoday" value="0"></td>
                                        <td class="text-center">{!! Form::text('price', old('price'), ['class' => 'form-control price','oninput'=>'this.value = this.value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1");', 'placeholder' => trans('Price')]) !!}</td>
                                        <td class="text-right"><button onclick="fields()" type="button" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus"></i>ADD</button></td>
                                    </tr>
                                    <?php }?>
                                    <tr>
                                        <?php if($request->type == "Animal"){ ?>
                                        <td class="no-line"></td>
                                        <?php }?>
                                        <td class="no-line"></td>
                                        <td class="thick-line text-center">
                                            <strong>Subtotal</strong></td>

                                        <td class="thick-line text-right"><i class="fa fa-rupee-sign"></i>  <b class="subtotal"></b></td>

                                    </tr>
                                    <tr>
                                        <?php if($request->type == "Animal"){ ?>
                                        <td class="no-line"></td>
                                        <?php }?>
                                        <td class="no-line"></td>

                                        <td class="no-line text-center">
                                            <strong>GST</strong></td>
                                        <td class="no-line text-right"><i class="fa fa-rupee-sign"></i> <b class="gstprice"></b></td>
                                    </tr>
                                    <tr><?php if($request->type == "Animal"){ ?>
                                        <td class="no-line"></td>
                                        <?php }?>
                                        <td class="no-line"></td>
                                        <td class="no-line text-center">
                                            <strong>Total</strong></td>
                                        <td class="no-line text-right"><h4 class="m-0"><i class="fa fa-rupee-sign"></i> <b class="totalprice"></b></h4></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="">

<input type="hidden" name="sub_price" class="subtotal">
                                <input type="hidden" name="gst_price" class="gstprice">
                                <input type="hidden" name="total_price" class="totalprice">

                                <a href="{{route('sales.index')}}" class="btn btn-lg btn-danger ">Cancel</a>
                                <button type="submit" class="btn btn-primary btn-lg  text-white float-right">Create</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!-- end row -->

        </div>
    </div>
</div> <!-- end col -->
</div>
{!! Form::close() !!}
</div>

</div><!--end row-->
</div> <!-- container-fluid -->
@endsection
@section('js_after')
<script>
$('#code').on('change',function () {
var animalcode = $('#code :selected').val();
$.ajax({
url: '{{route('sales.getprice')}}',
type: 'GET',
dataType: 'json',
data: {
animal_id: animalcode
},
success: function (result) {
if (!$.trim(result)){
$('#totalprice').html(" Please Enter Price Manually");
$('.notoday').val(result.price);
}
else{
$('#totalprice').html("₹ "+ result.price+" Per Kg as on ("+result.date+")");
$('.notoday').val(result.price);
}
}
});
$('.weight').val("");
$('.price').val("");
$('#totalprice').html(" 0");
$('.notoday').val(0);
});
$('#gst').on('change',function () {
cal(parseFloat($(this).val()));
});
<?php if(strtoupper($request->type) == "MILK"){ ?>
$(".price").on("input", function(e) {
var  gst = parseFloat($('#gst :selected').val());
cal(parseFloat(gst));

});
<?php }?>
$("#weightd").on("input", function(e) {
var input = $(this);
var val = input.val();
var today = parseFloat($('.notoday').val());

if (input.data("lastval") != val) {
input.data("lastval", val);
$('.price').val(val*today);
//your change action goes here

}

});
function fields() {
var count = $('.removeday').length+1,
animalcode = $('#code :selected').val(),
detail = $('#code :selected').text(),
weight = $('.weight').val(),
price = $('.price').val(),
gst = parseFloat($('#gst :selected').val());
if(animalcode === '' || weight === '' || price === ''){
alert('Required Item Code,Weight and Price value');
}else {
var objTo = document.getElementById('tbody');
var divtest = document.createElement("tr");
divtest.setAttribute("class", "removeday removeclassed" + animalcode);
divtest.innerHTML = '<td><input type="hidden" name="sales[' + count + '][acode]" value="' + animalcode + '">' + detail + '</td><td><input type="hidden" name="sales[' + count + '][weight]" value="' + weight + '">' + weight + '</td><?php if($request->type == "Animal"){ ?><td colspan="1"></td><?php }?><td><input type="hidden" class="oneprice" name="sales[' + count + '][price]" value="' + price + '">' + price + '</td><td class="text-right"><button class="btn btn-danger mr-1" type="button" onclick="remove_fields(' + animalcode + ');"><i class="fa fa-fw fa-times mr-1"></i> Delete </button></td>';
objTo.appendChild(divtest)
$('.weight').val("");
$('.price').val("");
$('#totalprice').html(" 0");
$('.notoday').val(0);
$("#code option[value='"+ animalcode +"']").prop("disabled",true);
$('#code').select2().val(null).trigger('change');
$('#code').select2().trigger('change');
cal(gst);
}
}
function remove_fields(rid) {
$('.removeclassed'+rid).remove();
var gst = parseFloat($('#gst :selected').val());
$("#code option[value='"+ rid +"']").prop("disabled",false);
$('#code').select2().val(null).trigger('change');
$('#code').select2().trigger('change');
cal(gst);
}
function cal(gst){
var sum = 0;
$('.oneprice').each(function(){
sum += parseFloat(this.value);
});
$('.subtotal').html(sum);
var gstamount = (sum * gst/100);
$('.gstprice').html(gstamount);
$('.totalprice').html(+gstamount + +sum);

$('.subtotal').val(sum);
$('.gstprice').val(gstamount);
$('.totalprice').val(+gstamount + +sum);
}
<?php if($request->type == "Animal"){ ?>

$('form').submit(function () {

// Get the Login Name value and trim it
var count = $('.removeday').length;
// Check if empty of not
if (count  === 0) {
alert('Add At Least One item to Create Invoice');
return false;
}
});

<?php }?>
</script>

@endsection
