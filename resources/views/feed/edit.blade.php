@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title"> Feed Stock</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Edit Feed Stock</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            {!! Form::open(['route' => ['feed.update', $feedchart->id], 'method' => 'put']) !!}
            <div class="card m-b-20">
                <div class="card-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::Label('Category Name','Category Name:') !!}
                                  <select required class="form-control" id="topcat" name="category_id">
                                        <option value="">Select Category</option>
                                     <?php foreach($category as $categorys){?> 
                                        <option <?php if(isset($feedchart->category_id)){ ?> <?php if($feedchart->category_id == $categorys->id) {?> selected <?php }?> <?php }?>value="{{$categorys->id}}">{{$categorys->category_name}}</option>
                                        <?php }?>
                                    </select>
                                     {!! $errors->first("category_name", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::Label('Sub Category Name', 'Sub Category Name:') !!}
                                    <select required class="form-control" id="category" name="subcategory_id">
                                       
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                     {!! Form::Label('Cell Name', 'Cell Name:') !!}
                                    <select class="form-control" name="cell_id">
                                         <?php if (isset($cell)) { ?>
                                <?php foreach($cell as $cells) { ?>
                                        <option <?php if($cells->id == $feedchart->cell_name){ ?> selected <?php } ?> value="{{$cells->id}}">{{$cells->cell_name}}</option>
                                        <?php }?>
                                        <?php }?>
                                    </select>
                                    <!--  {!! $errors->first("cell_name", '<span class="help-block">:message</span>') !!} -->
                              </div>
                          </div>

                          <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                {!! Form::label('quantity', trans('Quantity')) !!}
                                {!! Form::text('quantity', old('quantity',$feedchart->quantity), ['class' => 'form-control', 'placeholder' => trans('Quantity')]) !!}
                                {!! $errors->first('quantity', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                          <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }}">
                                {!! Form::label('unit', trans('Unit')) !!}
                                {!! Form::text('units', old('unit',$feedchart->units), ['class' => 'form-control', 'placeholder' => trans('Unit')]) !!}
                                {!! $errors->first('unit', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                      
                        <!-- <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('quality') ? ' has-error' : '' }}">
                                {!! Form::label('quality', trans('Quality')) !!}
                                {!! Form::text('quality', old('quality'), ['class' => 'form-control', 'placeholder' => trans('Quality')]) !!}
                                {!! $errors->first('quality', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div> -->

                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::Label('Added By', 'Added By:') !!}
                                <select class="form-control" name="added_by">
                                         <?php if (isset($add)) { ?>
                                <?php foreach($add as $key => $addedby) { ?>
                                        <option <?php if($addedby->id == $feedchart->username){ ?> selected <?php } ?> value="{{$addedby->id}}">{{$addedby->username}}</option>
                                        <?php }?>
                                        <?php }?>
                                    </select>
                                     <!-- {!! $errors->first("added_by", '<span class="help-block">:message</span>') !!} -->
                         </div>
                     </div>
                 </div>

                 <div class="">
                     <button type="submit" class="btn btn-primary btn-lg  text-white">Update</button>

                     <button type="button" class="btn btn-danger btn-lg float-right text-white ">Close</button>
                 </div>         

             </div>
         </div> <!-- end col -->
     </div> <!-- end row -->

{!! Form::close() !!}
</div>
</div>
</div> 
@endsection

@section('js_after')
<script>
  <?php if(isset($feedchart->category_id)){ ?>
    $( document ).ready(function() {
      var top_cat = "{{$feedchart->category_id}}";
      ajax(top_cat);
  });
<?php }?>
$('#topcat').on('change',function(){
    var top_cat = $(this).val();
    ajax(top_cat);

});
function ajax(top_cat) {

    url = '<?= route('production.subcat') ?>';
    $.get(url, {"top_cat": top_cat}, categorylist, 'json');

    function categorylist(d, s) {

      var elSel = document.getElementById('category');
      var i;
      for (i = elSel.length; i >= 0; i--) {
        elSel.remove(i);
    }
    if (d.length > 0) {
        $("#category").append('<option value="">Select Sub Category</option>');
    } else {
        $("#category").append('<option value="">No Sub Category</option>');
    }

    $.each(d,
        function () {
          var str = this.id;
          var name = this.subcategory_name;
          var option = new Option(str, name);
          var dropdownList = document.getElementById("category");
          var option = document.createElement("option");
          if (typeof name == 'undefined') {
            name = '';
        }
        option.appendChild(document.createTextNode(name));
        option.value = str;
        var subcat = "{{$feedchart->subcategory_id}}";
        if( subcat == str){
            option.setAttribute("selected","selected");
        }
        dropdownList.appendChild(option);
        if (name == 'error') {
        } else {
        }
    }
    );
}
}

</script>

@endsection